var socket = io.connect('https://codeengine-nodejs.herokuapp.com');
//var socket=io.connect('localhost:5000');
var newURL = "http://mylaravel5.herokuapp.com/slave";
var slaveConnectedUrl = "http://mylaravel5.herokuapp.com/slaveConnected";

var slaveTabId;
var songNumber;
var slaveUrl;

socket.on("ip",function(data){   
    alert('public ip ------> ' + data);
});

socket.on("open",function(data){

    slaveUrl = newURL + '/user/' + data.songNumber; 
  	chrome.tabs.create({ url: slaveUrl });
});

socket.on("start",function(data){
    slaveUrl = newURL + '/' + data.list + '/' + data.songNumber;
    chrome.tabs.create({ url: slaveUrl });
});

chrome.tabs.onCreated.addListener(function(tab) {

  		if(tab.url == slaveUrl){
  			 slaveTabId = tab.id;        
  		}  		
});

socket.on("close",function(data){

	chrome.tabs.remove(slaveTabId, function() { });	
});


chrome.browserAction.onClicked.addListener(function(activeTab){

  	chrome.tabs.create({ url: newURL });
});
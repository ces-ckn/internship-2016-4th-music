@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>
                <div class="panel-body">
                    <ul id="listSongs">
                    @foreach($listSong as $song)
                        <?php $url = URL::route("listentomusic",$song->song_id); 
                        $urlup = URL::route("voteup", $song->song_id);
                        $urldown = URL::route("votedown", $song->song_id);?>
                        <li id="{{$song->position}}"><p style="display:none" id="songId">{{$song->song_id}}</p><a id="songInfo" href="{{$url}}">{{$song->title}}</a> || <a href="{{$urlup}}">Up</a> || <a href="{{$urldown}}">Down</a><button id='upBtn'>Up</button>
                        <button id='dwnBtn'>Down</button></li>
                    @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<p id="msg"></p>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script src="nodejs/jquery.js"></script>
<script src="nodejs/socket.io.js"></script>
<script>
$(document).ready(function(){
    var socket=io.connect('https://codeengine-nodejs.herokuapp.com');
    $('#listSongs').children('li').children('#upBtn').click(function(){
        var songId = $(this).siblings('#songId').text();
        $.ajax({
        type:'POST',
        url:'./upajax',
        data:{_token:'<?php echo csrf_token() ?>',songId:songId},
        success:function(data){
            console.log(data.result);
            }
        });
    });

    socket.on('voteresult', function(data){
        $('#'+data['pos1']).children('#songInfo').html(data['songa']['title']);
        var url1 =  document.location.origin+"/internship-2016-4th-music/laravel/public"+"/listen/"+data['songa']['song_id'];
        $('#'+data['pos1']).children('#songInfo').attr('href',url1);
        $('#'+data['pos1']).children('#songId').text(data['songa']['song_id']);

        $('#'+data['pos2']).children('#songInfo').html(data['songb']['title']);
        var url2 = document.location.origin+"/internship-2016-4th-music/laravel/public"+"/listen/"+data['songb']['song_id'];
        $('#'+data['pos2']).children('#songInfo').attr('href',url2);
        $('#'+data['pos2']).children('#songId').text(data['songb']['song_id']);
    });

    $('#listSongs').children('li').children('#dwnBtn').click(function(){
        var songId = $(this).siblings('#songId').text();
        $.ajax({
        type:'POST',
        url:'./dwnajax',
        data:{_token:'<?php echo csrf_token() ?>',songId:songId},
        success:function(data){
            console.log(data.result);
            }
        });
    });
});
</script>
@endsection

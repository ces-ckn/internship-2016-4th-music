<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="{!! asset('user/css/animate.css') !!}"/>
<style>
@font-face {
   font-family: satisfy;
   src: url(user/fonts/Satisfy-Regular.ttf);
}

@font-face {
   font-family: tesla;
   src: url(user/fonts/TESLA.ttf);
}

@font-face {
   font-family: bigfish;
   src: url(user/fonts/Bigfish.ttf);
}

@font-face {
   font-family: waterlily;
   src: url(user/fonts/Waterlily.ttf);
}

@font-face {
   font-family: Ruffle-Beauty;
   src: url(user/fonts/Ruffle-Beauty.ttf);
}

.satisfy {
   font-family: satisfy;
}

.tesla {
   font-family: tesla;
}

.bigfish {
   font-family: bigfish;
}

.waterlily {
   font-family: waterlily;
}

.Ruffle-Beauty {
   font-family: Ruffle-Beauty;
   font-weight: bold;
}

</style>
</head>
<body>
<h1 class="animated swing">Example</h1> <!-- infinite -->
<div class="satisfy">
With CSS3, websites can finally use fonts other than the pre-selected "web-safe" fonts.<br>
Tiếng Việt
</div>

<div class="tesla">
Tesla font.<br>
Tiếng Việt
</div>

<div class="bigfish">
bigfish font.<br>
Tiếng Việt
</div>

<div class="waterlily">
waterlily font.<br>
Tiếng Việt
</div>

<div class="Ruffle-Beauty">
Ruffle-Beauty font.<br>
Tiếng Việt
</div>

Ruffle-Beauty

<p><b>Note:</b> Internet Explorer 8 and earlier, do not support the @font-face rule.</p>

</body>
</html>


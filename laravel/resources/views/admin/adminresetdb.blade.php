<!DOCTYPE html>
<head>
<title>Reset Database</title> <!–Tiêu đề trang web–>
<meta name="viewport" content="width=device-width, initial-scale=1.0"> <!–Tự động nhận dạng thiết bị để co vào cho phù hợp–>
<meta charset="UTF-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="stylesheet" type="text/css" href="{!! asset('user/css/bootstrap.min.css') !!}" /> <!– Gọi đến thư viện Bootstrap để sử dụng–>
<link rel="stylesheet" type="text/css" href="{!! asset('user/css/style.css') !!}"/> <!– Đây là file CSS của bạn–>
<link rel="stylesheet" href="{!! asset('user/css/style1.css') !!}" type="text/css" /> <!– Đây là file CSS của bạn–>
<link rel="stylesheet" href="{!! asset('user/css/style2.css') !!}" type="text/css" /> 
<link rel="stylesheet" href="{!! asset('user/css/timepicki.css') !!}" type="text/css" /> 
</head>
<body>
<!– Code bạn đặt trong đây–>

<!--include header-->
@include('user.header')
<div class="wrapPage" style="height: 500px">


<div class="col-md-4 col-md-offset-4" style="margin-top: 50px">
	<div class="panel panel-default">
        <div class="panel-heading">
            <strong>Reset Time</strong>
        </div>
		<div class="panel-body" style="padding-top: 20px">
            <div class="form-horizontal">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <label class="col-sm-4 control-label">Time:</label>
                    <div>
                        <input id='timeReset' type="text" class='timepicker' name="resettime">
                    </div>
                </div>

                <div class="form-group">
                    <div class="form-inline col-md-6 col-lg-offset-4">
                        <div class="form-group">
                            <button class="btn btn-warning" id="reset">
                                Reset
                            </button>
                        </div>
                        <div class="form-group" style="margin-left: 15px;">
                            <button class="btn btn-default" id="clearTimeBtn">
                                Clear
                            </button>
                        </div>
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-md-2 col-md-offset-4">
                    <img src="{!! asset('user/images/clock.png') !!}" alt="clock" height="42" width="42">
                </div>
                <div class="col-md-6" style="padding-top: 10px;">
                    <span id="time" class="label label-default"></span>
                </div>
            </div>

		</div>
	</div>

</div>
</div>


<!--include footer-->
@include('user.footer')
<!– Kết thúc Code của bạn–>
    <!– jQuery (necessary for Bootstrap’s JavaScript plugins) –>
    <script src= "{!! asset('user/js/jquery-2.2.4.min.js') !!}"></script>
    <!– Include all compiled plugins (below), or include individual files as needed –>
    <script src= "{!! asset('user/js/bootstrap.min.js') !!}"></script>
    <script src="{!! asset('user/js/myScript.js') !!}"></script> 
    <script src="{!! asset('user/js/timepicki.js') !!}"></script> 
    <script src="nodejs/socket.io.js"></script>
    <script type='text/javascript'> 
        $('.timepicker').timepicki({
        show_meridian:false,
        min_hour_value:0,
        max_hour_value:23,
        step_size_minutes:1,
        overflow_minutes:true,
        increase_direction:'up',
        disable_keyboard_mobile: true});
    </script>
    <script type="text/javascript">

        var socket=io.connect("https://codeengine-nodejs.herokuapp.com");
        //var socket=io.connect('127.0.0.1:5000');
        window.onload = function (){
            socket.emit('getResetTime', 'get reset db time');
            //alert('hehe');
        }        

        socket.on('getResetTime', function(data){
            //alert(data.time.hour);
            var time = '';
            if(data.time.hour == ''){
                time = 'The time\'s not set yet!';
            }else{
                time = data.time.hour + ':' + data.time.minute;
            }
            document.getElementById("time").innerHTML = time;
        });

        $('#reset').click(function(){
            $.ajax({
                type:'POST',
                url:'./set_resetdb_time',
                data:{_token:'{{ csrf_token() }}'
                    , timeReset:document.getElementById('timeReset').value
                },
                success:function(data){
                    alert(data);
                //return;
                }
            });
        });

        $('#clearTimeBtn').click(function(){
            document.getElementById('timeReset').value = "";
        });

    </script>
</body>
</html>
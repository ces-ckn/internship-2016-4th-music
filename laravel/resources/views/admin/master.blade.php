<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
@if($isListEmpty)
<h1>No song in list</h1>
@else
<table>
	<tr>
		<th>Position</th>
		<th>Name</th>
		<th></th>
		<th></th>
		<th></th>
	</tr>
	@foreach($masterList as $song)
	<?php $urldel = URL::route('masterdel', $song->song_id);
	$urlup = URL::route('masterup', $song->song_id);
	$urldown = URL::route('masterdown', $song->song_id);?>
	<tr>
		<td>{{$song->position}}</td>
		<td>{{$song->title}}</td>
		<td><a href="{{$urlup}}"><button>Up</button></a></td>
		<td><a href="{{$urldown}}"><button>Down</button></a></td>
		<td><a href="{{$urldel}}"><button>Delete</button></a></td>
	</tr>
	@endforeach
</table>
<?php $urldelall = URL::route('masterdelall')?>
<a href="{{$urldelall}}"><button>Delete All</button></a>
@endif
</body>
</html>
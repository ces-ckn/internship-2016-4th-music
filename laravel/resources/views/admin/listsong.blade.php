<!DOCTYPE html>
<head>
<title>Songs</title> <!–Tiêu đề trang web–>
<meta name="viewport" content="width=device-width, initial-scale=1.0"> <!–Tự động nhận dạng thiết bị để co vào cho phù hợp–>
<meta charset="UTF-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="stylesheet" type="text/css" href="{!! asset('user/css/bootstrap.min.css') !!}" /> <!– Gọi đến thư viện Bootstrap để sử dụng–>
<link rel="stylesheet" type="text/css" href="{!! asset('user/css/style.css') !!}"/> <!– Đây là file CSS của bạn–>
<link rel="stylesheet" href="{!! asset('user/css/style1.css') !!}" type="text/css" /> <!– Đây là file CSS của bạn–>
<link rel="stylesheet" href="{!! asset('user/css/style2.css') !!}" type="text/css" /> 
<style type="text/css">
	.linkDel{
		display: none;
	}
	.linkBlock{
		display: none;
	}
  table {
            width: 100%;

        }

        thead, tbody, tr, td, th { display: block; }

        tr:after {
            content: ' ';
            display: block;
            visibility: hidden;
            clear: both;
        }

        thead th {
            height: 30px;
        }

        tbody {
            height: 800px;
            overflow-y: auto;
        }

        thead {
        }


        tbody td, thead th {
            width: 20%;
            float: left;
        }
        .rating{
          width: 60px;
        }
        .delete{
          width: 90px;
        }
        .block{
          width: 90px;
        }
        .user{
          width: 220px;
        }
        .title{
          width: 250px;
        }
/*
 *  STYLE 3
 */

#scrollTable::-webkit-scrollbar-track
{
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
    background-color: #F5F5F5;
}

#scrollTable::-webkit-scrollbar
{
    width: 6px;
    background-color: #F5F5F5;
}

#scrollTable::-webkit-scrollbar-thumb
{
    background-color: #AFAFAF;
}

</style>
</head>
<body>
<!– Code bạn đặt trong đây–>

<!--include header-->
@include('user.header')

<div class="wrapPage" style="height: 1000px">
@if($isListEmpty)
<h1>No song in list</h1>
@else
<form class="form-inline" style="margin-bottom: 20px;">
    <input class="form-control form-sm" type="text" placeholder="Enter title or artist" onkeyup="showResult(this.value)">
</form>
<table class="table table-hover" id="myTable" style="font-size: medium">
    <thead>	
        <tr>
        	 <th class="rating">Rating</th>
            <th class="title">Title</th>
            <th>Artist</th>
            <th class="user">Added By</th>
            <th class="delete">Delete</th>
            <th class="linkDel">linkdel</th>
            <th class="block">Block</th>
            <th class="linkBlock">linkBlock</th>
        </tr>
    </thead>
    <tbody id="scrollTable">
	@foreach($listSong as $song)
			<?php 
				$info = $song->id_song."**".$song->title."**".$song->artist."**".$song->code128."**".$song->code320;
				$url = URL::route("listentomusic",$info);
				$urldel = URL::route('admindelete', $song->id_song);
				$urlblock = URL::route('adminblock', $info);
			?>
	    	<tr id="{{$song->id}}">
	    		<td class="rating"><strong>{{$song->position}}</strong></td>
	    		<td class="title"><a href="{{$url}}" title="" style="color: #FE9131;"><strong>{{$song->title}}</strong></a></td>
	    		<td>{{$song->artist}}</td>
	    		<td class="user">{{$song->userEmail}}</td>
	    		<td class="delete"><button class="btn btn-danger navbar-btn deleteBtn" data-toggle="modal" data-target="#modalDel" id="btnDelete"><span class="glyphicon glyphicon-trash"></span> Delete</button></td>
	    		<td class="linkDel">{{$urldel}}</td>
	    		<td class="block"><button class="btn btn-warning navbar-btn blockBtn" data-toggle="modal" data-target="#modalBlock" id="btnDelete"><span class="glyphicon glyphicon-remove"></span> Block</button></a></td>
	    		<td class="linkDel">{{$urlblock}}</td>
	    	</tr>
	@endforeach
	</tbody>
</table>
</div>
@endif
</div>

 <!-- Modal Delete -->
  <div class="modal fade" id="modalDel" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <span class="glyphicon glyphicon-alert" style="color: #C9302C;display:inline-block"></span><h4 class="modal-title" style="color: #C9302C;display:inline-block;margin-left: 5px;">Delete</h4>
        </div>
        <div class="modal-body" style="font-size: medium">
          <p>Do you want to delete <strong id="deleteConfirm" style="color: #C9302C"></strong> ?</p>
        </div>
        <div class="modal-footer">
        	<a href="" id="delSong"><button type="button" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> Delete</button></a>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        </div>
      </div>
      
    </div>
  </div>
  <!--Modal Block-->
   <div class="modal fade" id="modalBlock" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <span class="glyphicon glyphicon-exclamation-sign" style="color: #EC971F;display:inline-block"></span><h4 class="modal-title" style="color: #EC971F;display:inline-block;margin-left: 5px;">Block</h4>
        </div>
        <div class="modal-body" style="font-size: medium">
          <p>Do you want to block <strong id="blockConfirm" style="color: #EC971F"></strong> ?</p>
        </div>
        <div class="modal-footer">
        	<a href="" id="blockSong"><button type="button" class="btn btn-warning"><span class="glyphicon glyphicon-remove"></span> Block</button></a>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        </div>
      </div>
      
    </div>
  </div>

  <!--Modal Undo-->
   <div class="modal fade" id="modalUndo" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" style="color: #31B0D5">Undo</h4>
        </div>
        <div class="modal-body" style="font-size: medium">
          <p>You deleted a song. Do you want to undo your last delete?</p>
        </div>
        <div class="modal-footer">
          <a href="" id="undoSong"><button type="button" class="btn btn-info"><span class="glyphicon glyphicon-arrow-left"></span> Undo</button></a>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        </div>
      </div>
      
    </div>
  </div>


<!--include footer-->
@include('user.footer')
<!– Kết thúc Code của bạn–>
    <!– jQuery (necessary for Bootstrap’s JavaScript plugins) –>
    <script src= "{!! asset('user/js/jquery-2.2.4.min.js') !!}"></script>
    <!– Include all compiled plugins (below), or include individual files as needed –>
    <script src= "{!! asset('user/js/bootstrap.min.js') !!}"></script>
    <script src="{!! asset('user/js/myScript.js') !!}"></script> 
    <script type="text/javascript">
    var row;	
   	$('.deleteBtn').unbind().click(function(){
   		$('#deleteConfirm').text(document.getElementById("myTable").rows[row + 1].cells[1].textContent + " - " + document.getElementById("myTable").rows[row + 1].cells[2].textContent);
   		$('#delSong').prop('href',document.getElementById("myTable").rows[row + 1].cells[5].textContent);
    });

    $('.blockBtn').unbind().click(function(){
   		$('#blockConfirm').text(document.getElementById("myTable").rows[row + 1].cells[1].textContent + " - " + document.getElementById("myTable").rows[row + 1].cells[2].textContent);
   		$('#blockSong').prop('href',document.getElementById("myTable").rows[row + 1].cells[7].textContent);
    });
 	
 	$('tr').mouseover(function(){
            row = $(this).index();
    });
    </script>
    <script type="text/javascript">
        function showResult(key){
              key = key.toLowerCase();
              var listSong = <?php if($isListEmpty==false) echo json_encode($listSong);?>;
              var saveId=[];
              var length=listSong.length;
              for(i=0;i<length;i++){
                  $('#'+listSong[i].id).hide();
              }
              for(var i=0; i<length;i++){
                  if((listSong[i].title.toLowerCase().indexOf(key)!=-1)||(listSong[i].artist.toLowerCase().indexOf(key)!=-1)){
                      $('#'+listSong[i].id).show();
                      saveId.push(listSong[i].id);
                  }     
              }   
        }
</script>
<script type="text/javascript">
  $( document ).ready(function() {
        <?php
          if($deletedId!=""){
        ?>
              $('#undoSong').prop('href',"{{URL::route('aundo',$deletedId)}}");
              $('#modalUndo').modal('show');
        <?php 
          }
        ?>
  });
</script>
</body>
</html>
<!DOCTYPE html>
<html>
<head>
	<title>Set Open Time</title>
</head>
<body>

	<form action="set_open_time" method="POST">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="time" name="opentime">
		<input type="submit" value="Set open time">
	</form>

	<form action="set_closed_time" method="POST">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="time" name="closedtime">
		<input type="submit" value="Set closed time">
	</form>

</body>
</html>
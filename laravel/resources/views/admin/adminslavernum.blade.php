<!DOCTYPE html>
<head>
<title>Number of Slavers</title> <!–Tiêu đề trang web–>
<meta name="viewport" content="width=device-width, initial-scale=1.0"> <!–Tự động nhận dạng thiết bị để co vào cho phù hợp–>
<meta charset="UTF-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="stylesheet" type="text/css" href="{!! asset('user/css/bootstrap.min.css') !!}" /> <!– Gọi đến thư viện Bootstrap để sử dụng–>
<link rel="stylesheet" type="text/css" href="{!! asset('user/css/style.css') !!}"/> <!– Đây là file CSS của bạn–>
<link rel="stylesheet" href="{!! asset('user/css/style1.css') !!}" type="text/css" /> <!– Đây là file CSS của bạn–>
<link rel="stylesheet" href="{!! asset('user/css/style2.css') !!}" type="text/css" /> 
<link rel="stylesheet" href="{!! asset('user/css/timepicki.css') !!}" type="text/css" /> 
</head>
<body>
<!– Code bạn đặt trong đây–>

<!--include header-->
@include('user.header')
<div class="wrapPage" style="height: 500px">


<div class="col-md-4 col-md-offset-4" style="margin-top: 50px">

<div class="panel panel-default">
            <div class="panel-heading">
                <strong>Slaver number </strong><span id="slaverNumBadge" style="background-color: #777777" class="badge pull-right">10</span>
            </div>
            <div class="panel-body" style="padding-top: 10px">
                <div class="form-inline" role="form">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group" style="margin-left: 20px;">
                        <input id='slaverNumber' type='number' name="slavernumber" min='0' />
                    </div>                
                    <button id="setSlaverNumber" class="btn btn-warning">Ok</button>
                </div>

            </div>
        </div>    

</div>
</div>


<!--include footer-->
@include('user.footer')
<!– Kết thúc Code của bạn–>
    <!– jQuery (necessary for Bootstrap’s JavaScript plugins) –>
    <script src= "{!! asset('user/js/jquery-2.2.4.min.js') !!}"></script>
    <!– Include all compiled plugins (below), or include individual files as needed –>
    <script src= "{!! asset('user/js/bootstrap.min.js') !!}"></script>
    <script src="{!! asset('user/js/myScript.js') !!}"></script> 
    <script src="{!! asset('user/js/timepicki.js') !!}"></script> 
    <script src="nodejs/socket.io.js"></script>
    <script type='text/javascript'> 
        $('.timepicker').timepicki({
        show_meridian:false,
        min_hour_value:0,
        max_hour_value:23,
        step_size_minutes:1,
        overflow_minutes:true,
        increase_direction:'up',
        disable_keyboard_mobile: true});
    </script>
    <script type="text/javascript">
        var socket=io.connect("https://codeengine-nodejs.herokuapp.com");
        //var socket=io.connect('127.0.0.1:5000');

        window.onload = function (){
            socket.emit('getSlaverNumber', 'ok');
        }

        socket.on('getSlaverNumber', function (data){
            document.getElementById("slaverNumBadge").innerHTML = data;
        });

        $('#setSlaverNumber').click(function(){
            document.getElementById("slaverNumBadge").innerHTML = document.getElementById('slaverNumber').value;
            $.ajax({
                type:'POST',
                url:'./set_slaver_number',
                data:{_token:'{{ csrf_token() }}'
                    , slaverNumber:document.getElementById('slaverNumber').value
                },
                success:function(data){
                    alert(data);
                //return;
                }
            });
        });

        $('#clearTimeBtn').click(function(){
            document.getElementById('timeReset').value = "";
        });

    </script>
</body>
</html>
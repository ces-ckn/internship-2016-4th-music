<!DOCTYPE html>
<head>
    <title>Master</title> <!–Tiêu đề trang web–>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"> <!–Tự động nhận dạng thiết bị để co vào cho phù hợp–>
    <meta charset="UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="stylesheet" type="text/css" href="{!! asset('user/css/bootstrap.min.css') !!}" /> <!– Gọi đến thư viện Bootstrap để sử dụng–>
    <link rel="stylesheet" type="text/css" href="{!! asset('user/css/style.css') !!}"/> <!– Đây là file CSS của bạn–>
    <link rel="stylesheet" href="{!! asset('user/css/style1.css') !!}" type="text/css" /> <!– Đây là file CSS của bạn–>
    <link rel="stylesheet" href="{!! asset('user/css/style2.css') !!}" type="text/css" /> 
    <link rel="stylesheet" href="{!! asset('user/css/timepicki.css') !!}" type="text/css" /> 
    <style type="text/css">
        table {
            width: 100%;

        }

        thead, tbody, tr, td, th { display: block; }

        tr:after {
            content: ' ';
            display: block;
            visibility: hidden;
            clear: both;
        }

        thead th {
            height: 30px;
        }

        tbody {
            height: 500px;
            overflow-y: auto;
        }

        thead {
        }


        tbody td, thead th {
            width: 20%;
            float: left;
        }
        .deleteAll{
            text-align: center;
        }
        .deleteAll a{
            color: #F41C1C;
        }
        .up{
            width: 70px;
        }
        .down{
            width: 70px;
        }
        .up a{
            color: #000000;
        }
        .down a{
            color: #000000;
        }
        .title{
            width: 200px;
        }
        .delLink{
            display: none;
        }
/*
 *  STYLE 3
 */

#scrollTable::-webkit-scrollbar-track
{
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
    background-color: #F5F5F5;
}

#scrollTable::-webkit-scrollbar
{
    width: 6px;
    background-color: #F5F5F5;
}

#scrollTable::-webkit-scrollbar-thumb
{
    background-color: #AFAFAF;
}
</style>
</head>
<body>

<!– Code bạn đặt trong đây–>

<!--include header-->
@include('user.header')
<div class="wrapPage" style="height: 700px">

<div class="col-md-8" style="padding-right: 50px">
    @if($isListEmpty)
    <h1>No song in list</h1>
    @else
	<table id="masterList" class="table table-hover">
		<thead>	
                    <th class="title">Title</th>
                    <th>Artist</th>
                    <th class="up">Up</th>
                    <th class="down">Down</th>
                    <th class="deleteAll"><a href="#" class="delAll" >Delete All</a></th>
                    <th class="delLink">delLink</th>
        </thead>
        <tbody id="scrollTable">
            @foreach($masterList as $song)
            <?php $urldel = URL::route('masterdel', $song->id_song);
            $info = $song->id_song."**".$song->title."**".$song->artist."**".$song->code128."**".$song->code320;
            $urllisten = URL::route("listentomusic",$info);
            $urlup = URL::route('masterup', $song->id_song);
            $urldown = URL::route('masterdown', $song->id_song);?>
            @if($song->position == 1)
                <tr id="{{$song->position}}">    
                    <td class="title"><a href="{{$urllisten}}" style="color: #FE9131;font-size: medium"><strong>{{$song->title}}</strong></a></td>
                    <td>{{$song->artist}}</td>
                    <td class="up"><a href="{{$urlup}}"><button class="btn btn-default btn-sm hidden"><span class="glyphicon glyphicon-arrow-up"></span></button></a></td>
                    <td class="down"><a href="{{$urldown}}"><button class="btn btn-default btn-sm"><span class="glyphicon glyphicon-arrow-down"></span></button></a></td>
                    <td style="text-align: center">
                        <button class="btn btn-danger btn-sm btnDel" data-toggle="modal" data-target="#modalDelMaster"><span class="glyphicon glyphicon-trash"></span> Delete</button>
                    </td>
                    <td class="delLink">{{$urldel}}</td>
                </tr>
                @elseif($song->position != count($masterList))
                <tr id="{{$song->position}}">    
                    <td class="title"><a href="{{$urllisten}}" style="color: #FE9131;font-size: medium"><strong>{{$song->title}}</strong></a></td>
                    <td>{{$song->artist}}</td>
                    <td class="up"><a href="{{$urlup}}"><button class="btn btn-default btn-sm"><span class="glyphicon glyphicon-arrow-up"></span></button></a></td>
                    <td class="down"><a href="{{$urldown}}"><button class="btn btn-default btn-sm"><span class="glyphicon glyphicon-arrow-down"></span></button></a></td>
                    <td style="text-align: center">
                        <button class="btn btn-danger btn-sm btnDel" data-toggle="modal" data-target="#modalDelMaster"><span class="glyphicon glyphicon-trash"></span> Delete</button>
                    </td>
                    <td class="delLink">{{$urldel}}</td>
                </tr>
                @else
                <tr id="{{$song->position}}">    
                    <td class="title"><a href="{{$urllisten}}" style="color: #FE9131;font-size: medium"><strong>{{$song->title}}</strong></a></td>
                    <td>{{$song->artist}}</td>
                    <td class="up"><a href="{{$urlup}}"><button class="btn btn-default btn-sm"><span class="glyphicon glyphicon-arrow-up"></span></button></a></td>
                    <td class="down"><a href="{{$urldown}}"><button class="btn btn-default btn-sm hidden"><span class="glyphicon glyphicon-arrow-down"></span></button></a></td>
                    <td style="text-align: center">
                        <button class="btn btn-danger btn-sm btnDel" data-toggle="modal" data-target="#modalDelMaster"><span class="glyphicon glyphicon-trash"></span> Delete</button>
                    </td>
                    <td class="delLink">{{$urldel}}</td>
                </tr>
                @endif
                @endforeach

            </tbody>
        </table>
        @endif
    </div>
    <div class="col-md-4">

        <div style="margin-left:40px;margin-bottom: 20px;" class="form-group">
        <input style="margin-left:20px;margin-right:5px" type="radio" name="listRBtn" value="master"><strong>Master List</strong>
        <input checked="checked" style="margin-left:20px;margin-right:5px" type="radio" name="listRBtn" value="user"><strong>User List</strong>
        </div>

        <button id="start" class="btn btn-success btn-lg" style="margin-left: 40px;">Start Now</button>
        <button id="stop" class="btn btn-danger btn-lg">Stop Now</button>
        
        <div class="panel panel-warning" style="margin-top: 20px">
            <div class="panel-heading">
                <span class="glyphicon glyphicon-user"></span><strong>  Slave Number </strong>
            </div>
            <div class="panel-body" style="padding-top: 10px">
                <div class="form-inline" role="form">
                    <input type="hidden" name="" value="">
                    <div class="form-group" style="margin-left: 20px;">
                        <input id='' type='number' name="slavenumber" min='0' />
                    </div>                
                    <button id="" class="btn btn-warning" style="margin-left: 20px;">Ok</button>
                </div>

            </div>
        </div> 
        <div class="panel panel-success">
            <div class="panel-heading">
                <span class="glyphicon glyphicon-music"></span><strong>  Song Number </strong><span id="songNumBadge" style="background-color: #777777" class="badge pull-right">10</span>
            </div>
            <div class="panel-body" style="padding-top: 10px">
                <div class="form-inline" role="form">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group" style="margin-left: 20px;">
                        <input id='songNumber' type='number' name="songnumber" min='0' />
                    </div>                
                    <button id="setSongNumber" class="btn btn-success" style="margin-left: 20px;">Ok</button>
                </div>

            </div>
        </div>    


        <div class="panel panel-default">
            <div class="panel-heading">
                <span class="glyphicon glyphicon-time"></span><strong>  Set Time</strong>
            </div>
            <div class="panel-body" style="padding-top: 20px">
                <div class="form-horizontal">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Start:</label>
                        <div>
                            <input id='timeStart' type='text' class='timepicker' name="opentime" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Stop:</label>
                        <div>
                            <input id='timeStop' type='text' class='timepicker' name="closedtime" />
                        </div>

                    </div>

                        <div class="form-group col-md-6 col-lg-offset-4" style="margin-left: 45px">
                            <button class="btn btn-warning" id="setTime">
                                <span class="glyphicon glyphicon-time"></span> Set Time
                            </button>
                        </div>
                        <div class="form-group col-md-6 col-lg-offset-4">
                            <button class="btn btn-default" id="clearTimeBtn">
                                <span class="glyphicon glyphicon-erase"></span> Clear
                            </button>

                        </div>
                    </div>

                    <div class="row">
                    <div class="col-md-2 col-md-offset-4">
                        <img src="{!! asset('user/images/clock.png') !!}" alt="clock" height="42" width="42">
                    </div>
                    <div class="col-md-6" style="padding-top: 10px;">
                        <span id="opentime" class="label label-default"></span>
                        <span id="closedtime" class="label label-default"></span>
                    </div>
                </div>

                </div>

        </div>

   
    
    </div>

</div>

<!-- Modal Delete -->
<div class="modal fade" id="modalDelMaster" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <span class="glyphicon glyphicon-alert" style="color: #C9302C;display:inline-block"></span><h4 class="modal-title" style="color: #C9302C;display:inline-block;margin-left: 5px;">Delete</h4>
        </div>
        <div class="modal-body" style="font-size: medium">

          <p>Do you want to delete <strong id="deleteSMConfirm" style="color: #C9302C"></strong> ?</p>
      </div>
      <div class="modal-footer">
        <a href="" id="delSongMaster"><button type="button" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> Delete</button></a>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
    </div>
</div>

</div>
</div>

<!-- Modal Undo -->
<div class="modal fade" id="modalUndoMaster" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" style="color: #31B0D5">Undo</h4>
      </div>
      <div class="modal-body" style="font-size: medium">
        <p>You deleted a song. Do you want to undo your last delete?</p>
    </div>
    <div class="modal-footer">
        <a href="" id="undoMaster"><button type="button" class="btn btn-info"><span class="glyphicon glyphicon-arrow-left"></span> Undo</button></a>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
    </div>
</div>

</div>
</div>

  <!-- Modal Delete All -->
  <div class="modal fade" id="modalDelAll" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <span class="glyphicon glyphicon-alert" style="color: #C9302C;display:inline-block"></span><h4 class="modal-title" style="color: #C9302C;display:inline-block;margin-left: 5px;">Delete</h4>
        </div>
        @if(!$isListEmpty)
        <div class="modal-body" style="font-size: medium">
            <?php 
            $nos = count($masterList);
            ?>
          <p>Do you want to delete <strong style="color: #C9302C">{{$nos}}</strong> songs of <strong style="color: #C9302C">Master List</strong> ?</p>
        </div>
        @endif
        <div class="modal-footer">
            <?php $urldelall = URL::route('masterdelall')?>
            <a href="{{$urldelall}}"><button type="button" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> Delete</button></a>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        </div>
      </div>
      
    </div>
  </div>


<!--include footer-->
@include('user.footer')
<!– Kết thúc Code của bạn–>
<!– jQuery (necessary for Bootstrap’s JavaScript plugins) –>
<script src= "{!! asset('user/js/jquery-2.2.4.min.js') !!}"></script>
<!– Include all compiled plugins (below), or include individual files as needed –>
<script src= "{!! asset('user/js/bootstrap.min.js') !!}"></script>
<script src="{!! asset('user/js/myScript.js') !!}"></script> 
<script src="{!! asset('user/js/timepicki.js') !!}"></script>
<script src="{!! asset('nodejs/socket.io.js') !!}"></script>
<script type="text/javascript">

    var lastPos = <?php if($isListEmpty){
        echo 0;
    }else{
        echo count($masterList);
    } ?>;

    $(document).ready(function(data){
        var socket=io.connect("https://codeengine-nodejs.herokuapp.com");
            //var socket=io.connect('127.0.0.1:5000');

            socket.emit('getTime', 'get time');
            socket.emit('getSongNumber', 'get song number');

            socket.on('getTime', function(data){
                var opentime = '';
                var closedtime = '';
                if(data.openTime.hour == ''){
                    opentime = 'The time\'s not set yet!';
                }else{
                    opentime = data.openTime.hour + ':' + data.openTime.minute;
                    closedtime = data.closedTime.hour + ':' + data.closedTime.minute;
                }
                document.getElementById("opentime").innerHTML = opentime;
                document.getElementById("closedtime").innerHTML = closedtime;
            });

            socket.on('getSongNumber', function(data){
                document.getElementById("songNumBadge").innerHTML = data;
            });

            socket.on('masterlistaddresult',function(data){
                $('#masterList').find('#'+lastPos).find('.down').find('button').removeClass('hidden');
                lastPos+=1;
                var song = data['songlisten'];
                var info = song['id_song']+"**"+song['title']+"**"+song['artist']+"**"+song['code128']+"**"+song['code320'];
                var url = "<?php echo URL::route("listentomusic",'thisisthetempsongid');?>";
                url=url.replace('thisisthetempsongid',info);
                var urlup = "<?php echo URL::route('masterup','thisisthetempsongid');?>";
                urlup=urlup.replace('thisisthetempsongid',song['id_song']);
                var urldown = "<?php echo URL::route('masterdown','thisisthetempsongid');?>";
                urldown=urldown.replace('thisisthetempsongid',song['id_song']);
                var urldel = "<?php echo URL::route('masterdel','thisisthetempsongid');?>";
                urldel=urldel.replace('thisisthetempsongid',song['id_song']);
                var tbl = $('#masterList tbody');
                var newTr = '<tr id="'+lastPos+'">' + 
                    '<td class="title"><a href="'+url+'" style="color: #FE9131;font-size: medium"><strong>'+ song['title'] +'</strong></a></td>' +
                    '<td>'+song['artist']+'</td>' + 
                    '<td class="up"><a href="'+urlup+'"><button class="btn btn-default btn-sm"><span class="glyphicon glyphicon-arrow-up"></span></button></a></td>' + 
                    '<td class="down"><a href="'+urldown+'"><button class="btn btn-default btn-sm hidden"><span class="glyphicon glyphicon-arrow-down"></span></button></a></td>' +
                    '<td style="text-align: center">' +
                    '<button class="btn btn-danger btn-sm btnDel" data-toggle="modal" data-target="#modalDelMaster"><span class="glyphicon glyphicon-trash"></span> Delete</button>'+
                    '</td>'+
                    '<td class="delLink">'+urldel+'</td>'+

                '</tr>';
                tbl.append(newTr);
                var rowLS;
                $('tr').mouseover(function(){
                        rowLS = $(this).index();
                });
                $('.btnDel').unbind().click(function(){
                    $('#deleteSMConfirm').text(document.getElementById("masterList").rows[rowLS + 1].cells[0].textContent + " - " + document.getElementById("masterList").rows[rowLS + 1].cells[1].textContent);
                    $('#delSongMaster').prop('href',document.getElementById("masterList").rows[rowLS + 1].cells[5].textContent);
                });
            });
        });
    </script>

    <script type='text/javascript'> 
        $('.timepicker').timepicki({
            show_meridian:false,
            min_hour_value:0,
            max_hour_value:23,
            step_size_minutes:1,
            overflow_minutes:true,
            increase_direction:'up',
            disable_keyboard_mobile: true});
        </script>
        <script type="text/javascript">

            $('#setSongNumber').click(function(){            
                document.getElementById("songNumBadge").innerHTML = document.getElementById('songNumber').value;
                $.ajax({
                    type:'POST',
                    url:'./set_song_number',
                    data:{_token:'{{ csrf_token() }}'       
                    , songNumber:document.getElementById('songNumber').value
                },
                success:function(data){
                    alert(data);
                //return;
            }
        });
            });


            $('#setTime').click(function(){
                $.ajax({
                    type:'POST',
                    url:'./set_time',
                    data:{_token:'{{ csrf_token() }}'
                    , timeStart:document.getElementById('timeStart').value
                    , timeStop:document.getElementById('timeStop').value
                },
                success:function(data){
                    alert(data);
                //return;
            }
        });
            });

            $('#clearTimeBtn').click(function(){
                document.getElementById('timeStart').value = "";
                document.getElementById('timeStop').value = "";
            });

            var socket=io.connect('https://codeengine-nodejs.herokuapp.com');
        //var socket=io.connect('localhost:5000');

        $('#start').click(function(){
            //alert('start now');  
            var lRBtn = document.querySelector('input[name="listRBtn"]:checked').value;
            socket.emit('start', lRBtn); 
        });

        $('#stop').click(function(){            
            var r = confirm("Do you want to STOP now !");
            if (r == true) {
                socket.emit('stop', 'ok');
            }       
        });

    </script>
    <script type="text/javascript">
        var rowL;
        $('tr').mouseover(function(){
            rowL = $(this).index();
        });
        $('.btnDel').unbind().click(function(){
            $('#deleteSMConfirm').text(document.getElementById("masterList").rows[rowL + 1].cells[0].textContent + " - " + document.getElementById("masterList").rows[rowL + 1].cells[1].textContent);
            $('#delSongMaster').prop('href',document.getElementById("masterList").rows[rowL + 1].cells[5].textContent);
        });

        <?php 
        if($deletedId!=""){
            ?>
            $('#undoMaster').prop('href',"{{URL::route('masterundo',$deletedId)}}");
            $('#modalUndoMaster').modal('show');
            <?php 
        }
        ?>
        
    </script>
    <script type="text/javascript">
        $('.delAll').on('click',function(){
            $('#modalDelAll').modal('show');
        });
    </script>
</body>
</html>
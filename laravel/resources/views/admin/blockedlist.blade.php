<!DOCTYPE html>
<head>
<title>Blocked Songs</title> <!–Tiêu đề trang web–>
<meta name="viewport" content="width=device-width, initial-scale=1.0"> <!–Tự động nhận dạng thiết bị để co vào cho phù hợp–>
<meta charset="UTF-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="stylesheet" type="text/css" href="{!! asset('user/css/bootstrap.min.css') !!}" /> <!– Gọi đến thư viện Bootstrap để sử dụng–>
<link rel="stylesheet" type="text/css" href="{!! asset('user/css/style.css') !!}"/> <!– Đây là file CSS của bạn–>
<link rel="stylesheet" href="{!! asset('user/css/style1.css') !!}" type="text/css" /> <!– Đây là file CSS của bạn–>
<link rel="stylesheet" href="{!! asset('user/css/style2.css') !!}" type="text/css" /> 
<style type="text/css">
	.linkUnblock{
		display: none;
	}
    table {
            width: 100%;

        }

        thead, tbody, tr, td, th { display: block; }

        tr:after {
            content: ' ';
            display: block;
            visibility: hidden;
            clear: both;
        }

        thead th {
            height: 30px;
        }

        tbody {
            height: 800px;
            overflow-y: auto;
        }

        thead {
        }


        tbody td, thead th {
            width: 25%;
            float: left;
        }
/*
 *  STYLE 3
 */

#scrollTable::-webkit-scrollbar-track
{
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
    background-color: #F5F5F5;
}

#scrollTable::-webkit-scrollbar
{
    width: 6px;
    background-color: #F5F5F5;
}

#scrollTable::-webkit-scrollbar-thumb
{
    background-color: #AFAFAF;
}
</style>
</head>
<body>
<!– Code bạn đặt trong đây–>

<!--include header-->
@include('user.header')

<div class="wrapPage" style="height: 1000px">
@if($isListEmpty)
<h1>No song in list</h1>
@else
<form class="form-inline" style="margin-bottom: 20px">
    <input class="form-control form-sm" type="text" placeholder="Enter title or artist" onkeyup="showResult(this.value)">
</form>
<table class="table table-hover" id="myBlockSongTable" style="font-size: medium">
    <thead>	
        <tr>
            <th>Title</th>
            <th>Artist</th>
            <th>Added By</th>
            <th>Unblock</th>
            <th class="linkUnblock">linkUnblock</th>
        </tr>
    </thead>
    <tbody id="scrollTable">
	@foreach($listBlocked as $blockedsong)
		<?php 
			$urlunblock = URL::route('adminunblock', $blockedsong->id_song);
			$info = $blockedsong->id_song."**".$blockedsong->title."**".$blockedsong->artist."**".$blockedsong->code128."**".$blockedsong->code320;
			$url = URL::route("listentomusic",$info);
		?>
	    <tr id="{{$blockedsong->id}}">
	    	<td><a href="{{$url}}" style="color: #FE9131;"><strong>{{$blockedsong->title}}</strong></a></td>
	    	<td>{{$blockedsong->artist}}</td>
	    	<td>{{$blockedsong->userEmail}}</td>
	    	<td><button class="btn btn-success navbar-btn unblockBtn" data-toggle="modal" data-target="#modalUnblock"><span class="glyphicon glyphicon-ok"></span> Unblock</button></a></td>
	    	<td class="linkUnblock">{{$urlunblock}}</td>
	    </tr>
	@endforeach
	</tbody>
</table>
@endif
</div>

<div class="modal fade" id="modalUnblock" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <span class="glyphicon glyphicon-exclamation-sign" style="color: #449D44;display:inline-block"></span><h4 class="modal-title" style="color: #449D44;display:inline-block;margin-left: 5px;">Unblock</h4>
        </div>
        <div class="modal-body" style="font-size: medium">
          <p>Do you want to unblock <strong id="unblockConfirm" style="color: #449D44"></strong> ?</p>
        </div>
        <div class="modal-footer">
        	<a href="" id="unblockSong"><button type="button" class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Unblock</button></a>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        </div>
      </div>
      
    </div>
  </div>

<!--include footer-->
@include('user.footer')
<!– Kết thúc Code của bạn–>
    <!– jQuery (necessary for Bootstrap’s JavaScript plugins) –>
    <script src= "{!! asset('user/js/jquery-2.2.4.min.js') !!}"></script>
    <!– Include all compiled plugins (below), or include individual files as needed –>
    <script src= "{!! asset('user/js/bootstrap.min.js') !!}"></script>
    <script src="{!! asset('user/js/myScript.js') !!}"></script> 
    <script type="text/javascript">
    var rowTB;

    $('tr').mouseover(function(){
        rowTB = $(this).index();
    });
    $('.unblockBtn').unbind().click(function(){
   		$('#unblockConfirm').text(document.getElementById("myBlockSongTable").rows[rowTB + 1].cells[0].textContent + " - " + document.getElementById("myBlockSongTable").rows[rowTB + 1].cells[1].textContent);
   		$('#unblockSong').prop('href',document.getElementById("myBlockSongTable").rows[rowTB + 1].cells[4].textContent);
    });
    </script>
    <script type="text/javascript">
        function showResult(key){
            key = key.toLowerCase();
            var listBlocked = <?php if($isListEmpty==false) echo json_encode($listBlocked);?>;
            var saveId=[];
            var length=listBlocked.length;
            for(i=0;i<length;i++){
                $('#'+listBlocked[i].id).hide();
            }
            for(var i=0; i<length;i++){
                if((listBlocked[i].title.toLowerCase().indexOf(key)!=-1)||(listBlocked[i].artist.toLowerCase().indexOf(key)!=-1)){
                    $('#'+listBlocked[i].id).show();
                    saveId.push(listBlocked[i].id);
                }     
            }
        }
    </script>
</body>
</html>
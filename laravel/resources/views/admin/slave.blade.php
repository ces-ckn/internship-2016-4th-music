<!DOCTYPE html>
<head>
<title id="pageT">Slave</title> <!–Tiêu đề trang web–>
<meta name="viewport" content="width=device-width, initial-scale=1.0"> <!–Tự động nhận dạng thiết bị để co vào cho phù hợp–>
<meta charset="UTF-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="stylesheet" type="text/css" href="{!! asset('user/css/bootstrap.min.css') !!}" /> <!– Gọi đến thư viện Bootstrap để sử dụng–>
<link rel="stylesheet" type="text/css" href="{!! asset('user/css/style.css') !!}"/> <!– Đây là file CSS của bạn–>
<link rel="stylesheet" type="text/css" href="{!! asset('user/css/style1.css') !!}"/> <!– Đây là file CSS của bạn–>
<link rel="stylesheet" type="text/css" href="{!! asset('user/css/style2.css') !!}"/>
</head>
<body>
<!– Code bạn đặt trong đây–>

<!--include header-->
@include('user.header')

<div class="wrapPage" style="height: 700px">
	@if($isListEmpty)
		<h1>No Song In List</h1>
	@else
	<div class="form-horizontal formPlay">
		<div class="form-group">
            <label id="songTitle" style="font-size: 25px;color: #FE703E"></label>
    	</div>
    	<div class="form-group panel panel-warning" style="width: 750px;margin-bottom: 0px;">
		  		<div class="panel-heading playPlayList" style="height: 65px;padding-right:5px;">
		  		<audio id="sourceSong">
						<p>Your browser does not support HTML5 Audio</p>
						<?php
							$urlsource128 = 'http://api.mp3.zing.vn/api/mobile/source/song/'.$masterList[0]->code128;
						?>
						<source src="{{$urlsource128}}" type="audio/ogg">
				</audio>
	      			<div class="form-inline">
			  				<img src="{!! asset('user/images/previous.png') !!}" id="previous">	
							<img src="{!! asset('user/images/play1.png') !!}" id="playPause">
							<img src="{!! asset('user/images/next.png') !!}" id="next">
							<label id="currentTime" style="width: 20px;font-weight:normal;margin-left: 5px">00:00</label>
						<div class="form-group">
							<input id="seek" class="player--seek" type="range" min="0" value="0" style="width: 300px;">
						</div>
							<label id="durationTime" style="width: 20px;font-weight:normal;">00:00</label>	
						
							<img id="mutedMusic" src="{!! asset('user/images/volume.png') !!}">
						<div class="form-group">
			        		<input id="volume-seek" type="range" min="0" max="100" value="100" step="1" style="width: 70px;" style="display:inline-block;">
			        	</div>
			        		<img src="{!! asset('user/images/repeatAll.png') !!}" id="repeatSong">
			        </div>
	      		</div>
		</div>
		<div class="form-group panel panel-warning listSong" style="width: 750px;margin-top: 0px;">
				<?php 
					if(count($masterList) > 5){
				?>
						<div class="panel-body playListPanel" id="scrollList">
				<?php
					}
					else{
				?>
						<div class="panel-body shortplayList">
				<?php
					}
				?>
				<ul class="playListForm" id="playList" style="list-style: none;">
				@foreach($masterList as $song)
				<?php
					$urlsourceSlave = 'http://api.mp3.zing.vn/api/mobile/source/song/'.$song->code128;
				?>
					@if($song->position == 1)
			  		<li class="active">
                        <div class="form-inline plItem">
                        <a href="{{$urlsourceSlave}}"> 
                        <p id="{{$song->id_song}}" style="display:none">{{$song->id_song}}</p>
                            <div id="songInfo" class="form-group plTitle" value="{{$song->title}} - {{$song->artist}}" style="width: 450px">
                            	{{$song->title}}
                            </div>
                         </a>
                            <div class="form-group">
                            	{{$song->artist}}
                            </div>
                        </div>
                            <hr style="margin-top: 9px">
                    </li>
                    @else
                    <li>
                        <div class="form-inline plItem">
                            <a href="{{$urlsourceSlave}}">
                            <p id="{{$song->id_song}}" style="display:none">{{$song->id_song}}</p> 
                            <div id="songInfo" class="form-group plTitle" value="{{$song->title}} - {{$song->artist}}" style="width: 450px">
                            	{{$song->title}}
                            </div>
                         	</a>
                            <div class="form-group">
                            	{{$song->artist}}
                            </div>
                        </div>
                            <hr style="margin-top: 9px">
                    </li>
                    @endif
            	@endforeach
				</ul>
		</div>
	</div>
	@endif
</div>
</div>

@include('user.footer')
<!– Kết thúc Code của bạn–>
    <!– jQuery (necessary for Bootstrap’s JavaScript plugins) –>
    <script src= "{!! asset('user/js/jquery-2.2.4.min.js') !!}"></script>
    <!– Include all compiled plugins (below), or include individual files as needed –>
    <script src= "{!! asset('user/js/bootstrap.min.js') !!}"></script>
    <script src="{!! asset('user/js/myScript.js') !!}"></script>
    <script src="{!! asset('nodejs/jquery.js') !!}"></script>
	<script src="{!! asset('nodejs/socket.io.js') !!}"></script>
	<script src="{!! asset('user/js/jquery.slimscroll.js') !!}"></script>
    <script type="text/javascript">
    var socket=io.connect('https://codeengine-nodejs.herokuapp.com');
    var master = 0;
    <?php if(!Auth::guest()&&Auth::user()->admin){ ?>
    	var master = 1;
   	<?php } ?>
    console.log(master);
    var songS;
    var list;
    var audioSlave = $('#sourceSong');

    var currentS;
    var lenS;
    var linkS;
    $(document).ready(function(){
    	var titleSongS= $('songTitle');
		var volumeSeekS = document.getElementById('volume-seek');
		var seekS = document.getElementById('seek');
		var currentTimeS = document.getElementById('currentTime');
		var durationTimeS = document.getElementById('durationTime');
		var previousBtnS = document.getElementById('previous');
		var nextBtnS = document.getElementById('next');
		var muteBtnS = document.getElementById('mutedMusic');
		var repeatBtnS = document.getElementById('repeatSong');
		var playBtn = document.getElementById('playPause');
		var timeIntervalS;
		var repeatS;
		init();
		if(master != 1){
    		socket.on('getAction',function(msg){
    			console.log(msg);
    			switch(msg){
    				case 'play':
    				case 'pause':{
    					if (audioSlave[0].paused) {
						audioSlave[0].play();
						timeIntervalS = setInterval(function(){
					    	durationTimeS.innerHTML = secondToMinutes(audioSlave[0].duration);
							currentTimeS.innerHTML = secondToMinutes(audioSlave[0].currentTime);
							seekS.max = audioSlave[0].duration;
							seekS.value = audioSlave[0].currentTime;
					    }, 100);
						seekS.addEventListener('change', function(e){
					    	audioSlave[0].currentTime = seekS.value;
					    });
						$('#playPause').attr('src',"{!! asset('user/images/pause1.png') !!}");
				
						} else {
						audioSlave[0].pause();
						clearInterval(timeIntervalS);
						$('#playPause').attr('src',"{!! asset('user/images/play1.png') !!}");
						
						}
						break;
    				}
    				case 'next':{
    					if(currentS == lenS){
				    		currentS = 0;
				    		linkS = list.find('a')[currentS];
				    	}
				    	else{
				    		currentS++;
				    		linkS = list.find('a')[currentS];
				    	}
				    	run($(linkS),audioSlave[0]);
				    	break;
    				}
    				case 'previous':{
    					if(currentS == 0){
				    		currentS = lenS;
				    		linkS = list.find('a')[currentS];
				    	}
				    	else{
				    		currentS--;
				    		linkS = list.find('a')[currentS];
				    	}
				    	run($(linkS),audioSlave[0]);
				    	break;
    				}
    				case 'mute':
    				case 'unmute':{
    					if (audioSlave[0].muted) {
							audioSlave[0].muted = false;
							volumeSeekS.value = 100;
							audioSlave[0].volume = 1;
							$('#mutedMusic').attr('src',"{!! asset('user/images/volume.png') !!}");
							
						} else {
							audioSlave[0].muted = true;
							volumeSeekS.value = 0;
							$('#mutedMusic').attr('src',"{!! asset('user/images/mute.png') !!}");
							
						}
						break;
    				}
    				case 'repeat':{
    					if(repeatS == 0){
				    		repeatS = 1;
				    		$('#repeatSong').attr('src',"{!! asset('user/images/repeat.png') !!}");
				    	}
				    	else{
				    		repeatS = 0;
				    		$('#repeatSong').attr('src',"{!! asset('user/images/repeatAll.png') !!}");
				    	}
				    	break;
    				}
    				default:{
    					if(msg.indexOf("seek")>-1){
    						var arr = msg.split("==");
    						var time = parseInt(arr[1],10);
    						audioSlave[0].currentTime = time;
    						seekS.value=time;
    						break;
    					}else if(msg.indexOf("volume")>-1){
    						$('#mutedMusic').attr('src',"{!! asset('user/images/volume.png') !!}");
    						var arr = msg.split("==");
    						var volume = parseFloat(arr[1],10);
    						audioSlave[0].volume = volume;
    						volumeSeekS.value = volume * 100;
    						break;
    					}else{
    						var x = $('#'+msg);
    						var linkS = x.closest('a');
    						currentS = linkS.parent().parent().index();
							run(linkS, audioSlave[0]);
							break;
						}
    				}
    			}
    		});
    	}
		function init() {
			currentS = 0;
			repeatS = 0;
			list = $('#playList');
			songS = list.find('li a');
			songTitle.innerHTML = songS.find('#songInfo').attr('value');
			lenS = songS.length - 1;
			audioSlave[0].volume = 1;
			audioSlave[0].play();

			<?php if(!Auth::guest()&&Auth::user()->admin){ ?>
        		list.find('a').click(function (e) {
					e.preventDefault();
					linkS = $(this);
					currentS = linkS.parent().parent().index();
					console.log(currentS);
					run(linkS, audioSlave[0]);
					var idSong = $(this).children('p').html();
					socket.emit('sendAction',idSong);
				});
				seekS.addEventListener('input', function(e){
				    audioSlave[0].currentTime = seekS.value;
				    var para = "seek=="+seekS.value.toString();
				    socket.emit('sendAction',para);
				});
				volumeSeekS.addEventListener('change',function(e){
				    audioSlave[0].volume = volumeSeekS.value/100;
				    if(volumeSeekS.value == 0){
				    	$('#mutedMusic').attr('src',"{!! asset('user/images/mute.png') !!}");
				    	socket.emit('sendAction','mute');
				    }
				    else{
				    	$('#mutedMusic').attr('src',"{!! asset('user/images/volume.png') !!}");
				    	var para = "volume=="+ ((volumeSeekS.value)/100).toString();
				    	socket.emit('sendAction',para);
				    }
				});
				playBtn.addEventListener('click', function(e){
				    if (audioSlave[0].paused) {
						audioSlave[0].play();
						timeIntervalS = setInterval(function(){
					    	durationTimeS.innerHTML = secondToMinutes(audioSlave[0].duration);
							currentTimeS.innerHTML = secondToMinutes(audioSlave[0].currentTime);
							seekS.max = audioSlave[0].duration;
							seekS.value = audioSlave[0].currentTime;
					    }, 100);
						seekS.addEventListener('change', function(e){
					    	audioSlave[0].currentTime = seekS.value;
					    });
						$('#playPause').attr('src',"{!! asset('user/images/pause1.png') !!}");
						socket.emit('sendAction','play');
					} else {
						audioSlave[0].pause();
						clearInterval(timeIntervalS);
						$('#playPause').attr('src',"{!! asset('user/images/play1.png') !!}");
						socket.emit('sendAction','pause');
					}
					
				});
				previousBtnS.addEventListener('click',function(e){
				    	if(currentS == 0){
				    		currentS = lenS;
				    		linkS = list.find('a')[currentS];
				    	}
				    	else{
				    		currentS--;
				    		linkS = list.find('a')[currentS];
				    	}
				    	run($(linkS),audioSlave[0]);
				    	socket.emit('sendAction','previous');
				});
				nextBtnS.addEventListener('click',function(e){
				    	if(currentS == lenS){
				    		currentS = 0;
				    		linkS = list.find('a')[currentS];
				    	}
				    	else{
				    		currentS++;
				    		linkS = list.find('a')[currentS];
				    	}
				    	run($(linkS),audioSlave[0]);
				    	socket.emit('sendAction','next');
				});
				muteBtnS.addEventListener('click',function(e){
				    	if (audioSlave[0].muted) {
							audioSlave[0].muted = false;
							volumeSeekS.value = 100;
							audioSlave[0].volume = 1;
							$('#mutedMusic').attr('src',"{!! asset('user/images/volume.png') !!}");
							socket.emit('sendAction','unmute');
						} else {
							audioSlave[0].muted = true;
							volumeSeekS.value = 0;
							$('#mutedMusic').attr('src',"{!! asset('user/images/mute.png') !!}");
							socket.emit('sendAction','mute');
						}
						

				});
				repeatBtnS.addEventListener('click',function(e){
				    	if(repeatS == 0){
				    		repeatS = 1;
				    		$('#repeatSong').attr('src',"{!! asset('user/images/repeat.png') !!}");
				    	}
				    	else{
				    		repeatS = 0;
				    		$('#repeatSong').attr('src',"{!! asset('user/images/repeatAll.png') !!}");
				    	}
				    	socket.emit('sendAction','repeat');
				});

        	<?php } 
        		else{
        	?>
        		list.find('a').click(function (e) {
					e.preventDefault();
				});
				$('#seek').prop('disabled', true); 
				$('#volume-seek').prop('disabled', true);
        	<?php
        		}
        	?>

			audioSlave[0].onplay = function() {
				$('#playPause').attr('src',"{!! asset('user/images/pause1.png') !!}");
			};
			audioSlave[0].onpause = function() {
				$('#playPause').attr('src',"{!! asset('user/images/play1.png') !!}");
			};

			timeIntervalS = setInterval(function(){
				durationTimeS.innerHTML = secondToMinutes(audioSlave[0].duration);
				currentTimeS.innerHTML = secondToMinutes(audioSlave[0].currentTime);
				seekS.max = audioSlave[0].duration;
				seekS.value = audioSlave[0].currentTime;
			}, 100);

			audioSlave[0].addEventListener('ended', function (e) {
				if(repeatS == 1){
				    linkS = list.find('a')[currentS];
				    run($(linkS),audioSlave[0]);
				}
				else{    
					currentS++;
					console.log("current: " + currentS);
					console.log("len: " + lenS);
					if (currentS == lenS + 1) {
					    currentS = 0;
					    linkS = list.find('a')[0];
					} else {
					    linkS = list.find('a')[currentS];
					}
				    run($(linkS), audioSlave[0]);
				}

			});

		}
		function run(link, player) {
				    player.src = link.attr('href');
				    songTitle.innerHTML = link.find('#songInfo').attr('value');
				    $('#pageT').text(link.find('#songInfo').attr('value'));
				    console.log(link.find('#songInfo').attr('value'));
				    par = link.parent().parent();
				    par.addClass('active').siblings().removeClass('active');
				    audioSlave[0].load();
				    audioSlave[0].play();
		}
	
		// Convert Seconds to Minutes
		function secondToMinutes(seconds) {
			if(isNaN(seconds)){
				return "00:00";
			}
			else{
				var numMinutes = Math.floor((((seconds % 31536000) % 86400) % 3600) / 60),
						numSeconds = Math.round((((seconds % 3153600) % 86400) % 3600) % 60);
	
				if (numSeconds >= 10) {
					if(numSeconds == 60){
						var i = numMinutes + 1
						if(i >= 10){
							return i + ":" + "00";
						}
						else{
							return "0" + i + ':' + "00";
						}
					}
					else{
						if(numMinutes >= 10){
							return numMinutes + ':' + numSeconds;
						}
						else{
							return "0" + numMinutes + ':' + numSeconds;
						}
					}
				} 
				else {
						if(numMinutes >= 10){
							return numMinutes + ':0' + numSeconds;
						}
						else{
							return "0" + numMinutes + ':0' + numSeconds;
						}
				}
			}
		}

    })
    </script>

    <script>
    	var socket1=io.connect('https://codeengine-nodejs.herokuapp.com');
    	//var socket1=io.connect('localhost:5000');
	    window.onload = function () {
			audioSlave[0].pause();	     	
	     	socket1.emit('loaded', 'ok'); 
	 	}
	 	socket1.on('readyToPlay', function (){
	 		audioSlave[0].play();
	 	});
    </script>

    <script type="text/javascript">
    	$('#scrollList').slimScroll({
		height: 'auto',
		color: '#FAD6AC',
		size: '10px',
		railVisible: true,
	    railColor: '#F9C393',
	    alwaysVisible: true,
	});

    </script>
</body>
</html>
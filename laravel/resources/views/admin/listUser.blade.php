<head>
<title>Users</title> <!–Tiêu đề trang web–>
<meta name="viewport" content="width=device-width, initial-scale=1.0"> <!–Tự động nhận dạng thiết bị để co vào cho phù hợp–>
<meta charset="UTF-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="stylesheet" type="text/css" href="{!! asset('user/css/bootstrap.min.css') !!}" /> <!– Gọi đến thư viện Bootstrap để sử dụng–>
<link rel="stylesheet" type="text/css" href="{!! asset('user/css/style.css') !!}"/> <!– Đây là file CSS của bạn–>
<link rel="stylesheet" type="text/css" href="{!! asset('user/css/bootstrap-switch.css') !!}"/>
<link rel="stylesheet" href="{!! asset('user/css/style1.css') !!}" type="text/css" /> <!– Đây là file CSS của bạn–>
<link rel="stylesheet" href="{!! asset('user/css/style2.css') !!}" type="text/css" /> 
<style type="text/css">
    table {
            width: 100%;
        }

        thead, tbody, tr, td, th { display: block; }

        tr:after {
            content: ' ';
            display: block;
            visibility: hidden;
            clear: both;
        }

        thead th {
            height: 30px;
        }

        tbody {
            height: 800px;
            overflow-y: auto;
        }

        thead {
        }

        tbody td, thead th {
            width: 10%;
            float: left;
        }
        .id{
            width: 60px;
        }
        .userMail{
            width: 350px;
        }
        .name{
            width: 250px;
        }
/*
 *  STYLE 3
 */

#scrollTable::-webkit-scrollbar-track
{
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
    background-color: #F5F5F5;
}

#scrollTable::-webkit-scrollbar
{
    width: 6px;
    background-color: #F5F5F5;
}

#scrollTable::-webkit-scrollbar-thumb
{
    background-color: #AFAFAF;
}
</style>
</head>
<body>
<!– Code bạn đặt trong đây–>

<!--include header-->
@include('user.header')

<div class="wrapPage" style="height: 1000px">
@if($isListEmpty)
<h1>No User in list</h1>
@else
<form class="form-inline">
    <input class="form-control form-sm" type="text" placeholder="Enter name or email" onkeyup="showResult(this.value)">
</form>
	<table class="table table-hover">
                    <thead>	
                        <tr>
                            <th class="id">ID</th>
                            <th class="name">Name</th>
                            <th class="userMail">Email</th>
                            <th>Activated</th>
                            <th class="block">Blocked</th>
                            @if(Auth::user()->superadmin)
                            <th class="adminize">Admin</th>
                            @endif
                        </tr>
                    </thead>
                    <tbody id="scrollTable">
        @foreach($userList as $user)
        <form action ="{{ URL::to('userlist/adminizeuser') }}" method="POST">
        {{ csrf_field() }}
                        <tr id="{{$user->id}}">
                            <td class="id"><input name="id" type="text" class="hidden" value="{{$user->id}}"/><strong>{{$user->id}}</strong></td>
                            <td class="name">{{$user->name}}</td>
                            <td class="userMail"><strong>{{$user->email}}</strong></td>
							@if($user->activated)
								<td><img src="{!! asset('user/images/check.png') !!}" width="24" height="24"></td>
							@else
								<td><img src="{!! asset('user/images/cross.png') !!}" width="24" height="24"></td>
							@endif
                            @if($user->blocked)
                                <td class="block"><input id="blocked" type="checkbox" name="my-checkbox" data-off-color="danger" data-on-color="success" data-size="mini" data-on-text="Yes" data-off-text="No" checked="false"></td>
                            @else
                                <td class="block"><input id="blocked" type="checkbox" name="my-checkbox" data-off-color="danger" data-on-color="success" data-size="mini" data-on-text="Yes" data-off-text="No"></td>
                            @endif
                            @if(Auth::user()->superadmin)
                                @if($user->admin)
                                <td class="adminize"><input id="admin" type="checkbox" name="my-checkbox" data-off-color="danger" data-on-color="success" data-size="mini" data-on-text="Yes" data-off-text="No" checked="false"></td>
                                @else
                                <td class="adminize"><input id="admin" type="checkbox" name="my-checkbox" data-off-color="danger" data-on-color="success" data-size="mini" data-on-text="Yes" data-off-text="No"></td>
                                @endif
                            @endif
                        </tr>
        </form>
        @endforeach
        			</tbody>
    </table>
@endif
</div>

<!--Modal Message-->
   <div class="modal fade" id="modalMessage" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" id="messageHead"></h4>
        </div>
        <div class="modal-body" style="font-size: medium">
            <strong style="display:inline-block"><p id="mailUser"></p></strong><p id="messageContent" style="display:inline-block;margin-left: 5px;"></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
        </div>
      </div>
      
    </div>
  </div>


<!--include footer-->
@include('user.footer')
<!– Kết thúc Code của bạn–>
    <!– jQuery (necessary for Bootstrap’s JavaScript plugins) –>
    <script src= "{!! asset('user/js/jquery-2.2.4.min.js') !!}"></script>
    <!– Include all compiled plugins (below), or include individual files as needed –>
    <script src= "{!! asset('user/js/bootstrap.min.js') !!}"></script>
    <script src="{!! asset('user/js/myScript.js') !!}"></script> 
    <script src="{!! asset('user/js/bootstrap-switch.js') !!}"></script> 
    <script type="text/javascript">
        var idUser;
        var userMail;
        $("[name='my-checkbox']").bootstrapSwitch();
        $('input[id="blocked"]').on('switchChange.bootstrapSwitch', function(event, state) {
                if(state == true){
                    var url ="{{ URL::to('userlist/blockuser') }}" ;
                    var ob = $('#'+idUser).find('#admin');
                    if(ob.bootstrapSwitch('state')==true){
                        ob.bootstrapSwitch('toggleState', false);
                    }
                    $.ajax({
                        type:'POST',
                        url:url,
                        data:{_token:'<?php echo csrf_token() ?>',
                            id:idUser},
                        success:function(data){
                            if(data.result == "Success"){
                                $('#messageHead').text('Block User');
                                $('#messageHead').css('color','#449D44');
                                $('#mailUser').text(userMail);
                                $('#mailUser').css('color','#449D44');
                                $('#messageContent').text('is blocked!');
                                $('#modalMessage').modal('show');
                            }
                            else{
                                alert(data.result);
                            }
                        }
                    });
                }
                else{
                    var url ="{{ URL::to('userlist/unblockuser') }}" ;
                    //console.log($('#admin').bootstrapSwitch('state'));
                    $.ajax({
                        type:'POST',
                        url:url,
                        data:{_token:'<?php echo csrf_token() ?>',
                            id:idUser},
                        success:function(data){
                            if(data.result == "Success"){
                                $('#messageHead').text('Unblock User');
                                $('#messageHead').css('color','#C9302C');
                                $('#mailUser').text(userMail);
                                $('#mailUser').css('color','#C9302C');
                                $('#messageContent').text('is unblocked!');
                                $('#modalMessage').modal('show');
                            }
                            else{
                                alert('Sorry, User can not be unblocked!');
                            }
                        }
                    });

                }
            }
        );
        $('input[id="admin"]').on('switchChange.bootstrapSwitch', function(event, state) {
                if(state == true){
                    var url ="{{ URL::to('userlist/adminizeuser') }}" ;
                    var ob = $('#'+idUser).find('#blocked');
                    if(ob.bootstrapSwitch('state')==true){
                        ob.bootstrapSwitch('toggleState', false);
                    }
                    $.ajax({
                        type:'POST',
                        url:url,
                        data:{_token:'<?php echo csrf_token() ?>',
                            id:idUser},
                        success:function(data){
                            if(data.result == "Success"){
                                $('#messageHead').text('Adminize User');
                                $('#messageHead').css('color','#449D44');
                                $('#mailUser').text(userMail);
                                $('#mailUser').css('color','#449D44');
                                $('#messageContent').text('becomes admin now!');
                                $('#modalMessage').modal('show');
                            }
                            else{
                                alert(data.result);
                            }
                        }
                    });
                }
                else{
                    var url ="{{ URL::to('userlist/unadminizeuser') }}" ;
                    $.ajax({
                        type:'POST',
                        url:url,
                        data:{_token:'<?php echo csrf_token() ?>',
                            id:idUser},
                        success:function(data){
                            if(data.result == "Success"){
                                $('#messageHead').text('Unadminize User');
                                $('#messageHead').css('color','#C9302C');
                                $('#mailUser').text(userMail);
                                $('#mailUser').css('color','#C9302C');
                                $('#messageContent').text('is not admin anymore!');
                                $('#modalMessage').modal('show');
                            }
                            else{
                                alert('Sorry, You cannot do that!');
                            }
                        }
                    });

                }
            }
        );
        $('tr').mouseover(function(){
            idUser = $(this).find('td:first-child').text();
            userMail = $(this).find('.userMail').text();
        });
        
    </script>
    <script type="text/javascript">
        function showResult(key){
            key = key.toLowerCase();
            var userList = <?php if($isListEmpty==false) echo json_encode($userList);?>;
            var saveId=[];
            var length=userList.length;
            for(i=0;i<length;i++){
                $('#'+userList[i].id).hide();
            }
            for(var i=0; i<length;i++){
                if((userList[i].name.toLowerCase().indexOf(key)!=-1)||(userList[i].email.toLowerCase().indexOf(key)!=-1)){
                    $('#'+userList[i].id).show();
                    saveId.push(userList[i].id);
                }     
            }
        }
    </script>
</body>
</html>
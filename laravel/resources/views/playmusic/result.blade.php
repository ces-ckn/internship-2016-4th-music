<?php 
use App\SongSearch;
$result = json_decode($content,true);
$songs = $result['docs'];
$len = count($songs);
?>

<!DOCTYPE html>
<head>
<title>Search</title> <!–Tiêu đề trang web–>
<meta name="viewport" content="width=device-width, initial-scale=1.0"> <!–Tự động nhận dạng thiết bị để co vào cho phù hợp–>
<meta charset="UTF-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="stylesheet" type="text/css" href="{!! asset('user/css/bootstrap.min.css') !!}" /> <!– Gọi đến thư viện Bootstrap để sử dụng–>
<link rel="stylesheet" type="text/css" href="{!! asset('user/css/style.css') !!}"/> <!– Đây là file CSS của bạn–>
<link rel="stylesheet" href="{!! asset('user/css/style1.css') !!}" type="text/css" /> <!– Đây là file CSS của bạn–>
</head>
<body>
<!– Code bạn đặt trong đây–>

<!--include header-->
@include('user.header')

<ul>
<?php
	for($i=0;$i<$len;$i++){
		$songx = new SongSearch($songs[$i]);
		$song_id = $songx->song_id;
		$url = URL::route("listentomusic",$song_id); ?>
		<li><a href="{{$url}}">{{$songx->title}}||{{$songx->artist}}||{{$songx->genre}}||{{$songx->total_play}}</a></li>
<?php	}
?>
</ul>

<div class="wrapPage" style="height: 1000px;">
	<div class="col-md-8">
    <div class = "contentHead">Number of results: </div>
<?php
	for($i=0;$i<$len;$i++){
		$songx = new SongSearch($songs[$i]);
		$song_id = $songx->song_id;
		$url = URL::route("listentomusic",$song_id); ?>
    <div class="form-group songItem">
      <div class="form-group" style="margin-bottom: 5px;">
        <div class="form-inline">
          <div class="form-group">
            <h4><a href="">{{$songx->title}} - </a></h4>
          </div>
          <div class="form-group">
            <h4>{{$songx->artist}}</h4>
          </div>
        </div>
      </div>
      <div class="form-inline">
          <div class="form-group">Genre: {{$songx->genre}}</div>
          <div class="form-group navbar-right">
              @if (!Auth::guest())
                <a href="#"><img src="{!! asset('user/images/add.png') !!}" data-toggle="tooltip" title="Add"></a>
              @else
                <img src="{!! asset('user/images/add.png') !!}" data-toggle="tooltip" title="Login to add">
              @endif
          </div>
      </div>
      <div class="form-group" style="margin-top: 5px;">
       • Plays: {{$songx->total_play}}
      </div>
    </div><!--end-songItem-->
<?php	}
?>
    <hr>

  </div>

  <div class="col-md-4 sidebar">
  </div>



</div>

<!--include footer-->
@include('user.footer')
<!– Kết thúc Code của bạn–>
    <!– jQuery (necessary for Bootstrap’s JavaScript plugins) –>
    <script src= "{!! asset('user/js/jquery-2.2.4.min.js') !!}"></script>
    <!– Include all compiled plugins (below), or include individual files as needed –>
    <script src= "{!! asset('user/js/bootstrap.min.js') !!}"></script>
    <script src="{!! asset('user/js/myScript.js') !!}"></script>    
    <script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();   
    });
    </script>
</body>
</html>
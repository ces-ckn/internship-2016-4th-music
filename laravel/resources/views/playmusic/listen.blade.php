<!DOCTYPE html>
<html>
<head>
<title>listen to music</title>
<style>
.dropbtn {
    background-color: #4CAF50;
    color: white;
    padding: 16px;
    font-size: 16px;
    border: none;
    cursor: pointer;
}

.dropbtn:hover, .dropbtn:focus {
    background-color: #3e8e41;
}

.dropdown {
    position: relative;
    display: inline-block;
}

.dropdown-content {
    display: none;
    position: absolute;
    background-color: #f9f9f9;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
}

.dropdown-content a {
    color: black;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
}

.dropdown-content a:hover {background-color: #f1f1f1}

.show {display:block;}
</style>
</head>
<body>
	<?php
	$urlsource = $songlisten->source['128'];
	?>
<audio id='audio' controls autoplay loop>
  <source id='sourceogg' src="{{$urlsource}}" type="audio/ogg">
  <source id='sourcempeg' src="{{$urlsource}}" type="audio/mpeg">
Your browser does not support the audio element.
</audio> 
<select id='quality' onchange='changeQuality()' name='quality'>
 	<option selected='selected' value="{{$songlisten->source['128']}}">128kbps</option>
  	<option value="{{$songlisten->source['320']}}">320kbps</option>
  	<option value="{{$songlisten->source['lossless']}}">Lossless</option>
</select>
 <div class="dropdown">
  <button onclick="dropdownbtn()" class="dropbtn">Download</button>
  <div id="myDropdown" class="dropdown-content">
    <a href="{{$songlisten->link_download['128']}}">128kbps</a>
    <a href="{{$songlisten->link_download['320']}}">320kbps</a>
    <a href="{{$songlisten->link_download['lossless']}}">Lossless</a>
  </div>
</div>
<?php $url = URL::route("addsong",$songlisten->song_id);?>
<form method="POST" action="{{$url}}">
  <input type="hidden" name="_token" value="{{csrf_token()}}">
  <button type="submit">Add</button>
</form>

<script type="text/javascript">

  function add(){
    
  }
	function changeQuality(){

		var e = document.getElementById("quality");
		var quality = e.options[e.selectedIndex].value;
		console.log(quality);
		var audio = document.getElementById('audio');

        var sourceogg = document.getElementById('sourceogg');
        var sourcempeg = document.getElementById('sourcempeg');
        sourceogg.src=quality;
        sourcempeg.src=quality;
        audio.load(); 
        audio.play();
	}

	function dropdownbtn () {
    	document.getElementById("myDropdown").classList.toggle("show");
	}


	window.onclick = function(event) {
  		if (!event.target.matches('.dropbtn')) {

    	var dropdowns = document.getElementsByClassName("dropdown-content");
    	var i;
    	for (i = 0; i < dropdowns.length; i++) {
      		var openDropdown = dropdowns[i];
     		if (openDropdown.classList.contains('show')) {
        		openDropdown.classList.remove('show');
      		}
    	}
  	}
  }

  
</script>
</body>
</html>
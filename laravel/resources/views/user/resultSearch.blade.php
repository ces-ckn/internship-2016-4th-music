<!DOCTYPE html>
<head>
<title>Search</title> <!–Tiêu đề trang web–>
<meta name="viewport" content="width=device-width, initial-scale=1.0"> <!–Tự động nhận dạng thiết bị để co vào cho phù hợp–>
<meta charset="UTF-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="stylesheet" type="text/css" href="{!! asset('user/css/bootstrap.min.css') !!}" /> <!– Gọi đến thư viện Bootstrap để sử dụng–>
<link rel="stylesheet" type="text/css" href="{!! asset('user/css/style.css') !!}"/> <!– Đây là file CSS của bạn–>
<link rel="stylesheet" href="{!! asset('user/css/style1.css') !!}" type="text/css" /> <!– Đây là file CSS của bạn–>
</head>
<body>
<!– Code bạn đặt trong đây–>

<!--include header-->
@include('user.header')

<div class="wrapPage" style="height: 2500px">
  @if($isResultEmpty == false)
    <div class="col-md-8">
      <?php
      foreach($songxs as $songx){
        $info = $songx->song_id."**".$songx->title."**".$songx->artist."**".$songx->code128."**".$songx->code320;
        $url = URL::route("listentomusic",$info); 
      ?>
      <div id="{{$songx->song_id}}" class="form-group songItem">
          <div class="form-group" style="margin-bottom: 5px;">
            <div class="form-inline">
              <div class="form-group">
                <h4><a href="{{$url}}" style="color: #FE9131">{{$songx->title}} - </a></h4>
              </div>
              <div class="form-group">
                <h4>{{$songx->artist}}</h4>
              </div>
            </div>
          </div>
      <div class="form-inline">
          <div class="form-group">Genre: {{$songx->genre}}</div>
          <div class="form-group navbar-right">
              @if (!Auth::guest())
                <?php  $url = URL::route("addsong",$songx->song_id);?>
                @if($songx->isBlocked())
                <p>Blocked</p>
                @elseif(!$songx->isInList())
                <a class="addBtn" href="javascript:void(0)"><button class="btn btn-warning" data-toggle="tooltip" title="Add">Add to Playlist</button>
                <p id="songId" style="display:none">{{$songx->song_id}}</p>
                <p id="title" style="display:none">{{$songx->title}}</p>
                <p id="artist" style="display:none">{{$songx->artist}}</p>
                <p id="code128" style="display:none">{{$songx->code128}}</p>
                <p id="code320" style="display:none">{{$songx->code320}}</p></a>
                <p hidden>Added to the list</p>
                @else
                <p>Added to the list</p>
                @endif
                @if(Auth::user()->admin)
                  <?php $urlmasteradd = URL::route('masteradd', $info);?>
                  <a href={{$urlmasteradd}}><button class="btn btn-warning navbar-btn"><span class="glyphicon glyphicon-king"></span> Add to master list</button></a>
                @endif
              @else
                <button class="btn btn-warning" data-toggle="tooltip" title="Sign In to add" disabled>Add to Playlist</button>
              @endif
          </div>
      </div>
      <div class="form-group" style="margin-top: 5px;">
       • Plays: {{$songx->total_play}} times
      </div>
    </div><!--end-songItem-->
    <hr>

<?php 
  }
?>
  <div>{!! $songxs->appends(array('searchname' => $name))->links() !!}</div>
  @else
    <h1>Sorry. No result was found.</h1>
  @endif
  </div>

  <div class="col-md-4 sidebar">
  </div>

</div>


<!--include footer-->
@include('user.footer')
<!– Kết thúc Code của bạn–>
    <!– jQuery (necessary for Bootstrap’s JavaScript plugins) –>
    <script src= "{!! asset('user/js/jquery-2.2.4.min.js') !!}"></script>
    <!– Include all compiled plugins (below), or include individual files as needed –>
    <script src= "{!! asset('user/js/bootstrap.min.js') !!}"></script>
    <script src="{!! asset('user/js/myScript.js') !!}"></script>
    <script src="nodejs/jquery.js"></script>
    <script src="nodejs/socket.io.js"></script>    
    <script>
    $(document).ready(function(){
      //$('[data-toggle="tooltip"]').tooltip();
    });
    $(document).ready(function(){
      
      var socket=io.connect('https://codeengine-nodejs.herokuapp.com');
      socket.on('addresult',function(data){
        var songId = data['songId'];
        var obj = $('#'+songId);
        if(obj!=undefined){
          var btnObj = obj.find('.addBtn');
          $(btnObj).hide();
          $(btnObj).siblings('p').show();
          }
        });
      $(".addBtn").click(function(){
          var songId = $(this).children('#songId').text();
          var title = $(this).children('#title').text();
          var artist = $(this).children('#artist').text();
          var code128 = $(this).children('#code128').text();
          var code320 = $(this).children('#code320').text();
          var obj = this;
          $.ajax({
            type:'POST',
            url:'./listen/addajax',
            data:{_token:'<?php echo csrf_token() ?>',
            songId:songId,
            title:title,
            artist:artist,
            code128:code128,
            code320:code320
          },
            success:function(data){
            }
          });
        });
    });
    </script>
</body>
</html>
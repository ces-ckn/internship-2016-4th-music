<!DOCTYPE html>
<head>
<title>Template</title> <!–Tiêu đề trang web–>
<meta name="viewport" content="width=device-width, initial-scale=1.0"> <!–Tự động nhận dạng thiết bị để co vào cho phù hợp–>
<meta charset="UTF-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="stylesheet" type="text/css" href="{!! asset('user/css/bootstrap.min.css') !!}" /> <!– Gọi đến thư viện Bootstrap để sử dụng–>
<link rel="stylesheet" type="text/css" href="{!! asset('user/css/style.css') !!}"/> <!– Đây là file CSS của bạn–>
</head>
<body>
<!– Code bạn đặt trong đây–>
<header>
	<div class="container group">
			<a href="{{ url('') }}"><img src="{!! asset('user/images/logo.png') !!}" id="logo"></a>
			<form class="input-group" id="searchForm">
			      <input type="search" class="form-control" placeholder="Search">
			      <span class="input-group-btn">
			        <button class="btn btn-default" type="button"><span class="glyphicon glyphicon-search" aria-hidden="true"></span>
			        </button>
			      </span>
			</form><!-- /input-group -->

					<div class="navbar-right" id="navUser">
						<div class="dropdown">
						 	<h4 class="dropdown-toggle" id="dropdownUser" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
						 	<span class="glyphicon glyphicon-user" aria-hidden="true"></span>
							    NaNa
							<span class="caret"></span>
						 	</h4>	
						  <ul class="dropdown-menu" aria-labelledby="dropdownUser">
						    <li><a href="#"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>     Update Profile</a></li>
						    <li><a href="#"><img src="{!! asset('user/images/logout.png') !!}">     Log Out</a></li>
						  </ul>
						</div>						    
					</div>

	</div>
</header>

	<div id="menu">
		<nav class="navbar navbar-default" role="navigation">
			<!– Brand and toggle get grouped for better mobile display –>
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1″>
				<span class="sr-only"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				</button>
				<!--<a class="navbar-brand" href="#">HOT</a>-->
			</div>
			<!– Collect the nav links, forms, and other content for toggling –>
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
				<li id="hot"><a href="#">Hot</a></li>
				<li><a href="#">Music</a></li>
				</ul>
			</div><!– /.navbar-collapse –>
		</nav>
	</div>

  <div class="wrapPage" style="height: 1000px;">
 </div>

<!--include footer-->
@include('user.footer')

<!– Kết thúc Code của bạn–>
<!– jQuery (necessary for Bootstrap’s JavaScript plugins) –>
    <script src= "{!! asset('user/js/jquery-2.2.4.min.js') !!}"></script>
    <!– Include all compiled plugins (below), or include individual files as needed –>
    <script src= "{!! asset('user/js/bootstrap.min.js') !!}"></script>
    <script src="{!! asset('user/js/myScript.js') !!}"></script>
</body>
</html>
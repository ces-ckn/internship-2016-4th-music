<!DOCTYPE html>
<head>
<title>Added Songs</title> <!–Tiêu đề trang web–>
<meta name="viewport" content="width=device-width, initial-scale=1.0"> <!–Tự động nhận dạng thiết bị để co vào cho phù hợp–>
<meta charset="UTF-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="stylesheet" type="text/css" href="{!! asset('user/css/bootstrap.min.css') !!}" /> <!– Gọi đến thư viện Bootstrap để sử dụng–>
<link rel="stylesheet" type="text/css" href="{!! asset('user/css/style.css') !!}"/> <!– Đây là file CSS của bạn–>
<link rel="stylesheet" href="{!! asset('user/css/style1.css') !!}" type="text/css" /> <!– Đây là file CSS của bạn–>
<link rel="stylesheet" href="{!! asset('user/css/style2.css') !!}" type="text/css" /> 
<style type="text/css">
  .linkDel{
    display: none;
  }
  table {
            width: 100%;

        }

        thead, tbody, tr, td, th { display: block; }

        tr:after {
            content: ' ';
            display: block;
            visibility: hidden;
            clear: both;
        }

        thead th {
            height: 30px;
        }

        tbody {
            height: 800px;
            overflow-y: auto;
        }

        thead {
        }


        tbody td, thead th {
            /*width: 25%;*/
            float: left;
        }
        .rating{
          width: 70px;
        }
        .title{
          width: 450px;
        }
        .delete{
          width: 90px;
        }
        .artist{
          width: 330px;
        }
/*
 *  STYLE 3
 */

#scrollTable::-webkit-scrollbar-track
{
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
    background-color: #F5F5F5;
}

#scrollTable::-webkit-scrollbar
{
    width: 6px;
    background-color: #F5F5F5;
}

#scrollTable::-webkit-scrollbar-thumb
{
    background-color: #AFAFAF;
}
</style>
</head>
<body>
<!– Code bạn đặt trong đây–>

<!--include header-->
@include('user.header')
<div class="wrapPage" style="height: 1000px">

@if($isListEmpty)
<h1>U did not add any song. Go search and add a song yourself</h1>
@else
<form class="form-inline" style="margin-bottom: 20px">
    <input class="form-control form-sm" type="text" placeholder="Enter title or artist" onkeyup="showResult(this.value)">
</form>
<table class="table table-hover" id="mySongUserTable" style="font-size: medium">
   <thead>  
        <tr>
            <th class="rating">Rating</th>
            <th class="title">Title</th>
            <th class="artist">Artist</th>
            <th class="delete">Delete</th>
            <th class="linkDel">linkdel</th>
        </tr>
    </thead>
    <tbody id="scrollTable">
	@foreach($listAdded as $songAdded)
			<?php
				$info = $songAdded->id_song."**".$songAdded->title."**".$songAdded->artist."**".$songAdded->code128."**".$songAdded->code320;
				$url = URL::route("listentomusic",$info);
				$urldel = URL::route("deletesong", $songAdded->id_song);
			?>
	    <tr id="{{$songAdded->id}}">
          <td class="rating"><strong>{{$songAdded->position}}</strong></td>
          <td class="title"><a href="{{$url}}" title="" style="color: #FE9131;"><strong>{{$songAdded->title}}</strong></a></td>
          <td class="artist">{{$songAdded->artist}}</td>
          <td class="delete">
          @if($songAdded->voted == true)
            <button disabled="true" class="btn btn-danger" data-toggle="tooltip" title="This song is voted by other users"><span class="glyphicon glyphicon-trash"></span> Delete</button>
          @else
            <button class="btn btn-danger btnDelete" data-toggle="modal" data-target="#modalDelete"><span class="glyphicon glyphicon-trash"></span> Delete</button>
          @endif
          </td>
          <td class="linkDel">{{$urldel}}</td>
      </tr>
	@endforeach
  </tbody>
  </table>
@endif
</div>

<div class="modal fade" id="modalDelete" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <span class="glyphicon glyphicon-alert" style="color: #C9302C;display:inline-block"></span><h4 class="modal-title" style="color: #C9302C;display:inline-block;margin-left: 5px;">Delete</h4>
        </div>
        <div class="modal-body" style="font-size: medium">
          <p>Do you want to delete <strong id="SongConfirm" style="color: #C9302C"></strong> ?</p>
        </div>
        <div class="modal-footer">
        	<a href="" id="del"><button type="button" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> Delete</button></a>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        </div>
      </div>
      
    </div>
</div>

  <!-- Modal Undo -->
  <div class="modal fade" id="modalUndoS" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" style="color: #31B0D5">Undo</h4>
        </div>
        <div class="modal-body" style="font-size: medium">
            <p>You deleted a song. Do you want to undo your last delete?</p>
        </div>
        <div class="modal-footer">
            <a href="" id="undodDelS"><button type="button" class="btn btn-info"><span class="glyphicon glyphicon-arrow-left"></span> Undo</button></a>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        </div>
      </div>
      
    </div>
  </div>
<!--include footer-->
@include('user.footer')
<!– Kết thúc Code của bạn–>
    <!– jQuery (necessary for Bootstrap’s JavaScript plugins) –>
    <script src= "{!! asset('user/js/jquery-2.2.4.min.js') !!}"></script>
    <!– Include all compiled plugins (below), or include individual files as needed –>
    <script src= "{!! asset('user/js/bootstrap.min.js') !!}"></script>
    <script src="{!! asset('user/js/myScript.js') !!}"></script> 
    <script type="text/javascript">
    var linkDel;
    var rHover;
    	$('.btnDelete').click(function(){
        $('#SongConfirm').text(document.getElementById("mySongUserTable").rows[rHover + 1].cells[1].textContent + " - " + document.getElementById("mySongUserTable").rows[rHover + 1].cells[2].textContent);
        $('#del').prop('href',document.getElementById("mySongUserTable").rows[rHover + 1].cells[4].textContent);
    	});

      $('tr').mouseover(function(){
            rHover = $(this).index();
      });

		<?php 
			if($deletedId!=""){
		?>
				$('#undodDelS').prop('href','{{URL::route('uundo',$deletedId)}}');
				$('#modalUndoS').modal('show');
		<?php 
			}
		?>

    </script>
    <script type="text/javascript">
        function showResult(key){
            key = key.toLowerCase();
            var listAdded = <?php if($isListEmpty==false) echo json_encode($listAdded);?>;
            var saveId=[];
            var length=listAdded.length;
            for(i=0;i<length;i++){
                $('#'+listAdded[i].id).hide();
            }
            for(var i=0; i<length;i++){
                if((listAdded[i].title.toLowerCase().indexOf(key)!=-1)||(listAdded[i].artist.toLowerCase().indexOf(key)!=-1)){
                    $('#'+listAdded[i].id).show();
                    saveId.push(listAdded[i].id);
                }     
            }
        }
</script>
</body>
</html>

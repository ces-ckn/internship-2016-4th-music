<?php
	$urlsource128 = 'http://api.mp3.zing.vn/api/mobile/source/song/'.$paras[3];
	$urlsource320 = 'http://api.mp3.zing.vn/api/mobile/source/song/'.$paras[4];
?>

<!DOCTYPE html>
<head>
<title>{{$paras[1]}} - {{$paras[2]}}</title> <!–Tiêu đề trang web–>
<meta name="viewport" content="width=device-width, initial-scale=1.0"> <!–Tự động nhận dạng thiết bị để co vào cho phù hợp–>
<meta charset="UTF-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="stylesheet" type="text/css" href="{!! asset('user/css/bootstrap.min.css') !!}" /> <!– Gọi đến thư viện Bootstrap để sử dụng–>
<link rel="stylesheet" type="text/css" href="{!! asset('user/css/style.css') !!}"/> <!– Đây là file CSS của bạn–>
<link rel="stylesheet" href="{!! asset('user/css/style1.css') !!}" type="text/css" /> <!– Đây là file CSS của bạn–>
</head>
<body>
<!– Code bạn đặt trong đây–>


<!--include header-->
@include('user.header')

<p id="songId" style="display:none">{{$paras[0]}}</p>
<div class="wrapPage" style="height: 1000px;">
	<div class="col-md-8">
	    <div class="form-inline">
	          <div class="form-group songName">
	            <h3>{{$paras[1]}} - </h3>
	          </div>
	          <div class="form-group">
	            <h3>{{$paras[2]}}</h3>
	          </div>
	    </div>
        <div>    	
			@if($isBlocked)
				<input style="color: #FFFFFF" id="blocked" type="button" disabled="true" class="btn btn-danger navbar-btn" value="Blocked">
			@elseif($isInList)
			<input style="color: #FFFFFF" id="addedBtn" type="button" disabled="true" class="btn btn-warning navbar-btn" value="Added">
			@elseif(Auth::guest())
			<button class="btn btn-warning navbar-btn" data-toggle="tooltip" title="Sign In to add" disabled>Add to Playlist</button>
			@else
			<input style="color: #FFFFFF" id="addBtn" type="button" class="btn btn-warning navbar-btn" value="Add to Playlist">
			@endif
			@if(!Auth::guest()&&Auth::user()->admin)
	                <?php
	                	$info = $paras[0]."**".$paras[1]."**".$paras[2]."**".$paras[3]."**".$paras[4];
	                	$urlmasteradd = URL::route('masteradd', $info);?>
	                <a href={{$urlmasteradd}}><button class="btn btn-warning navbar-btn"><span class="glyphicon glyphicon-king"></span> Add to Master list</button></a>
	        @endif
        </div>

	<div class="panel panel-warning" style="width: 600px;margin-top: 50px;">
  		<div class="panel-heading" style="height: 50px;padding-right:5px;">
  			<div class="form-inline">
  				<audio id="player__source" loop>
					<p>Your browser does not support HTML5 Audio</p>
					  <source id='sourceogg' src="{{$urlsource128}}" type="audio/ogg">
  					  <source id='sourcempeg' src="{{$urlsource128}}" type="audio/mpeg">
				</audio>
	          	<div class="form-group">
					<img src="{!! asset('user/images/play.png') !!}" id="playPause" class="icon-play">
	          	</div>
	          	<div class="form-group">
					<label id="currentTime" style="width: 20px;font-weight:normal;margin-top: 5px;">00:00</label>
	          	</div>
	          	<div class="form-group">
					<input id="seek" class="player--seek" type="range" min="0" value="0" style="width: 220px;margin-left: 20px;">	
	        	</div>
	        	<div class="form-group">
					<label id="durationTime" style="width: 20px;font-weight:normal;margin-top: 5px;">00:00</label>
	        	</div>
	        	<div class="form-group">
					<img id="mute" src="{!! asset('user/images/volume.png') !!}" class="icon-play" style="margin-left: 30px;">	
	        	</div>
	        	<div class="form-group">
	        		<input id="volume-seek" type="range" min="0" max="100" value="100" step="1" style="width: 70px;margin-right: 10px;">
	        	</div>
	        	<div class="form-group">
					<select id='quality' onchange='changeQuality()' name='quality'>
					 	<option selected='selected' value="{{$urlsource128}}">128kbps</option>
					  	<option value="{{$urlsource320}}">320kbps</option>
					</select>
	        	</div>
      		</div>
  		</div>
	</div>
	</div>
	<div class="col-md-4 sidebar" style="background-color: #F8F8F8">
	</div>
</div>

<!--include footer-->
@include('user.footer')
<!– Kết thúc Code của bạn–>
    <!– jQuery (necessary for Bootstrap’s JavaScript plugins) –>
    <script src= "{!! asset('user/js/jquery-2.2.4.min.js') !!}"></script>
    <!– Include all compiled plugins (below), or include individual files as needed –>
    <script src= "{!! asset('user/js/bootstrap.min.js') !!}"></script>
    <script src="{!! asset('user/js/myScript.js') !!}"></script>
    <script>
	    $(document).ready(function(){
	        //$('[data-toggle="tooltip"]').tooltip();   
	    });
    </script>
    <script type="text/javascript">
    	var player = document.getElementById('player__source'),
		playLoading = document.querySelectorAll('.player__loading span'),
		playPause = document.getElementById('playPause'),
		currentTime = document.getElementById('currentTime'),
		seek = document.getElementById('seek'),
		durationTime = document.getElementById('durationTime'),
		volumeSeek = document.getElementById('volume-seek'),
		mute = document.getElementById('mute'),
		timeInterval,
		i, 
		len = playLoading.length;

		window.onload = function() {
			//player.play();
			timeInterval = setInterval(timeUpdateMusic, 100);
			seek.addEventListener('input', seekMusic, false);
			//$('#playPause').attr('src',"{!! asset('user/images/pause.png') !!}");
			for (i = 0; i < len; i++) {
				playLoading[i].classList.add('active');
			}
			playPause.addEventListener('click', playPauseMusic, false);
			player.addEventListener('ended', endedMusic, false);
			volumeSeek.addEventListener('change',setvolume);
			mute.addEventListener('click',mutedMusic);
		};


		// Set Volume Music
		function setvolume(){
			player.volume = volumeSeek.value/100;
			if(volumeSeek.value == 0){
				$('#mute').attr('src',"{!! asset('user/images/mute.png') !!}");
			}
			else{
				$('#mute').attr('src',"{!! asset('user/images/volume.png') !!}");
			}
		}

		// Play and Pause Music
		function playPauseMusic() {
			var i, len = playLoading.length;
			if (player.paused) {
				player.play();
				timeInterval = setInterval(timeUpdateMusic, 100);
				seek.addEventListener('change', seekMusic, false);
				$('#playPause').attr('src',"{!! asset('user/images/pause.png') !!}");
				for (i = 0; i < len; i++) {
					playLoading[i].classList.add('active');
				}
			} else {
				player.pause();
				clearInterval(timeInterval);
				$('#playPause').attr('src',"{!! asset('user/images/play.png') !!}");
				for (i = 0; i < len; i++) {
					playLoading[i].classList.remove('active');
				}
			}
		}

		// Muted Music
		function mutedMusic() {
				if (player.muted) {
					player.muted = false;
					volumeSeek.value = 100;
					player.volume = 1;
					$('#mute').attr('src',"{!! asset('user/images/volume.png') !!}");
				} else {
					player.muted = true;
					volumeSeek.value = 0;
					$('#mute').attr('src',"{!! asset('user/images/mute.png') !!}");
				}
		}

		// Seek Music
		function seekMusic() {
			player.currentTime = seek.value;
		}

		// Time Update
		function timeUpdateMusic() {
			durationTime.innerHTML = secondToMinutes(player.duration);
			currentTime.innerHTML = secondToMinutes(player.currentTime);
			seek.max = player.duration;
			seek.value = player.currentTime;
		}

		// Convert Seconds to Minutes
		function secondToMinutes(seconds) {
			if(isNaN(seconds)){
				return "00:00";
			}
			else{
				var numMinutes = Math.floor((((seconds % 31536000) % 86400) % 3600) / 60),
						numSeconds = Math.round((((seconds % 3153600) % 86400) % 3600) % 60);
	
				if (numSeconds >= 10) {
					if(numSeconds == 60){
						var i = numMinutes + 1
						if(i >= 10){
							return i + ":" + "00";
						}
						else{
							return "0" + i + ':' + "00";
						}
					}
					else{
						if(numMinutes >= 10){
							return numMinutes + ':' + numSeconds;
						}
						else{
							return "0" + numMinutes + ':' + numSeconds;
						}
					}
				} 
				else {
						if(numMinutes >= 10){
							return numMinutes + ':0' + numSeconds;
						}
						else{
							return "0" + numMinutes + ':0' + numSeconds;
						}
				}
			}
		}

		// Ended Music
		function endedMusic() {
			player.pause();
			player.currentTime = 0;
			$('#playPause').attr('src',"{!! asset('user/images/play.png') !!}");
			for (i = 0; i < len; i++) {
				playLoading[i].classList.remove('active');
			}
		}
    </script>
    <script src="../nodejs/jquery.js"></script>
	<script src="../nodejs/socket.io.js"></script>    
	<script>
		function changeQuality(){

			var e = document.getElementById("quality");
			var quality = e.options[e.selectedIndex].value;
			console.log(quality);
			var audio = document.getElementById('player__source');

	        var sourceogg = document.getElementById('sourceogg');
	        var sourcempeg = document.getElementById('sourcempeg');
	        sourceogg.src=quality;
	        sourcempeg.src=quality;
	        audio.load();
	        audio.play();
	        seek.value=0;
	        volumeSeek.value=100;
	        player.volume = 1;
	        $('#playPause').attr('src',"{!! asset('user/images/pause.png') !!}");
	        $('#mute').attr('src',"{!! asset('user/images/volume.png') !!}");
		}

		function dropdownbtn () {
	    	document.getElementById("myDropdown").classList.toggle("show");
		}


		window.onclick = function(event) {
	  		if (!event.target.matches('.dropbtn')) {
	    	var dropdowns = document.getElementsByClassName("dropdown-content");
	    	var i;
	    		for (i = 0; i < dropdowns.length; i++) {
	      			var openDropdown = dropdowns[i];
	     			if (openDropdown.classList.contains('show')) {
	        			openDropdown.classList.remove('show');
	      			}
	    		}
	  		}
		}
		$(document).ready(function(){
			var socket=io.connect("https://codeengine-nodejs.herokuapp.com");
			socket.on('addresult',function(data){
				var songId = $('#songId').text();
				if(data['songId']==songId){
					$('#addBtn').attr('disabled',true);
            		$('#addBtn').attr('value','Added');
				}
			});
			$("#addBtn").click(function(){
				var songId = $('#songId').text();
				var title = "<?php echo $paras[1]; ?>";
				var artist = "<?php echo $paras[2]; ?>";
				var code128 = "<?php echo $paras[3]; ?>";
				var code320 = "<?php echo $paras[4]; ?>";
		    	$.ajax({
            		type:'POST',
           			url:'./addajax',
            		data:{_token:'<?php echo csrf_token() ?>',
            			songId:songId,
            			title:title,
            			artist:artist,
            			code128:code128,
            			code320:code320
            		},
            		success:function(data){
            		}
        		});
			});
		});
	</script>
</body>
</html>
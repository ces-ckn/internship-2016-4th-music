<!DOCTYPE html>
<head>
<title id="pageTitle">Home</title> <!–Tiêu đề trang web–>
<meta name="viewport" content="width=device-width, initial-scale=1.0"> <!–Tự động nhận dạng thiết bị để co vào cho phù hợp–>
<meta charset="UTF-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="stylesheet" type="text/css" href="{!! asset('user/css/bootstrap.min.css') !!}" /> <!– Gọi đến thư viện Bootstrap để sử dụng–>
<link rel="stylesheet" type="text/css" href="{!! asset('user/css/style.css') !!}"/> <!– Đây là file CSS của bạn–>
<link rel="stylesheet" type="text/css" href="{!! asset('user/css/style1.css') !!}"/> <!– Đây là file CSS của bạn–>
<link rel="stylesheet" type="text/css" href="{!! asset('user/css/style2.css') !!}"/>
<style>
div.card {
  width: 170px;
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
  text-align: center;
  float: right;
  position: fixed;
  right: 0; 
  top: 70px;
}

div.headerCard {
    background-color: #FEDAC2;
    color: white;
    padding: 10px;
    font-size: 40px;
}

div.containerCard {
    padding: 10px;
    font-size: large;
    background-color: #FFFFFF;
}
</style>
</head>
<body>
<!– Code bạn đặt trong đây–>

<!--include header-->
@include('user.header')
@if(!Auth::guest() && $isListEmpty==false)
<div class="card">
	<div class="headerCard">
	    <h1 id="voteTimes">{{$userVoteleft}}</h1>
	</div>
	<div class="containerCard">
	    <p>More Votes Left</p>
	</div>
</div>
@endif
<div class="wrapPage" style="height: 1000px">
@if($isListEmpty==false)
<div class="form-horizontal formPlay">
		<div class="form-group">
            <label id="songTitle" class="animated infinite flip" style="font-size: 25px;color: #FE703E;  -webkit-animation-duration: 10s;"></label>
    	</div>

    	<!-- <p>star me on <a href="https://github.com/wayou/audio-visualizer-with-controls">github</a> </p>
    	<canvas id='canvas' width="800" height="350"></canvas> -->

		<div class="form-group panel panel-warning" style="width: 750px;margin-bottom: 0px;">
		  		<div class="panel-heading playPlayList" style="height: 65px;padding-right:5px;">
		  			<audio id="player__source">
						<p>Your browser does not support HTML5 Audio</p>
						<?php
							echo $listSong[0]->title;
							echo $listSong[0]->artist;
							$urlsource0 = 'http://api.mp3.zing.vn/api/mobile/source/song/'.$listSong[0]->code128;
						?>
						<source src="{{$urlsource0}}" type="audio/ogg">
					</audio>
					

	      			<div class="form-inline">
			  				<img src="{!! asset('user/images/previous.png') !!}" id="previous">	
							<img src="{!! asset('user/images/play1.png') !!}" id="playPause">
							<img src="{!! asset('user/images/next.png') !!}" id="next">
							<label id="currentTime" style="width: 20px;font-weight:normal;margin-left: 5px">00:00</label>
						<div class="form-group">
							<input id="seek" class="player--seek" type="range" min="0" value="0" style="width: 250px;">
						</div>
							<label id="durationTime" style="width: 20px;font-weight:normal;">00:00</label>	
						
							<img id="mutedMusic" src="{!! asset('user/images/volume.png') !!}">
						<div class="form-group">
			        		<input id="volume-seek" type="range" min="0" max="100" value="100" step="1" style="width: 70px;" style="display:inline-block;">
			        	</div>
			        		<img src="{!! asset('user/images/repeatAll.png') !!}" id="repeatSong">
			        		<img src="{!! asset('user/images/random.png') !!}" id="randomSong" style="margin-left: 10px;">
			        </div>
	      		</div>
		</div>
		<div class="form-group panel panel-warning listSong" style="width: 750px;margin-top: 0px">

		<?php 
			if(count($listSong) > 10){
		?>
				<div class="panel-body playListPanel" id="scrollList">
		<?php
			}
			else{
		?>
				<div class="panel-body shortplayList">
		<?php
			}
		?>
		<ul class="playListForm" id="playList" style="list-style: none;">
				@foreach($listSong as $song)
				<!--<?php 
					$url = URL::route("listentomusic",$song->id_song."**".$song->title."**".$song->artist."**".$song->code128."**".$song->code320); 
	                $urlup = URL::route("voteup", $song->id_song);
	                $urldown = URL::route("votedown", $song->id_song);
	            ?>-->
	            <?php
					$urlsource = 'http://api.mp3.zing.vn/api/mobile/source/song/'.$song->code128;
					if($song->position == 1){
				?>
			  		<li id="{{$song->position}}" class="active">
                        <div class="form-inline plItem">
                            <div class="form-group plNum"><strong>{{$song->position}}</strong></div>
                            <a id="songURL" href="{{$urlsource}}"><div id="songInfo" class="form-group plTitle" value="{{$song->title}} - {{$song->artist}}">{{$song->title}} - {{$song->artist}}</div>
  							</a>
                            <div class="form-group">
                            	<div class="form-inline">
                            		<p style="display:none" id="songId">{{$song->id_song}}</p>
                            		<button id="upBtn" class="form-group btn btn-default btnUp" style="visibility: hidden;">
									     <span class="glyphicon glyphicon-arrow-up"></span>
									</button>
									@if(count($listSong)>1)
										@if(Auth::guest())
										<button id="dwnBtn" class="form-group btn btn-default btnDown" disabled data-toggle="tooltip" title="Sign In to vote">
									     <span class="glyphicon glyphicon-arrow-down"></span>
										</button>
										@elseif($song->isVoted)
										<button id="dwnBtn" class="form-group btn btn-default btnDown" disabled data-toggle="tooltip" title="You voted this song">
									     <span class="glyphicon glyphicon-arrow-down"></span>
										</button>
										@elseif($isVoted5Times)
										<button id="dwnBtn" class="form-group btn btn-default btnDown" disabled data-toggle="tooltip" title="You voted 5 times today">
									     <span class="glyphicon glyphicon-arrow-down"></span>
										</button>
										@else
										<button id="dwnBtn" class="form-group btn btn-default btnDown">
										     <span class="glyphicon glyphicon-arrow-down"></span>
										</button>
										@endif
									@else
										@if(Auth::guest())
										<button id="dwnBtn" class="form-group btn btn-default btnDown" disabled data-toggle="tooltip" title="Sign In to vote" style="visibility: hidden;">
									     <span class="glyphicon glyphicon-arrow-down"></span>
										</button>
										@elseif($song->isVoted)
										<button id="dwnBtn" class="form-group btn btn-default btnDown" disabled data-toggle="tooltip" title="You voted this song" style="visibility: hidden;">
									     <span class="glyphicon glyphicon-arrow-down"></span>
										</button>
										@elseif($isVoted5Times)
										<button id="dwnBtn" class="form-group btn btn-default btnDown" disabled data-toggle="tooltip" title="You voted 5 times today" style="visibility: hidden;">
									     <span class="glyphicon glyphicon-arrow-down"></span>
										</button>
										@else
										<button id="dwnBtn" class="form-group btn btn-default btnDown" style="visibility: hidden;">
										     <span class="glyphicon glyphicon-arrow-down"></span>
										</button>
										@endif	
									@endif
								</div>
                            </div>
                            <hr style="margin-top: 9px">
                        </div>
                    </li>
			  	<?php
			  		}
			  		else{
			  			if($song->position != count($listSong)){
			  	?>
			  		<li id="{{$song->position}}">
                        <div class="form-inline plItem">
                            <div class="form-group plNum"><strong>{{$song->position}}</strong></div>
                            <a id="songURL" href="{{$urlsource}}">
                            <div id="songInfo" class="form-group plTitle" value="{{$song->title}} - {{$song->artist}}">{{$song->title}} - {{$song->artist}}</div>
                            </a>
                            <div class="form-group">
                            	<div class="form-inline">
                            		<p style="display:none" id="songId">{{$song->id_song}}</p>
                            		@if(Auth::guest())
                            		<button id="upBtn" class="form-group btn btn-default btnUp" disabled data-toggle="tooltip" title="Sign In to vote">
									     <span class="glyphicon glyphicon-arrow-up"></span>
									</button>
									<button id="dwnBtn" class="form-group btn btn-default btnDown" disabled data-toggle="tooltip" title="Sign In to vote">
									     <span class="glyphicon glyphicon-arrow-down"></span>
									</button>
									@elseif($song->isVoted)
									<button id="upBtn" class="form-group btn btn-default btnUp" disabled data-toggle="tooltip" title="You voted this song">
									     <span class="glyphicon glyphicon-arrow-up"></span>
									</button>
									<button id="dwnBtn" class="form-group btn btn-default btnDown" disabled data-toggle="tooltip" title="You voted this song">
									    <span class="glyphicon glyphicon-arrow-down"></span>
									</button>
									@elseif($isVoted5Times)
									<button id="upBtn" class="form-group btn btn-default btnUp" disabled data-toggle="tooltip" title="You voted 5 times today">
									     <span class="glyphicon glyphicon-arrow-up"></span>
									</button>
									<button id="dwnBtn" class="form-group btn btn-default btnDown" disabled data-toggle="tooltip" title="You voted 5 time today">
									     <span class="glyphicon glyphicon-arrow-down"></span>
									</button>
									@else
                            		<button id="upBtn" class="form-group btn btn-default btnUp">
									     <span class="glyphicon glyphicon-arrow-up"></span>
									</button>
									<button id="dwnBtn" class="form-group btn btn-default btnDown">
									     <span class="glyphicon glyphicon-arrow-down"></span>
									</button>
									@endif
								</div>
                            </div>
                            <hr style="margin-top: 10px">
                        </div>
                    </li>
			  	<?php
			  			}
			  		else{
			    ?> 	
			    	<li id="{{$song->position}}">
                        <div class="form-inline plItem">
                            <div class="form-group plNum"><strong>{{$song->position}}</strong></div>
                            <a id="songURL" href="{{$urlsource}}"><div id="songInfo" class="form-group plTitle" value="{{$song->title}} - {{$song->artist}}">{{$song->title}} - {{$song->artist}}</div></a>
                            <div class="form-group">
                            	<div class="form-inline">
                            		<p style="display:none" id="songId">{{$song->id_song}}</p>
                            		@if(Auth::guest())
                            		<button id="upBtn" class="form-group btn btn-default btnUp" disabled data-toggle="tooltip" title="Sign In to vote">
									     <span class="glyphicon glyphicon-arrow-up"></span>
									</button>
									@elseif($song->isVoted)
									<button id="upBtn" class="form-group btn btn-default btnUp" disabled data-toggle="tooltip" title="You voted this song">
									     <span class="glyphicon glyphicon-arrow-up"></span>
									</button>
									@elseif($isVoted5Times)
									<button id="upBtn" class="form-group btn btn-default btnUp" disabled data-toggle="tooltip" title="You voted 5 times today">
									    <span class="glyphicon glyphicon-arrow-up"></span>
									</button>
									@else
									<button id="upBtn" class="form-group btn btn-default btnUp">
									     <span class="glyphicon glyphicon-arrow-up"></span>
									</button>
									@endif
									<button id="dwnBtn" class="form-group btn btn-default btnDown" style="visibility: hidden;">
									     <span class="glyphicon glyphicon-arrow-down"></span>
									</button>
								</div>
                            </div>
                            <hr style="margin-top: 10px">
                        </div>
                    </li>
			  	<?php	
			  			}
			  		}
			  	?>
            	@endforeach
		</ul>
		</div>
		</div>
</div>
@else
<h1>NO SONG IN LIST. SEARCH FOR A SONG AND ADD IT TO LIST :)</h1>
@endif
</div>
</div>


<!--include footer-->
@include('user.footer')
<!– Kết thúc Code của bạn–>
    <!– jQuery (necessary for Bootstrap’s JavaScript plugins) –>
    <script src= "{!! asset('user/js/jquery-2.2.4.min.js') !!}"></script>
    <!– Include all compiled plugins (below), or include individual files as needed –>
    <script src= "{!! asset('user/js/bootstrap.min.js') !!}"></script>
    <script src="{!! asset('user/js/myScript.js') !!}"></script>
    <script src="{!! asset('user/js/jquery.slimscroll.js') !!}"></script>
    <script type="text/javascript">
    var tracks;
    var playlist;
    var audio = $('#player__source');
    var current;
    var len;
    var link;
    
		$(document).ready(function () {
			var titleSong= $('songTitle');
			var volumeSeek = document.getElementById('volume-seek');
			var seek = document.getElementById('seek');
			var currentTime = document.getElementById('currentTime');
			var durationTime = document.getElementById('durationTime');
			var previousBtn = document.getElementById('previous');
			var nextBtn = document.getElementById('next');
			var muteBtn = document.getElementById('mutedMusic');
			var repeatBtn = document.getElementById('repeatSong');
			var timeInterval;
			var repeat;
			var random;

			init();
			function init() {
				    current = 0;
				    repeat = 0;
				    random = 0;
				    playlist = $('#playList');
				    tracks = playlist.find('li a');
				    songTitle.innerHTML = tracks.find('#songInfo').attr('value');
				    len = tracks.length - 1;
				    audio[0].volume = 1;

				    playlist.find('a').click(function (e) {
				        e.preventDefault();
				        link = $(this);
				        current = link.parent().parent().index();
				        console.log(current);
				        run(link, audio[0]);
				    	if(audioS.currentTime != 0){
				    		audioS.pause();
							audioS.currentTime = 0;
							audioS.src = "";
							$('#resultSearch').addClass('hidden');
							$("#searchBar").val("");

				    	}
				    });

				    timeInterval = setInterval(function(){
				    	durationTime.innerHTML = secondToMinutes(audio[0].duration);
						currentTime.innerHTML = secondToMinutes(audio[0].currentTime);
						seek.max = audio[0].duration;
						seek.value = audio[0].currentTime;
				    }, 100);

				    seek.addEventListener('input', function(e){
				    	audio[0].currentTime = seek.value;
				    });


				    seek.addEventListener('mousedown', function(e){
				    	audio[0].currentTime = seek.value;
				    });

				    audio[0].addEventListener('ended', function (e) {
				    	if(repeat == 1){
				    		link = playlist.find('a')[current];
				    		run($(link),audio[0]);
				    	}
					    else{
					    	if(random == 1){
					    		current = getRandomIntInclusive(0,len -1);
					    		console.log(current);
					    		link = playlist.find('a')[current];
					    		run($(link), audio[0]);
					    	}
						    else{
						        current++;
						        console.log("current: " + current);
						        console.log("len: " + len);
						        if (current == len + 1) {
						            current = 0;
						            link = playlist.find('a')[0];
						        } else {
						            link = playlist.find('a')[current];
						        }
					        run($(link), audio[0]);
					    	}
				    	}
				    });


				    volumeSeek.addEventListener('change',function(e){
				    	audio[0].volume = volumeSeek.value/100;
				    	if(volumeSeek.value == 0){
				    		$('#mutedMusic').attr('src',"{!! asset('user/images/mute.png') !!}");
				    	}
				    	else{
				    		$('#mutedMusic').attr('src',"{!! asset('user/images/volume.png') !!}");
				    	}
				    });

				    previousBtn.addEventListener('click',function(e){
				    	if(current == 0){
				    		current = len;
				    		link = playlist.find('a')[current];
				    	}
				    	else{
				    		current--;
				    		link = playlist.find('a')[current];
				    	}
				    	run($(link),audio[0]);
				    });
				    nextBtn.addEventListener('click',function(e){
				    	if(current == len){
				    		current = 0;
				    		link = playlist.find('a')[current];
				    	}
				    	else{
				    		current++;
				    		link = playlist.find('a')[current];
				    	}
				    	run($(link),audio[0]);
				    });

				    playPause.addEventListener('click', function(e){
				    	if (audio[0].paused) {
							audio[0].play();
							timeInterval = setInterval(function(){
					    	durationTime.innerHTML = secondToMinutes(audio[0].duration);
							currentTime.innerHTML = secondToMinutes(audio[0].currentTime);
							seek.max = audio[0].duration;
							seek.value = audio[0].currentTime;
					    	}, 100);
							seek.addEventListener('change', function(e){
				    			audio[0].currentTime = seek.value;
				    		});
							$('#playPause').attr('src',"{!! asset('user/images/pause1.png') !!}");
						} else {
							audio[0].pause();
							clearInterval(timeInterval);
							$('#playPause').attr('src',"{!! asset('user/images/play1.png') !!}");
						}
				    });

				    muteBtn.addEventListener('click',function(e){
				    	if (audio[0].muted) {
							audio[0].muted = false;
							volumeSeek.value = 100;
							audio[0].volume = 1;
							$('#mutedMusic').attr('src',"{!! asset('user/images/volume.png') !!}");
						} else {
							audio[0].muted = true;
							volumeSeek.value = 0;
							$('#mutedMusic').attr('src',"{!! asset('user/images/mute.png') !!}");
						}

				    });
				    repeatBtn.addEventListener('click',function(e){
				    	if(repeat == 0){
				    		repeat = 1;
				    		$('#repeatSong').attr('src',"{!! asset('user/images/repeat.png') !!}");
				    		random = 0;
				    		$('#randomSong').attr('src',"{!! asset('user/images/random.png') !!}");
				    	}
				    	else{
				    		repeat = 0;
				    		$('#repeatSong').attr('src',"{!! asset('user/images/repeatAll.png') !!}");
				    	}
				    });

				    audio[0].onpause = function() {
						 $('#playPause').attr('src',"{!! asset('user/images/play1.png') !!}");
					};

					audio[0].onplay = function() {
					    $('#playPause').attr('src',"{!! asset('user/images/pause1.png') !!}");
					};
					$('#randomSong').on('click',function(){
						if(random == 0){
							random = 1;
							$('#randomSong').attr('src',"{!! asset('user/images/unrandom.png') !!}");
							repeat = 0;
							$('#repeatSong').attr('src',"{!! asset('user/images/repeatAll.png') !!}");
							console.log(random);
							console.log(getRandomIntInclusive(0,10));
						}
						else{
							random = 0;
							console.log('unrandom');
							$('#randomSong').attr('src',"{!! asset('user/images/random.png') !!}");
						}
					});
				}
			function run(link, player) {
				    player.src = link.attr('href');
				    songTitle.innerHTML = link.find('#songInfo').attr('value');
				    $('#pageTitle').text(link.find('#songInfo').attr('value'));
				    console.log(link.find('#songInfo').attr('value'));
				    par = link.parent().parent();
				    par.addClass('active').siblings().removeClass('active');
				    audio[0].load();
				    audio[0].play();
			}			

		// Convert Seconds to Minutes
		function secondToMinutes(seconds) {
			if(isNaN(seconds)){
				return "00:00";
			}
			else{
				var numMinutes = Math.floor((((seconds % 31536000) % 86400) % 3600) / 60),
						numSeconds = Math.round((((seconds % 3153600) % 86400) % 3600) % 60);
	
				if (numSeconds >= 10) {
					if(numSeconds == 60){
						var i = numMinutes + 1
						if(i >= 10){
							return i + ":" + "00";
						}
						else{
							return "0" + i + ':' + "00";
						}
					}
					else{
						if(numMinutes >= 10){
							return numMinutes + ':' + numSeconds;
						}
						else{
							return "0" + numMinutes + ':' + numSeconds;
						}
					}
				} 
				else {
						if(numMinutes >= 10){
							return numMinutes + ':0' + numSeconds;
						}
						else{
							return "0" + numMinutes + ':0' + numSeconds;
						}
				}
			}
		}


	})
	function getRandomIntInclusive(min, max) {
	  return Math.floor(Math.random() * (max - min + 1)) + min;
	}

    </script>
    <script type="text/javascript">
	$('#scrollList').slimScroll({
		height: 'auto',
		color: '#FAD6AC',
		size: '10px',
		railVisible: true,
	    railColor: '#F9C393',
	    alwaysVisible: true,
	});
	</script>
<script src="{!! asset('user/js/main.js') !!}"></script>
<script src="{!! asset('nodejs/jquery.js') !!}"></script>
<script src="{!! asset('nodejs/socket.io.js') !!}"></script>

<script>
$(document).ready(function(){

    var socket=io.connect('https://codeengine-nodejs.herokuapp.com');
    //var socket=io.connect('localhost:5000');

    var lastPos = <?php if($isListEmpty){
    	echo 0;
    	}else{
    		echo count($listSong);
    	} ?>;

    socket.on('voteresult', function(data){
    	var pos1TitleArtist = $('#'+data['pos1']).find('#songInfo').html();
    	var pos1SongURL = $('#'+data['pos1']).find('#songURL').attr('href');
    	var pos1SongId = $('#'+data['pos1']).find('#songId').text();

    	var pos2TitleArtist = $('#'+data['pos2']).find('#songInfo').html();
    	var pos2SongURL = $('#'+data['pos2']).find('#songURL').attr('href');
    	var pos2SongId = $('#'+data['pos2']).find('#songId').text();

        $('#'+data['pos1']).find('#songInfo').html(pos2TitleArtist);
        $('#'+data['pos1']).find('#songInfo').attr('value',pos2TitleArtist);
        $('#'+data['pos1']).find('#songURL').attr('href',pos2SongURL);
        $('#'+data['pos1']).find('#songId').text(pos2SongId);

        $('#'+data['pos2']).find('#songInfo').html(pos1TitleArtist);
        $('#'+data['pos2']).find('#songInfo').attr('value',pos1TitleArtist);
        $('#'+data['pos2']).find('#songURL').attr('href',pos1SongURL);
        $('#'+data['pos2']).find('#songId').text(pos1SongId);

        
        var pos1Class = $('#'+data['pos1']).attr('class');
        var pos2Class = $('#'+data['pos2']).attr('class');
        if(pos2Class=="active"){
        	$('#'+data['pos2']).removeClass('active');
        	$('#'+data['pos1']).addClass('active');

        	current = data['pos1']-1;

        }
        if(pos1Class=="active"){
        	$('#'+data['pos1']).removeClass('active');
        	$('#'+data['pos2']).addClass('active');

        	current = data['pos2']-1;
        }


    });
	socket.on('listaddresult',function(data){

		if(lastPos<1)location.reload(true);

		var song = data['songlisten'];
		$('#'+lastPos).find('#dwnBtn').css('visibility','visible');
		lastPos+=1;
		if(lastPos == 11){
			console.log($('.panel-body.shortplayList'));
			$('.panel-body.shortplayList').css('overflow-y','scroll');//panel-body's style
		}
            var newLI = $('<li id="'+lastPos+'">'+
                        '<div class="form-inline plItem">' +
                            '<div class="form-group plNum"><strong>'+lastPos+'</strong></div>' +
                            '<a id="songURL" href="'+'http://api.mp3.zing.vn/api/mobile/source/song/'+song['code128']+'"><div id="songInfo" class="form-group plTitle" value="'+song['title']+' - '+song['artist']+'">'+song['title']+' - '+song['artist']+'</div></a>' + 
                            '<div class="form-group">' + 
                            	'<div class="form-inline" style="margin-left: 7px">' +
                            		'<p style="display:none" id="songId">'+song['id_song']+'</p>' +
									'<button id="upBtn" class="form-group btn btn-default btnUp">'+
									     '<span class="glyphicon glyphicon-arrow-up"></span>'+
									'</button>' +
									'<button id="dwnBtn" class="form-group btn btn-default btnDown" style="visibility: hidden;">' +
									     '<span class="glyphicon glyphicon-arrow-down"></span>' +
									'</button>' +
								'</div>'+
                            '</div>' +
                            '<hr style="margin-top: 10px">' + 
                        '</div>' + 
                    '</li>');
		$('#playList').append(newLI);
		//add action event for new item
		$('#playList').children('#'+lastPos).find('#upBtn').click(function(){
			var crtId = $(this).closest('li').attr('id');
    		crtId = parseInt(crtId, 10);
    		var nxtId = crtId - 1;
        	var songId = $(this).siblings('#songId').text();
         	$.ajax({
        		type:'POST',
        		url:'./upajax',
        		data:{_token:'<?php echo csrf_token() ?>',songId:songId},
        		success:function(data){
        		$('#voteTimes').html(5-data.vTimes['votetimes']);
        		if(data.vTimes['votetimes']==5){
        		$('#playList').children('li').find('#upBtn').prop('disabled',true);
        		$('#playList').children('li').find('#dwnBtn').prop('disabled',true);
        		$('#playList').children('li').find('#dwnBtn').attr('data-toggle',"tooltip");
        		$('#playList').children('li').find('#dwnBtn').attr('title',"You voted 5 times");
        		$('#playList').children('li').find('#upBtn').attr('data-toggle',"tooltip");
        		$('#playList').children('li').find('#upBtn').attr('title',"You voted 5 times");
        		return;
        		}
        		if($('#playList').children('#'+nxtId).find('#upBtn').prop('disabled')){
        		$('#playList').children('#'+crtId).find('#upBtn').prop('disabled',true);
        		$('#playList').children('#'+crtId).find('#dwnBtn').prop('disabled',true);
        		$('#playList').children('#'+crtId).find('#dwnBtn').attr('data-toggle',"tooltip");
        		$('#playList').children('#'+crtId).find('#dwnBtn').attr('title',"You voted this song");
        		$('#playList').children('#'+crtId).find('#upBtn').attr('data-toggle',"tooltip");
        		$('#playList').children('#'+crtId).find('#upBtn').attr('title',"You voted this song");
        		}
        		else{
        		$('#playList').children('#'+nxtId).find('#upBtn').prop('disabled',true);
        		$('#playList').children('#'+nxtId).find('#dwnBtn').prop('disabled',true);
        		$('#playList').children('#'+nxtId).find('#dwnBtn').attr('data-toggle',"tooltip");
        		$('#playList').children('#'+nxtId).find('#dwnBtn').attr('title',"You voted this song");
        		$('#playList').children('#'+nxtId).find('#upBtn').attr('data-toggle',"tooltip");
        		$('#playList').children('#'+nxtId).find('#upBtn').attr('title',"You voted this song");
        		}
        		return;
            	}
        	});
    	});

    	$('#playList').children('#'+lastPos).find('#dwnBtn').click(function(){
    		var crtId = $(this).closest('li').attr('id');
    		crtId = parseInt(crtId, 10);
    		var nxtId = crtId + 1;
        	var songId = $(this).siblings('#songId').text();
    		$.ajax({
        		type:'POST',
        		url:'./dwnajax',
        		data:{_token:'<?php echo csrf_token() ?>',songId:songId},
        		success:function(data){
        		$('#voteTimes').html(5-data.vTimes['votetimes']);
        		if(data.vTimes['votetimes']==5){
        		$('#playList').children('li').find('#upBtn').prop('disabled',true);
        		$('#playList').children('li').find('#dwnBtn').prop('disabled',true);
        		$('#playList').children('li').find('#dwnBtn').attr('data-toggle',"tooltip");
        		$('#playList').children('li').find('#dwnBtn').attr('title',"You voted 5 times");
        		$('#playList').children('li').find('#upBtn').attr('data-toggle',"tooltip");
        		$('#playList').children('li').find('#upBtn').attr('title',"You voted 5 times");
        		return;
        		}
        		if($('#playList').children('#'+nxtId).find('#upBtn').prop('disabled')){
        		$('#playList').children('#'+crtId).find('#upBtn').prop('disabled',true);
        		$('#playList').children('#'+crtId).find('#dwnBtn').prop('disabled',true);
        		$('#playList').children('#'+crtId).find('#dwnBtn').attr('data-toggle',"tooltip");
        		$('#playList').children('#'+crtId).find('#dwnBtn').attr('title',"You voted this song");
        		$('#playList').children('#'+crtId).find('#upBtn').attr('data-toggle',"tooltip");
        		$('#playList').children('#'+crtId).find('#upBtn').attr('title',"You voted this song");
        		}
        		else{
        		$('#playList').children('#'+nxtId).find('#upBtn').prop('disabled',true);
        		$('#playList').children('#'+nxtId).find('#dwnBtn').prop('disabled',true);
        		$('#playList').children('#'+nxtId).find('#dwnBtn').attr('data-toggle',"tooltip");
        		$('#playList').children('#'+nxtId).find('#dwnBtn').attr('title',"You voted this song");
        		$('#playList').children('#'+nxtId).find('#upBtn').attr('data-toggle',"tooltip");
        		$('#playList').children('#'+nxtId).find('#upBtn').attr('title',"You voted this song");
        		}
        		return;
            	}
        	});
    	});
    	len++;
		tracks = playlist.find('li a');
		playlist.children('#'+lastPos).find('a').click(function (e) {
			e.preventDefault();
			link = $(this);
			current = link.parent().parent().index();
			console.log(current);
			run(link, audio[0]);
		});
		function run(link, player) {
			player.src = link.attr('href');
			songTitle.innerHTML = link.find('#songInfo').attr('value');
			console.log(link.find('#songInfo').attr('value'));
			par = link.parent().parent();
			par.addClass('active').siblings().removeClass('active');
			audio[0].load();
			audio[0].play();
		}			
		//finish add new event for new item
	});

    $('#playList').children('li').find('#upBtn').click(function(){
    	var crtId = $(this).closest('li').attr('id');
    	crtId = parseInt(crtId, 10);
    	var nxtId = crtId - 1;
        var songId = $(this).siblings('#songId').text();
         $.ajax({
        type:'POST',
        url:'./upajax',
        data:{_token:'<?php echo csrf_token() ?>',songId:songId},
        success:function(data){
        	$('#voteTimes').html(5-data.vTimes['votetimes']);
        	if(data.vTimes['votetimes']==5){
        		$('#playList').children('li').find('#upBtn').prop('disabled',true);
        		$('#playList').children('li').find('#dwnBtn').prop('disabled',true);
        		$('#playList').children('li').find('#dwnBtn').attr('data-toggle',"tooltip");
        		$('#playList').children('li').find('#dwnBtn').attr('title',"You voted 5 times");
        		$('#playList').children('li').find('#upBtn').attr('data-toggle',"tooltip");
        		$('#playList').children('li').find('#upBtn').attr('title',"You voted 5 times");
        		return;
        	}
        	if($('#playList').children('#'+nxtId).find('#upBtn').prop('disabled')){
        		$('#playList').children('#'+crtId).find('#upBtn').prop('disabled',true);
        		$('#playList').children('#'+crtId).find('#dwnBtn').prop('disabled',true);
        		$('#playList').children('#'+crtId).find('#dwnBtn').attr('data-toggle',"tooltip");
        		$('#playList').children('#'+crtId).find('#dwnBtn').attr('title',"You voted this song");
        		$('#playList').children('#'+crtId).find('#upBtn').attr('data-toggle',"tooltip");
        		$('#playList').children('#'+crtId).find('#upBtn').attr('title',"You voted this song");
        	}
        	else{
        		$('#playList').children('#'+nxtId).find('#upBtn').prop('disabled',true);
        		$('#playList').children('#'+nxtId).find('#dwnBtn').prop('disabled',true);
        		$('#playList').children('#'+nxtId).find('#dwnBtn').attr('data-toggle',"tooltip");
        		$('#playList').children('#'+nxtId).find('#dwnBtn').attr('title',"You voted this song");
        		$('#playList').children('#'+nxtId).find('#upBtn').attr('data-toggle',"tooltip");
        		$('#playList').children('#'+nxtId).find('#upBtn').attr('title',"You voted this song");
        	}
        	return;
            }
        });
    });

    $('#playList').children('li').find('#dwnBtn').click(function(){
    	var crtId = $(this).closest('li').attr('id');
    	crtId = parseInt(crtId, 10);
    	var nxtId = crtId + 1;
        var songId = $(this).siblings('#songId').text();
    	$.ajax({
        type:'POST',
        url:'./dwnajax',
        data:{_token:'<?php echo csrf_token() ?>',songId:songId},
        success:function(data){
        	$('#voteTimes').html(5-data.vTimes['votetimes']);
        	if(data.vTimes['votetimes']==5){
        		$('#playList').children('li').find('#upBtn').prop('disabled',true);
        		$('#playList').children('li').find('#dwnBtn').prop('disabled',true);
        		$('#playList').children('li').find('#dwnBtn').attr('data-toggle',"tooltip");
        		$('#playList').children('li').find('#dwnBtn').attr('title',"You voted 5 times");
        		$('#playList').children('li').find('#upBtn').attr('data-toggle',"tooltip");
        		$('#playList').children('li').find('#upBtn').attr('title',"You voted 5 times");
        		return;
        	}
        	if($('#playList').children('#'+nxtId).find('#upBtn').prop('disabled')){
        		$('#playList').children('#'+crtId).find('#upBtn').prop('disabled',true);
        		$('#playList').children('#'+crtId).find('#dwnBtn').prop('disabled',true);
        		$('#playList').children('#'+crtId).find('#dwnBtn').attr('data-toggle',"tooltip");
        		$('#playList').children('#'+crtId).find('#dwnBtn').attr('title',"You voted this song");
        		$('#playList').children('#'+crtId).find('#upBtn').attr('data-toggle',"tooltip");
        		$('#playList').children('#'+crtId).find('#upBtn').attr('title',"You voted this song");
        	}
        	else{
        		$('#playList').children('#'+nxtId).find('#upBtn').prop('disabled',true);
        		$('#playList').children('#'+nxtId).find('#dwnBtn').prop('disabled',true);
        		$('#playList').children('#'+nxtId).find('#dwnBtn').attr('data-toggle',"tooltip");
        		$('#playList').children('#'+nxtId).find('#dwnBtn').attr('title',"You voted this song");
        		$('#playList').children('#'+nxtId).find('#upBtn').attr('data-toggle',"tooltip");
        		$('#playList').children('#'+nxtId).find('#upBtn').attr('title',"You voted this song");
        	}
        	return;
            }
        });
    });
});
</script>
</body>
</html>
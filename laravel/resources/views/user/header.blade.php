<!DOCTYPE html>
<head>
<style type="text/css">
body {  
	position: relative;
}
.nav .active {  
	background-color: #9F9F9F;
}
 
.nav{
	font-weight: bold;
	font-size: medium;
	width: 175px;
	height: 300px;
	background-color: #B6B6B6;
}
.dropdown-menu.forAnimate{ 
position: static; 
float: none; 
width: auto; 
margin-top: 0; 
background-color: transparent; 
border: 0; 
-webkit-box-shadow: none; 
box-shadow: none;
}

.dropdown-menu.forAnimate a{
	font-size: medium;
}
.nav.nav-list.affix a{
	color: #FFFFFF;
}
.nav.nav-list.affix a:hover{
	background-color: #DEDEDE;
}
/*
 *  STYLE 3
 */

#resultSearch::-webkit-scrollbar-track
{
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
    background-color: #F5F5F5;
}

#resultSearch::-webkit-scrollbar
{
    width: 6px;
    background-color: #F5F5F5;
}

#resultSearch::-webkit-scrollbar-thumb
{
    background-color: #AFAFAF;
}
</style>
</head>
<body>
<!– Code bạn đặt trong đây–>
<header>
	<div class="container group">
		<a href="{{ url('') }}"><img src="{!! asset('user/images/logo.png') !!}" id="logo"></a>
		<form role="form" id="searchForm" method="get" action="{{ url('/result') }}" class="input-group">
			      <input id="searchBar" type="search" class="form-control" placeholder="Search" name="searchname" autocomplete="off" required>
			      <span class="input-group-btn">
			        <button class="btn btn-default" type="submit"><span class="glyphicon glyphicon-search" aria-hidden="true"></span>
			        </button>
			      </span>
		</form><!-- /input-group -->
		@if (Auth::guest())
			<div class="navbar-right">
							    <a href="{{ url('/register') }}" class="btn btn-default navbar-btn">Sign Up</a>
							    <a href="{{ url('/login') }}" class="btn btn-warning navbar-btn">Sign In</a>
			</div>
		@else
			<div class="navbar-right" id="navUser">
							<div class="dropdown">
							 	<h4 id="userName" class="dropdown-toggle" id="dropdownUser" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
							 	<span class="glyphicon glyphicon-user" aria-hidden="true"></span>
								    {{ Auth::user()->name }}
								<span class="caret"></span>
							 	</h4>	
							  <ul class="dropdown-menu" aria-labelledby="dropdownUser">
							    <li><a href="{{route('user.edit',Auth::user()->id)}}"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>     Update Profile</a></li>
							    <li><a href="{{ url('/listadded') }}"><span class="glyphicon glyphicon-music" aria-hidden="true"></span>     Added Songs</a></li>
							    <li><a href="{{ url('/logout') }}"><span class="glyphicon glyphicon-log-out" aria-hidden="true"></span>     Log Out</a></li>
							  </ul>
							</div>						    
			</div>
		@endif
	</div>
		<div id="resultSearch" style="margin-left:355px;width: 355px;height: 200px;z-index:1;position: absolute;padding-top: 10px;padding-left: 5px;color: #FF9517;overflow-y:scroll;font-size: medium" class="container group panel panel-default hidden">
				<audio id="audioSo"></audio>
			<ul id="listSong" style="list-style-type:none;color: #FF763A">
			</ul>
		</div>	
		@if(!Auth::guest()&&Auth::user()->admin)
		<div class="col-sm-3 sidebar" style="padding-left: 0px;"> 
		   <ul class="nav nav-list affix">
		    <li class="active">
		    	<a href="{{ url('') }}">Home<span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-home"></span></a>
		    </li> 
			<li class="dropdown"> <a class="dropdown-toggle" data-toggle="dropdown">Songs   <span class="caret"></span><span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-music"></span></a> 
			     <ul class="dropdown-menu forAnimate" role="menu"> 
			      <li><a href="{{ url('/admin') }}">All</a>
			      </li> 
			      <li><a href="{{ url('/adminblockedlist') }}">Blocked</a>
			      </li>  
			     </ul> 
			</li>  
			<li>
				<a href="{{ url('/userlist') }}">Users<span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-user"></span></a>
			</li> 
			<li>
				<a href="{{ url('/master') }}">Master<span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-king"></span></a>
			</li> 
			<li>
				<a href="{{ url('/adminresetdb') }}">Reset Database<span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-refresh"></span></a>
			</li>
		   </ul> 
		  </div> 
		@endif
</header>



<!– Kết thúc Code của bạn–>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script src="{!! asset('nodejs/jquery.js') !!}"></script>
<script src="{!! asset('nodejs/socket.io.js') !!}"></script>
<script type="text/javascript">
var audioS = document.getElementById("audioSo");
$(document).ready(function(){

	var socket=io.connect('https://codeengine-nodejs.herokuapp.com');
    socket.on('addresult',function(data){
    	var idBtn = data['songId'].toString();
    	var addBtn = $('#listSong').children('li').find('#'+idBtn);
    	addBtn.hide();
    });
    socket.on('masteraddresult',function(data){
    	var idBtn = data['songId'].toString();
    	var mstBtn = $('#listSong').children('li').find('#'+'master'+idBtn);
    	mstBtn.hide();
    });

    var timer;

	$('#searchForm').children('#searchBar').keyup(function(){
		var playingS = null;
		var clickedS = 0;
	 	var dInput = this.value;
	 	var list=$('#resultSearch').children('#listSong');
	 	var fragment = document.createDocumentFragment();
	 	var config = {
        	routes: [
            	{ searchajax: "{{ URL::to('searchajax') }}" }
        	]
    	};
    	clearTimeout(timer);
	 	if(dInput!=""){
		 	timer=setTimeout(function validate(){
		 		$.ajax({
		 			type:'GET',
	        		url:config.routes[0].searchajax,
	        		data:"keyword="+dInput,
	        		success:function(data){
	        			list.empty();
	        			var songs = data.songs
	        			for(var num in songs){      				
	        				var songId = songs[num]['song_id'];
	        				var title = songs[num]['title'];
	        				var artist = songs[num]['artist'];
	        				var code128 = songs[num]['code128'];
	        				var code320 = songs[num]['code320'];
	        				var isInList = songs[num]['isInList'];
	        				var isInMList = songs[num]['isInMList'];
	        				<?php $url = URL::route("listentomusic",'thisisthetempsongid');?>
	        				var listenurl = "<?php echo $url ?>";
	        				var info = songId+"**"+title+"**"+artist+"**"+code128+"**"+code320;
	        				listenurl = listenurl.replace('thisisthetempsongid',info);

	        				var newAnchor = document.createElement('a');
	        				newAnchor.setAttribute('href', listenurl);
	        				newAnchor.textContent = songs[num]['title'] + " - " + songs[num]['artist'];
	        				newAnchor.style.color = "#FE704A";

	        				var ele = document.createElement('li');
	        				var hrtag = document.createElement('hr');
	        				var divFormInline = document.createElement("div");
	        				divFormInline.setAttribute("class","form-inline");
	        				var divFormGroup = document.createElement("div");
	        				divFormGroup.setAttribute("class","form-group");
	        				divFormGroup.style = 'margin-bottom: 5px;';

	        				var imgPlay= document.createElement("IMG");
	        				imgPlay.setAttribute("class","playImmediately");
						    imgPlay.setAttribute("src", "{!! asset('user/images/play16.png') !!}");
						    imgPlay.style= 'border: 1px solid #ddd;border-radius: 4px;padding: 2px;';
						    var srcSong = 'http://api.mp3.zing.vn/api/mobile/source/song/'+songs[num]['code128'];
						    imgPlay.setAttribute("id",srcSong);
						    
						    var imgAdd= document.createElement("IMG");
						    imgAdd.setAttribute("src", "{!! asset('user/images/add16.png') !!}");
						    imgAdd.setAttribute("class","addImmediately");
						    imgAdd.setAttribute("id",songId);
						    imgAdd.setAttribute("name",info);
						    imgAdd.style = 'border: 1px solid #ddd;border-radius: 4px;padding: 2px;margin-left: 5px';

						    if(isInList){
						    	imgAdd.setAttribute('style','visibility:hidden');
						    }

						    var imgMasterAdd= document.createElement("IMG");
						    imgMasterAdd.setAttribute("src", "{!! asset('user/images/king.png') !!}");
						    imgMasterAdd.setAttribute("class","addMasterList");
						    imgMasterAdd.setAttribute("name",info);
						    imgMasterAdd.setAttribute("id","master"+songId);
						    imgMasterAdd.style = 'border: 1px solid #ddd;border-radius: 4px;padding: 2px;margin-left: 5px';

						    if(isInMList){
						    	imgMasterAdd.setAttribute('style','visibility:hidden');
						    }

						    ele.appendChild(divFormInline);
						    ele.appendChild(divFormGroup);
	        				ele.appendChild(newAnchor);
	        				ele.appendChild(divFormGroup);
	        				ele.appendChild(imgPlay);
	        				<?php if(!Auth::guest()){ ?>
	        					ele.appendChild(imgAdd);
	        				<?php } ?>
	        				<?php if(!Auth::guest()&&Auth::user()->admin){ ?>
	        					ele.appendChild(imgMasterAdd);
	        				<?php } ?>
	        				ele.appendChild(hrtag);
	        				fragment.appendChild(ele);
	        			}
	        			list.append(fragment);
	        			$('#resultSearch').removeClass('hidden');

	        			$('.playImmediately').click(function(){
							clickedS = $(this).parent().index();
							if(clickedS != playingS){
								audioS.setAttribute("src",this.getAttribute("id"));
								audioS.play();
								this.setAttribute("src","{!! asset('user/images/stop16.png') !!}");
								if(playingS != null){
									console.log(list.find('li').eq(playingS));
									console.log(list.find('li').eq(playingS).find(".playImmediately"));
									list.find('li').eq(playingS).find(".playImmediately").attr("src","{!! asset('user/images/play16.png') !!}");
								}
								playingS = $(this).parent().index();
								audio[0].pause();
							}
							else{
								audioS.pause();
								audioS.currentTime = 0;
								audioS.src = "";
								this.setAttribute("src","{!! asset('user/images/play16.png') !!}");
								playingS = null;
								if(audio[0].currentTime != 0){
									audio[0].play();
								}
							}
						});

						$('.addImmediately').click(function(){
							var paras = this.getAttribute("name").split("**");
							var url = "{{ URL::to('/listen/addajax') }}"
							var btn = $(this);
							$.ajax({
	            				type:'POST',
	            				url:url,
	            				data:{_token:'<?php echo csrf_token() ?>',
	            				songId:paras[0],
	            				title:paras[1],
	            				artist:paras[2],
	            				code128:paras[3],
	            				code320:paras[4]
	          				},
	            			success:function(data){
	            				
	            			}
	          				});
						});
						$('.addMasterList').click(function(){
							var paras = this.getAttribute("name");
							var url = "{{ URL::to('/addmasterajax') }}" ;
							$.ajax({
								type:'GET',
								url:url,
								data:{paras},
							success:function(data){

							}
							});
						});
		 			}
		 		});//ajax
			},500);
		}
			else{
			list.empty();
			audioS.pause();
			audioS.currentTime = 0;
			audioS.src = "";
			$('#resultSearch').addClass('hidden');
			if(audio[0].currentTime != 0){
				audio[0].play();
			}
			
		}
		
	});//searchBar

});//document
</script>
</body>
</html>
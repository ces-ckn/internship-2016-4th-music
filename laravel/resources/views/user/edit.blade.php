<!DOCTYPE html>
<head>
<title>Edit Profile</title> <!–Tiêu đề trang web–>
<meta name="viewport" content="width=device-width, initial-scale=1.0"> <!–Tự động nhận dạng thiết bị để co vào cho phù hợp–>
<meta charset="UTF-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="stylesheet" type="text/css" href="{!! asset('user/css/bootstrap.min.css') !!}" /> <!– Gọi đến thư viện Bootstrap để sử dụng–>
<link rel="stylesheet" type="text/css" href="{!! asset('user/css/style.css') !!}"/> <!– Đây là file CSS của bạn–>
<link rel="stylesheet" type="text/css" href="{!! asset('user/css/style1.css') !!}"/> <!– Đây là file CSS của bạn–>
</head>
<body>
<!– Code bạn đặt trong đây–>

<!--include header-->
@include('user.header')

 @if(Session::has('message'))
  <p>
   {{Session::get('message')}}
  </p>
 @endif
 <?php
 $url = route("user.update",$user->id);
 ?>
<div class="wrapPage" style="height: 1000px">
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2" style="margin-top: 20px;margin-left: 120px;">
            <div class="panel panel-warning">
                <div class="panel-heading" style="text-align: center;"><h4>Edit Profile</h4></div>
                <div class="panel-body">
                 <form id="editForm" onsubmit="return false" class="form-horizontal" role="form" accept-charset="UTF-8">
					 <!--<input name="_method" type="hidden" value="PUT">
					 <input type="hidden" name="_token" value="{{ csrf_token() }}">-->
                     <div class="form-group">
                        <div class="form-inline" style="margin-left: 85px;">
                            <div class="form-group">
                                <hr style="width: 250px;">
                            </div>
                            <div class="form-group seperate">
                                Edit Your Name
                            </div>
                            <div class="form-group">
                                <hr style="width: 250px;">
                            </div>  
                        </div>
                    </div>
					 <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Name</label>
                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value='{{$user->name}}'>
                            </div>
                    </div>
                    <div class="form-group">
                            <div class="alert alert-success hide" id="alertName" style="text-align: center"></div>
                    </div>
                    <div class="form-group">
                        <div class="form-inline" style="margin-left: 65px;">
                            <div class="form-group">
                                <hr style="width: 250px;">
                            </div>
                            <div class="form-group seperate">
                                Or Edit Your Password
                            </div>
                            <div class="form-group">
                                <hr style="width: 250px;">
                            </div>  
                        </div>
                    </div>
                    <div class="form-group">
                            <label class="col-md-4 control-label">Current Password:</label>
                            <div class="col-md-6">
                                <input id="current_password" type="password" class="form-control" name="current_password" placeholder="Your Current Password">
                            </div>
                    </div>
                    <div class="form-group">
                            <label class="col-md-4 control-label">New Password:</label>
                            <div class="col-md-6">
                                <input id="new_password" type="password" class="form-control" name="new_password" placeholder="New Password">
                            </div>
                    </div>
                    <div class="form-group">
                            <label class="col-md-4 control-label">New Password Confirmation:</label>
                            <div class="col-md-6">
                                <input id="new_password_confirmation" type="password" class="form-control" name="new_password_confirmation" placeholder="New Password Again">
                            </div>
                    </div>
                    <div class="form-group">
                            <div class="alert alert-success hide" id="alertPass" style="text-align: center"></div>
                            <div class="alert alert-danger hide" id="alertWrongPass" style="text-align: center"></div>
                            <div class="alert alert-danger hide" id="alertWrongConfirm" style="text-align: center"></div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-warning" id="btUpdate">
                                Update
                            </button>
                        </div>
                    </div>      
                    <!--<p>{{$message}}</p> -->             
					</form>   
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!--include footer-->
@include('user.footer')
<!– Kết thúc Code của bạn–>
    <!– jQuery (necessary for Bootstrap’s JavaScript plugins) –>
    <script src= "{!! asset('user/js/jquery-2.2.4.min.js') !!}"></script>
    <!– Include all compiled plugins (below), or include individual files as needed –>
    <script src= "{!! asset('user/js/bootstrap.min.js') !!}"></script>
    <script src="{!! asset('user/js/myScript.js') !!}"></script>
    <script type="text/javascript">
        $('#editForm').submit(function(){
            var name = $('#editForm').find('#name').val();
            var current_password = $('#editForm').find('#current_password').val();
            var new_password = $('#editForm').find('#new_password').val();
            var new_password_confirmation = $('#editForm').find('#new_password_confirmation').val();
            var url ="{{ URL::to('updateajax') }}" ;
            var currentName = '<?php echo Auth::user()->name ;?>';
            $.ajax({
                type:'POST',
                url:url,
                data:{_token:'<?php echo csrf_token() ?>',
                    name:name, current_password:current_password,
                    new_password:new_password, new_password_confirmation:new_password_confirmation
                    },
                success:function(data){
                    if(data.result){
                        if(data.message == "Your Name is updated!"){
                            console.log($('#userName').html('<span class="glyphicon glyphicon-user" aria-hidden="true"></span>'+'\n'+
                                        name+'\n'+
                                    '<span class="caret"></span>'));
                            $('#alertName').html(data.message).removeClass('hide');
                            $('#alertPass').addClass('hide');
                            $('#alertWrongPass').addClass('hide');
                            $('#alertWrongConfirm').addClass('hide');
                            $('#editForm').find('#current_password').val("");
                            $('#editForm').find('#new_password').val("");
                            $('#editForm').find('#new_password_confirmation').val("");
                        }
                        else{//pass is updated
                            $('#alertName').addClass('hide');
                            $('#alertPass').html(data.message).removeClass('hide');
                            $('#alertWrongPass').addClass('hide');
                            $('#alertWrongConfirm').addClass('hide');
                            $('#editForm').find('#current_password').val("");
                            $('#editForm').find('#new_password').val("");
                            $('#editForm').find('#new_password_confirmation').val("");
                        }
                    }else if(!data.result){//current pass fault
                            $('#alertName').addClass('hide');
                            $('#alertPass').addClass('hide');
                            $('#alertWrongPass').html(data.message).removeClass('hide');
                            $('#alertWrongConfirm').addClass('hide');
                            $('#editForm').find('#current_password').val("");
                            $('#editForm').find('#new_password').val("");
                            $('#editForm').find('#new_password_confirmation').val("");
                            $('#editForm').find('#current_password').focus();
                    }
                },
                error:function(data){
                    var res = data.responseJSON;
                    var announce = "";
                    console.log(res);
                    for (var key in res){
                        if(key!='__proto__'){
                            announce += res[key][0]+'\n';
                        }
                    }
                        $('#alertName').addClass('hide');
                        $('#alertPass').addClass('hide');
                        $('#alertWrongPass').addClass('hide');
                        $('#alertWrongConfirm').html(announce).removeClass('hide');
                        $('#editForm').find('#current_password').val("");
                        $('#editForm').find('#new_password').val("");
                        $('#editForm').find('#new_password_confirmation').val("");
                        $('#editForm').find('#current_password').focus();
                }
            });
        })
    </script>
</body>
</html>
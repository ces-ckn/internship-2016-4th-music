<!DOCTYPE html>
<head>
<title>Sign In</title> <!–Tiêu đề trang web–>
<meta name="viewport" content="width=device-width, initial-scale=1.0"> <!–Tự động nhận dạng thiết bị để co vào cho phù hợp–>
<meta charset="UTF-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="stylesheet" href="{!! asset('user/css/bootstrap.min.css') !!}" type="text/css" /> <!– Gọi đến thư viện Bootstrap để sử dụng–>
<link rel="stylesheet" href="{!! asset('user/css/style.css') !!}" /> <!– Đây là file CSS của bạn–>
</head>
<body>
<!– Code bạn đặt trong đây–>
<header>
    <div class="container group">
            <a href="{{ url('') }}"><img src="{!! asset('user/images/logo.png') !!}" id="logo"></a>
            <form role="form" id="searchForm" method="get" action="{{ url('/result') }}" class="input-group">
                  <input id="searchBar" type="search" class="form-control" placeholder="Search" name="searchname" autocomplete="off" required>
                  <span class="input-group-btn">
                    <button class="btn btn-default" type="button"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                  </span>
            </form><!-- /input-group -->
    </div>
    <div id="resultSearch" style="margin-left:355px;width: 355px;height: 200px;z-index:1;position: absolute;padding-top: 10px;padding-left: 5px;color: #FF9517;overflow-y:scroll;font-size: medium" class="container group panel panel-default hidden">
                <audio id="audioSo"></audio>
            <ul id="listSong" style="list-style-type:none;color: #FF763A">
            </ul>
    </div>  
</header>
<div class="wrapPage" style="height: 700px">
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2"  style="margin-top: 70px;margin-left: 120px;">
            <div class="panel panel-warning">
                <div class="panel-heading" style="text-align: center;"><h4>Login</h4></div>
                <div class="panel-body">
                    <form class="form-horizontal " role="form" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        @if (session('warning'))
                            <div class="alert alert-warning">
                                {{ session('warning') }}
                            </div>
                        @endif
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="E-mail" autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" placeholder="Password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember"> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-warning">
                                    <i class="fa fa-btn fa-sign-in"></i> Sign In
                                </button>

                                <a class="btn btn-link" id="forgetP" href="{{ url('/password/reset') }}" style="color: #FF7E00;">Forgot Your Password?</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<!--include footer-->
@include('user.footer')

<!– Kết thúc Code của bạn–>
    <!– jQuery (necessary for Bootstrap’s JavaScript plugins) –>
    <script src="{!! asset('user/js/jquery-2.2.4.min.js') !!}"></script>
    <!– Include all compiled plugins (below), or include individual files as needed –>
    <script src="{!! asset('user/js/bootstrap.min.js') !!}"></script>
    <script src="{!! asset('user/js/myScript.js') !!}"></script>
    <script type="text/javascript">
    $(document).ready(function(){

    var socket=io.connect('https://codeengine-nodejs.herokuapp.com');
    socket.on('addresult',function(data){
        var idBtn = data['songId'].toString();
        var addBtn = $('#listSong').children('li').find('#'+idBtn);
        addBtn.hide();
    });
    socket.on('masteraddresult',function(data){
        var idBtn = data['songId'].toString();
        var mstBtn = $('#listSong').children('li').find('#'+'master'+idBtn);
        mstBtn.hide();
    });

    var timer;

    $('#searchForm').children('#searchBar').keyup(function(){
        var audioS = document.getElementById("audioSo");
        var playingS = null;
        var clickedS = 0;
        var dInput = this.value;
        var list=$('#resultSearch').children('#listSong');
        var fragment = document.createDocumentFragment();
        var config = {
            routes: [
                { searchajax: "{{ URL::to('searchajax') }}" }
            ]
        };
        clearTimeout(timer);
        if(dInput!=""){
        timer=setTimeout(function validate(){
            $.ajax({
                type:'GET',
                url:config.routes[0].searchajax,
                data:"keyword="+dInput,
                success:function(data){
                    list.empty();
                    var songs = data.songs
                    for(var num in songs){                      
                        var songId = songs[num]['song_id'];
                        var title = songs[num]['title'];
                        var artist = songs[num]['artist'];
                        var code128 = songs[num]['code128'];
                        var code320 = songs[num]['code320'];
                        var isInList = songs[num]['isInList'];
                        var isInMList = songs[num]['isInMList'];
                        <?php $url = URL::route("listentomusic",'thisisthetempsongid');?>
                        var listenurl = "<?php echo $url ?>";
                        var info = songId+"**"+title+"**"+artist+"**"+code128+"**"+code320;
                        listenurl = listenurl.replace('thisisthetempsongid',info);

                        var newAnchor = document.createElement('a');
                        newAnchor.setAttribute('href', listenurl);
                        newAnchor.textContent = songs[num]['title'] + "-" + songs[num]['artist'];
                        var ele = document.createElement('li');
                        var hrtag = document.createElement('hr');
                        var divFormInline = document.createElement("div");
                        divFormInline.setAttribute("class","form-inline");
                        var divFormGroup = document.createElement("div");
                        divFormGroup.setAttribute("class","form-group");
                        divFormGroup.style = 'margin-bottom: 5px;';

                        var imgPlay= document.createElement("IMG");
                        imgPlay.setAttribute("class","playImmediately");
                        imgPlay.setAttribute("src", "{!! asset('user/images/play16.png') !!}");
                        imgPlay.style= 'border: 1px solid #ddd;border-radius: 4px;padding: 2px;';
                        var srcSong = 'http://api.mp3.zing.vn/api/mobile/source/song/'+songs[num]['code128'];
                        imgPlay.setAttribute("id",srcSong);
                        
                        var imgAdd= document.createElement("IMG");
                        imgAdd.setAttribute("src", "{!! asset('user/images/add16.png') !!}");
                        imgAdd.setAttribute("class","addImmediately");
                        imgAdd.setAttribute("id",songId);
                        imgAdd.setAttribute("name",info);
                        imgAdd.style = 'border: 1px solid #ddd;border-radius: 4px;padding: 2px;margin-left: 5px';

                        if(isInList){
                            imgAdd.setAttribute('style','visibility:hidden');
                        }

                        var imgMasterAdd= document.createElement("IMG");
                        imgMasterAdd.setAttribute("src", "{!! asset('user/images/king.png') !!}");
                        imgMasterAdd.setAttribute("class","addMasterList");
                        imgMasterAdd.setAttribute("name",info);
                        imgMasterAdd.setAttribute("id","master"+songId);
                        imgMasterAdd.style = 'border: 1px solid #ddd;border-radius: 4px;padding: 2px;margin-left: 5px';

                        if(isInMList){
                            imgMasterAdd.setAttribute('style','visibility:hidden');
                        }

                        ele.appendChild(divFormInline);
                        ele.appendChild(divFormGroup);
                        ele.appendChild(newAnchor);
                        ele.appendChild(divFormGroup);
                        ele.appendChild(imgPlay);
                        <?php if(!Auth::guest()){ ?>
                            ele.appendChild(imgAdd);
                        <?php } ?>
                        <?php if(!Auth::guest()&&Auth::user()->admin){ ?>
                            ele.appendChild(imgMasterAdd);
                        <?php } ?>
                        ele.appendChild(hrtag);
                        fragment.appendChild(ele);
                    }
                    list.append(fragment);
                    $('#resultSearch').removeClass('hidden');

                    $('.playImmediately').click(function(){
                        clickedS = $(this).parent().index();
                        if(clickedS != playingS){
                            audioS.setAttribute("src",this.getAttribute("id"));
                            audioS.play();
                            this.setAttribute("src","{!! asset('user/images/stop16.png') !!}");
                            if(playingS != null){
                                console.log(list.find('li').eq(playingS));
                                console.log(list.find('li').eq(playingS).find(".playImmediately"));
                                list.find('li').eq(playingS).find(".playImmediately").attr("src","{!! asset('user/images/play16.png') !!}");
                            }
                            playingS = $(this).parent().index();
                            audio[0].pause();
                        }
                        else{
                            audioS.pause();
                            audioS.currentTime = 0;
                            audioS.src = "";
                            this.setAttribute("src","{!! asset('user/images/play16.png') !!}");
                            playingS = null;
                            if(audio[0].currentTime != 0){
                                audio[0].play();
                            }
                        }
                    });
                    $('.addImmediately').click(function(){
                        var paras = this.getAttribute("name").split("**");
                        var url = "{{ URL::to('/listen/addajax') }}"
                        var btn = $(this);
                        $.ajax({
                            type:'POST',
                            url:url,
                            data:{_token:'<?php echo csrf_token() ?>',
                            songId:paras[0],
                            title:paras[1],
                            artist:paras[2],
                            code128:paras[3],
                            code320:paras[4]
                        },
                        success:function(data){
                            
                        }
                        });
                    });
                    $('.addMasterList').click(function(){
                        var paras = this.getAttribute("name");
                        var url = "{{ URL::to('/addmasterajax') }}" ;
                        $.ajax({
                            type:'GET',
                            url:url,
                            data:{paras},
                        success:function(data){

                        }
                        });
                    });
                }
            });
        },1000);
        }
            else{
            list.empty();
            audioS.pause();
            audioS.currentTime = 0;
            audioS.src = "";
            $('#resultSearch').addClass('hidden');
            if(audio[0].currentTime != 0){
                audio[0].play();
            }
            
        }
        
    });

});
</script>
</body>
</html>
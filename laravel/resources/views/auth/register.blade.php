<!DOCTYPE html>
<head>
<title>Sign Up</title> <!–Tiêu đề trang web–>
<meta name="viewport" content="width=device-width, initial-scale=1.0"> <!–Tự động nhận dạng thiết bị để co vào cho phù hợp–>
<meta charset="UTF-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="stylesheet" href="{!! asset('user/css/bootstrap.min.css') !!}" type="text/css" /> <!– Gọi đến thư viện Bootstrap để sử dụng–>
<link rel="stylesheet" href="{!! asset('user/css/style.css') !!}" /> <!– Đây là file CSS của bạn–>
</head>
<body>
<!– Code bạn đặt trong đây–>
<header>
    <div class="container group">
            <a href="{{ url('') }}"><img src="{!! asset('user/images/logo.png') !!}" id="logo"></a>
            <form role="form" id="searchForm" method="get" action="{{ url('/result') }}" class="input-group">
                  <input id="searchBar" type="search" class="form-control" placeholder="Search" name="searchname" autocomplete="off" required>
                  <span class="input-group-btn">
                    <button class="btn btn-default" type="submit"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                  </span>
            </form><!-- /input-group -->
    </div>
    <div id="resultSearch" style="margin-left:355px;width: 320px;height: 200px;z-index:1;position: absolute;padding-top: 10px;padding-left: 5px;color: #FF9517;overflow-y:scroll;" class="container group panel panel-default hidden">
        <ul id="listSong" style="list-style-type:none;color: #FF763A">

        </ul>
    </div>  
</header>

    <!--<div id="menu">
        <nav class="navbar navbar-default" role="navigation">
            <!– Brand and toggle get grouped for better mobile display –>
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1″>
                <span class="sr-only"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                </button>
            </div>
            <!– Collect the nav links, forms, and other content for toggling –>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                <li id="hot"><a href="{{ url('') }}">Hot</a></li>
                <li><a href="#">Music</a></li>
                </ul>
            </div><!– /.navbar-collapse –>
        </nav>
    </div>-->


<div class="wrapPage" style="height: 700px">
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2" style="margin-top: 70px;margin-left: 120px;">
            <div class="panel panel-warning">
                <div class="panel-heading" style="text-align: center;"><h4>Register</h4></div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Name" autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="E-mail">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" placeholder="Password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password">

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-warning">
                                    <i class="fa fa-btn fa-user"></i> Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<!--include footer-->
@include('user.footer')

<!– Kết thúc Code của bạn–>
    <!– jQuery (necessary for Bootstrap’s JavaScript plugins) –>
    <script src="{!! asset('user/js/jquery-2.2.4.min.js') !!}"></script>
    <!– Include all compiled plugins (below), or include individual files as needed –>
    <script src="{!! asset('user/js/bootstrap.min.js') !!}"></script>
    <script src="{!! asset('user/js/myScript.js') !!}"></script>
    <script type="text/javascript">
    $(document).ready(function(){
        $('#searchForm').children('#searchBar').keyup(function(){
            var dInput = this.value;
            var list=$('#resultSearch').children('#listSong');
            var fragment = document.createDocumentFragment();
            var config = {
                routes: [
                    { searchajax: "{{ URL::to('searchajax') }}" }
                ]
            };
            if(dInput!=""){
                $.ajax({
                    type:'GET',
                    url:config.routes[0].searchajax,
                    data:"keyword="+dInput,
                    success:function(data){
                        list.empty();
                        var songs = data.songs
                        for(var num in songs){
                            console.log(songs[num]);
                            var newAnchor = document.createElement('a');
                            var songId = songs[num]['song_id'];
                            <?php $url = URL::route("listentomusic",'thisisthetempsongid');?>
                            var listenurl = "<?php echo $url ?>";
                            listenurl = listenurl.replace('thisisthetempsongid',songId.toString());
                            newAnchor.setAttribute('href', listenurl);
                            newAnchor.textContent = songs[num]['title'] + "-" + songs[num]['artist'];
                            var ele = document.createElement('li');
                            var hrtag = document.createElement('hr');
                            ele.appendChild(newAnchor);
                            ele.appendChild(hrtag);
                            fragment.appendChild(ele);
                        }
                        list.append(fragment);
                        $('#resultSearch').removeClass('hidden');
                    }
                });
            }else{
                list.empty();
                $('#resultSearch').addClass('hidden');
            }
        });
    });
    </script>
</body>
</html>
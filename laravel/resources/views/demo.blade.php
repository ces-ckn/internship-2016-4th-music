<?php
use App\Song;
?>
<!DOCTYPE html>
<head>
<title>Home</title> <!–Tiêu đề trang web–>
<meta name="viewport" content="width=device-width, initial-scale=1.0"> <!–Tự động nhận dạng thiết bị để co vào cho phù hợp–>
<meta charset="UTF-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="stylesheet" type="text/css" href="{!! asset('user/css/bootstrap.min.css') !!}" /> <!– Gọi đến thư viện Bootstrap để sử dụng–>
<link rel="stylesheet" type="text/css" href="{!! asset('user/css/style.css') !!}"/> <!– Đây là file CSS của bạn–>
<link rel="stylesheet" type="text/css" href="{!! asset('user/css/style1.css') !!}"/> <!– Đây là file CSS của bạn–>
</head>
<body>
<!– Code bạn đặt trong đây–>

<!--include header-->
@include('user.header')

<div class="wrapPage" style="height: 100%;margin-bottom: 0px">
<div class="form-horizontal formPlay">
		<div class="form-group panel panel-warning" style="width: 625px;margin-top: 50px;">
		  		<div class="panel-body" style="height: 250px;">
			  		<audio id="player__source" preload="auto">
						<p>Your browser does not support HTML5 Audio</p>
						<source src="http://www.archive.org/download/CanonInD_261/CanoninD.mp3" type="audio/ogg">
					</audio>
		  		</div>
		  		<div class="panel-heading" style="height: 50px;padding-right:5px;">

		      	</div>
		</div>
		<div class="form-group">
		<div class="form-group panel panel-default listSong" style="width: 650px;">
		<div class="panel-body" style="height: 100%">
		<ul id="playList">
				@foreach($listSong as $song)
				<?php 
					$url = URL::route("listentomusic",$song->song_id); 
	                $urlup = URL::route("voteup", $song->song_id);
	                $urldown = URL::route("votedown", $song->song_id);
	            ?>
	            <?php
					$api = 'http://api.mp3.zing.vn/api/mobile/song/getsonginfo?requestdata={"id":'.$song->song_id.'}';
					$content = file_get_contents($api) or die("Can't open URL");
					$result = json_decode($content,true);
					$songlisten = new Song($result);
					$urlsource = $songlisten->source['128'];
				?>
            	<li><a href="{{$urlsource}}">{{$song->title}}</a></li>
            	@endforeach
		</ul>
		</div>
		</div>
		</div>
</div>
</div>

<!--include footer-->
@include('user.footer')
<!– Kết thúc Code của bạn–>
    <!– jQuery (necessary for Bootstrap’s JavaScript plugins) –>
    <script src= "{!! asset('user/js/jquery-2.2.4.min.js') !!}"></script>
    <!– Include all compiled plugins (below), or include individual files as needed –>
    <script src= "{!! asset('user/js/bootstrap.min.js') !!}"></script>
    <script src="{!! asset('user/js/myScript.js') !!}"></script>
    <script type="text/javascript">
	var audio, playbtn, seekslider, volumeslider, seeking=false, seekto;
	function initAudioPlayer(){
		audio = new Audio();
		audio.src = "audio/Stoker.mp3";
		audio.loop = true;
		audio.play();
		// Set object references
		playbtn = document.getElementById("playpausebtn");
		mutebtn = document.getElementById("mutebtn");
		seekslider = document.getElementById("seekslider");
		volumeslider = document.getElementById("volumeslider");
		// Add Event Handling
		playbtn.addEventListener("click",playPause);
		mutebtn.addEventListener("click", mute);
		seekslider.addEventListener("mousedown", function(event){ seeking=true; seek(event); });
		seekslider.addEventListener("mousemove", function(event){ seek(event); });
		seekslider.addEventListener("mouseup",function(){ seeking=false; });
		volumeslider.addEventListener("mousemove", setvolume);
		// Functions
		function playPause(){
			if(audio.paused){
			    audio.play();
			    playbtn.style.background = "url(images/pause.png) no-repeat";
		    } else {
			    audio.pause();
			    playbtn.style.background = "url(images/play.png) no-repeat";
		    }
		}
		function mute(){
			if(audio.muted){
			    audio.muted = false;
			    mutebtn.style.background = "url(images/speaker.png) no-repeat";
		    } else {
			    audio.muted = true;
			    mutebtn.style.background = "url(images/speaker_muted.png) no-repeat";
		    }
		}
		function seek(event){
		    if(seeking){
			    seekslider.value = event.clientX - seekslider.offsetLeft;
		        seekto = audio.duration * (seekslider.value / 100);
		        audio.currentTime = seekto;
		    }
	    }
		function setvolume(){
		    audio.volume = volumeslider.value / 100;
	    }
	}
	window.addEventListener("load", initAudioPlayer);
	//***********************************/
$(document).ready(function () {
var audio;
var playlist;
var tracks;
var current;

init();
function init() {
    current = 0;
    audio = $('#player__source');
    playlist = $('#playList');
    tracks = playlist.find('li a');
    len = tracks.length - 1;
    //audio[0].volume = 1;
    audio[0].play();
    playlist.find('a').click(function (e) {
        e.preventDefault();
        link = $(this);
        current = link.parent().index();
        run(link, audio[0]);
    });
    audio[0].addEventListener('ended', function (e) {
        current++;
        if (current == len) {
            current = 0;
            link = playlist.find('a')[0];
        } else {
            link = playlist.find('a')[current];
        }
        run($(link), audio[0]);
    });
}
function run(link, player) {
    player.src = link.attr('href');
    par = link.parent();
    //par.addClass('active').siblings().removeClass('active');
    audio[0].load();
    audio[0].play();
    }
})
    </script>
</body>
</html>

    <script type="text/javascript">
		$(document).ready(function () {
		var audio;
		var playlist;
		var tracks;
		var current;
		var playbtn, seekslider, volumeslider, seeking=false, seekto;

		init();
		function init() {
		    current = 0;
		    audio = $('#player__source');
		    playlist = $('#playList');
		    tracks = playlist.find('tr a');
		    len = tracks.length - 1;
		    audio[0].play();
		    playlist.find('a').click(function (e) {
		        e.preventDefault();
		        link = $(this);
		        current = link.parent().index();
		        run(link, audio[0]);
		    });
		    audio[0].addEventListener('ended', function (e) {
		        current++;
		        if (current == len) {
		            current = 0;
		            link = playlist.find('a')[0];
		        } else {
		            link = playlist.find('a')[current];
		        }
		        run($(link), audio[0]);
		    });
		}
		function run(link, player) {
		    player.src = link.attr('href');
		    par = link.parent();
		    par.addClass('active').siblings().removeClass('active');
		    audio[0].load();
		    audio[0].play();
		    }
		}
	)
    </script>
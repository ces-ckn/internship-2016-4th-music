<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlockedSong extends Model
{
    protected $table = 'block_songs';
}

<?php

namespace App\Http\Controllers;
use Hash;
use Illuminate\Http\Request;
use App\User;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
class UserController extends Controller
{
    public function edit($id){
    	$user = User::find($id);
    	return view('user.edit')->with(['user'=>$user,'message'=>'']);
    }
    public function update(Requests\UserEditRequest $request, $id){

    	$current_password  = $request->get('current_password');
        $user = User::find($id);
        if($current_password == ""){
            $user->update([
            'name' => $request->get('name')
            ]);
            return view('user.edit',['user'=>$user,'message'=>'Your profile is updated!']);
        }
        if(Hash::check($current_password, $user->password)){
            $tmp_password = $request->get('new_password');
            $new_password = \Hash::make($tmp_password);
        	$user->update([
            'name' => $request->get('name'),
            'password'=> $new_password
        	]);
        	return view('user.edit',['user'=>$user,'message'=>'Your profile is updated!']);
		}
    }
}

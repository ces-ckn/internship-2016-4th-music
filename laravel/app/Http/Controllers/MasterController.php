<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\MasterList;
use SoftDeletes; 
use App\Song;
use App\ListSong;
use Illuminate\Support\Facades\Input;
use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version1X;
use ElephantIO\Exception\ServerConnectionFailureException;
class MasterController extends Controller
{
    public function index(){
        $list = MasterList::onlyTrashed();
        $list->forceDelete();
    	$masterList = MasterList::orderBy('position', 'asc')->get();
    	if($masterList->isEmpty()){
            return view('admin.setTime')->with(['isListEmpty'=>true,'deletedId'=>""]);
        }
        else{
        return view('admin.setTime', ['masterList' => $masterList, 'isListEmpty' => false,'deletedId'=>""]);
        }
    	
    }
    public function indextmp($songId){
        $masterList = MasterList::orderBy('position', 'asc')->get();
        if($masterList->isEmpty()){
            return view('admin.setTime')->with(['isListEmpty'=>true,'deletedId'=>$songId]);
        }
        else{
        return view('admin.setTime', ['masterList' => $masterList, 'isListEmpty' => false,'deletedId'=>$songId]);
        }
    }

    public function slave($whichList='user',$limit=-1){
        if($limit<0){
            if($whichList=='master'){
                $masterList = MasterList::orderBy('position', 'asc')->get();
            }
            else{
                $masterList = ListSong::orderBy('position', 'asc')->get();
            }
        }else{
            if($whichList=='master'){
                $masterList = MasterList::orderBy('position', 'asc')->take($limit)->get();
            }
            else{
                $masterList = ListSong::orderBy('position', 'asc')->take($limit)->get();
            }

        }
        if($masterList->isEmpty()){
            return view('admin.slave')->with('isListEmpty', true);
        }
        else{
            return view('admin.slave', ['masterList' => $masterList, 'isListEmpty' => false]);
        }
    }

    public function add($paras1){
    	$song = new MasterList;
        $paras = explode("**",$paras1);
	    $song->id_song = $paras[0];
	    $song->position = MasterList::count()+1;
        $song->title = $paras[1];
        $song->artist = $paras[2];
        $song->code128 = $paras[3];
        $song->code320 = $paras[4];
	    $song->save();
	    return redirect()->route('master');
    }

    public function delete($songId){
    	$deletedRow = MasterList::where('id_song', $songId)->first();
    	$updateList = MasterList::where('position', '>', $deletedRow['position']);
    	$updateList->decrement('position', 1);
    	$deletedRow->delete();
        return redirect()->route('mastertmp',$songId);
    }

    public function search($songId){
        $api = 'http://api.mp3.zing.vn/api/mobile/song/getsonginfo?requestdata={"id":'.$songId.'}';
        $content = file_get_contents($api) or die("Can't open URL");
        $result = json_decode($content,true);
        $songlisten = new Song($result);
        return ($songlisten);
    }

    public function up($songId){
    	$updateRow = MasterList::where('id_song', $songId)->first();
        
        if($updateRow['position']<=1) return redirect()->route('master');
    	 //save position of song above
    	$positionOfSongAbove = $updateRow['position']-1;
    	$updateRowAbove = MasterList::where('position', $positionOfSongAbove)->increment('position', 1);
    	$updateRow->decrement('position', 1);
    	return redirect()->route('master');
    }

    public function down($songId){
    	$updateRow = MasterList::where('id_song', $songId)->first();
        if($updateRow['position']>=MasterList::count()) return redirect()->route('master');
    	$positionOfSongUnder = $updateRow['position']+1;
    	$updateRowUnder = MasterList::where('position', $positionOfSongUnder)->decrement('position', 1);
    	$updateRow->increment('position', 1);
    	return redirect()->route('master');
    }

    public function deleteAll(){
    	MasterList::truncate();
    	return redirect()->route('master');
    }

    public function addajax(){
        $paras1 = Input::get('paras','Nothing');
        $song = new MasterList;
        $paras = explode("**",$paras1);
        $song->id_song = $paras[0];
        $song->position = MasterList::count()+1;
        $song->title = $paras[1];
        $song->artist = $paras[2];
        $song->code128 = $paras[3];
        $song->code320 = $paras[4];
        $song->save();

        $client = new Client(new Version1X("https://codeengine-nodejs.herokuapp.com")); 
        //$client = new Client(new Version1X("127.0.0.1:5000"));         
        $client->initialize();
        $client->emit('masteradd', ['songId'=>$paras[0]]);
        $client->emit('masterlistaddsong',['songlisten'=>$song]);
        $client->close();

        return response()->json(array('result'=>'success'),200);
    }

    public function undo($songId){
        MasterList::withTrashed()->where('id_song', $songId)->restore();
        $deletedRow = MasterList::where('id_song', $songId)->first();
        $updatedRow = MasterList::where('position', $deletedRow['position'])->where('id_song','!=',$songId);
        $updatedRows = MasterList::where('position','>',$deletedRow['position']);
        if($updatedRows){
            $updatedRows->increment('position',1);
        }
        if($updatedRow){
            $updatedRow->increment('position',1);
        }
        return redirect()->route('master');
    }
}

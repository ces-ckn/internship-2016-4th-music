<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version1X;
use ElephantIO\Exception\ServerConnectionFailureException;
class MusicController extends Controller {

    protected $ipAddress;

    public function __construct(Request $request) {

        $this->ipAddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $this->ipAddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $this->ipAddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $this->ipAddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $this->ipAddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
            $this->ipAddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $this->ipAddress = getenv('REMOTE_ADDR');
        else
            $this->ipAddress = 'UNKNOWN';

    }
    // Create view
    public function createOpenMusicView() {
    	return view('openmusic');
    }

    // Open music
    public function openMusic(Request $request) {

    	$client = new Client(new Version1X("https://codeengine-nodejs.herokuapp.com"));			
		$client->initialize();		
        
        //if ($this->ipAddress == '54.205.207.199') {
        $client->emit('open', ['username' => "cube", 'msgcontent'=> "st"]);
        //}

        //$client->emit('ip', ['ip' => $this->ipAddress]);
        
    	$client->close();
    }

    // Close music
    public function closeMusic() {
        $client = new Client(new Version1X("https://codeengine-nodejs.herokuapp.com")); 
        //$client = new Client(new Version1X("localhost:5000"));         
        $client->initialize();

        //if ($this->ipAddress == '54.205.207.199') {
        $client->emit('close', ['username' => "cube", 'msgcontent'=> "st"]);
        //}

        //$client->emit('ip', ['ip' => $this->ipAddress]);

        $client->close();
    }

}

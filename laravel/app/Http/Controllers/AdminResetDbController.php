<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version1X;
use ElephantIO\Exception\ServerConnectionFailureException;

class AdminResetDbController extends Controller {

    public function index(){
    	return view('admin/adminresetdb');
    }

    public function emitResetDbTime(Request $request){
 
    	$time = $request->get('timeReset');
    	//echo $time; exit();
		$timeArray = explode(":", $time);
		$hour = trim($timeArray[0]);
		$minute = trim($timeArray[1]);

		$timeSplit = str_split($time);
		if($timeSplit[0] == '0'){
			$hour = $timeSplit[1];
		}
		if($timeSplit[5] == '0'){
			$minute = $timeSplit[6];
		}

		$hourInt = (int)$hour;
		if ($hourInt < 7) {
			$hourInt = $hourInt + 24 - 7;
		} else {
			$hourInt -= 7;
		}

		$hour = $hourInt . '';
	
		$client = new Client(new Version1X("https://codeengine-nodejs.herokuapp.com"));
		//$client = new Client(new Version1X("localhost:5000"));		
		$client->initialize();
		$client->emit('setresetdbtime', ['hour' => $hour, 'minute'=> $minute]);
    	$client->close();
    	echo "Successfully !";
    }
    
}
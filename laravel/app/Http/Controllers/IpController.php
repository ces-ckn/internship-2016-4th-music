<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class IpController extends Controller {
	
	public function getIp(Request $request) {
  		echo $request->ip();
	}
}

<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\ListSong;
use App\Song;
use Carbon\Carbon;
use App\UserVotetimes;
use App\UserSong;
use Auth;
use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version1X;
use ElephantIO\Exception\ServerConnectionFailureException;
class AjaxControllerAdd extends Controller
{
    public function add(Request $request){
    	$songId = $request->get('songId');
        $title = $request->get('title');
        $artist = $request->get('artist');
        $code128 = $request->get('code128');
        $code320 = $request->get('code320');
    	$song = new ListSong;
	    $song->id_song = $songId;
        $song->title = $title;
        $song->artist = $artist;
        $song->code128 = $code128;
        $song->code320 = $code320;
	    $song->user_id = Auth::id();
	    $song->position = ListSong::count()+1;
	    $song->save();

	    $client = new Client(new Version1X("https://codeengine-nodejs.herokuapp.com"));         
        $client->initialize();
        $client->emit('addsong', ['songId'=>$songId]);
        $client->emit('listaddsong',['songlisten'=>$song]);
        $client->close();

	    return response()->json(array('result'=>'success'),200);
    }
}

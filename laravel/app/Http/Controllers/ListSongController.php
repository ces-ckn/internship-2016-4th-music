<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\ListSong;

use App\Song;

use App\BlockedSong;

use Carbon\Carbon;

use App\UserVotetimes;

use App\UserSong;

use Auth;

use Illuminate\Pagination\LengthAwarePaginator;

use Illuminate\Support\Collection;

use URL;

class ListSongController extends Controller
{
    public function index(){
    	$listSong = ListSong::orderBy('position', 'asc')->get();
        
        if($listSong->isEmpty()){
            return view('user.master')->with(['isListEmpty'=>true]);
        }
        else{
            if(Auth::user()){
                foreach ($listSong as $song) {
                    $song['isVoted'] = self::isSongVoted($song->id_song);
                }
                if(self::userVotetimes()==5){
                    $isVoted5Times = true;
                }
                else $isVoted5Times = false;
                $userVoteleft = 5 - self::userVotetimes();
                return view('user.master', ['listSong' => $listSong, 'isListEmpty' => false, 'isVoted5Times' => $isVoted5Times,'userVoteleft'=>$userVoteleft]);
            }
            else return view('user.master', ['listSong' => $listSong, 'isListEmpty' => false]);
        }
 
    }

    public function search($songId){
    	$api = 'http://api.mp3.zing.vn/api/mobile/song/getsonginfo?requestdata={"id":'.$songId.'}';
		$content = file_get_contents($api) or die("Can't open URL");
		$result = json_decode($content,true);
		$songlisten = new Song($result);
    	return ($songlisten);
    }

    public function add($songId){
        $result = ListSong::where('id_song', $songId)->get();
        if($result->isEmpty()){
            $song = new ListSong;
            $song->id_song = $songId;
            $song->user_id = Auth::id();
            $song->position = ListSong::count()+1;
            $song->save();
            $msg = 'Add successfully';
        } else{
            $isInList = true;
            $msg = 'Song has been already in list';
        }
    	
    	return redirect()->route('listentomusic', $songId);	
    }

    public function delete($songId){
    	$deletedRow = ListSong::where('id_song', $songId)->first();
    	if ($deletedRow['user_id'] == Auth::id() && $deletedRow['voted'] == false){
    	//change position for songs under delete song
    	$updateList = ListSong::where('position', '>', $deletedRow['position']);
    	$updateList->decrement('position', 1);
    	$deletedRow->delete();
    	}
        return redirect()->route('listaddedtmp',$songId);
    }

    public function voteUp($songId){
    	$updateRow = ListSong::where('id_song', $songId)->first();
        
        if($updateRow['position']<=1)return;
        //check 'voted' if not user who added
        $userId = Auth::user()->id;
        $isUserAdded = !(ListSong::where('id_song', $songId)->where('user_id', $userId)->get()->isEmpty());
        if(!$isUserAdded){
            $updateRow->update(['voted' => true]);
        }
    	 //save position of song above
    	$positionOfSongAbove = $updateRow['position']-1;
    	$updateRowAbove = ListSong::where('position', $positionOfSongAbove)->increment('position', 1);
    	$updateRow->decrement('position', 1);
    }

    public function voteDown($songId){
    	$updateRow = ListSong::where('id_song', $songId)->first();
        if($updateRow['position']>=ListSong::count())return;
        //check 'voted' if not user who added
        $userId = Auth::user()->id;
        $isUserAdded = !(ListSong::where('id_song', $songId)->where('user_id', $userId)->get()->isEmpty());
        if(!$isUserAdded){
            $updateRow->update(['voted' => true]);
        }
    	$positionOfSongUnder = $updateRow['position']+1;
    	$updateRowUnder = ListSong::where('position', $positionOfSongUnder)->decrement('position', 1);
    	$updateRow->increment('position', 1);
    }

    public function isSongVoted($songId){
        $user = Auth::user();
        $userId = $user->id;
        $userSong = UserSong::where('user_id',$userId)->where('song_id',$songId)->get();
        if(!$userSong->isEmpty()){
            return true;
        }else{
            return false;
        }

    }

    public function userVotetimes(){
        $user = Auth::user();
        $userId = $user->id;
        $userVote = UserVotetimes::where('user_id',$userId)->first();
        if($userVote!=null){
            return $userVote['votetimes'];
        }else{
            return 0;
        }
    }

    public function doVoteUp($songId){
        $voteTimes = self::userVotetimes();
        $user = Auth::user();
        $userId = $user->id;
        if(self::isSongVoted($songId)||$voteTimes==5){
            return false;
        }
        self::voteUp($songId);
        if($voteTimes==0){
            UserVotetimes::create([
                'user_id'=>$userId,
                'votetimes'=>1]);
        }else{
            $updateRow = UserVotetimes::where('user_id', $userId)->first();
            $updateRow->increment('votetimes',1);
        }
        UserSong::create([
            'user_id'=>$userId,
            'song_id'=>$songId]);
        return true;
    }

    public function doVoteDown($songId){
        $voteTimes = self::userVotetimes();
        $user = Auth::user();
        $userId = $user->id;
        if(self::isSongVoted($songId)||$voteTimes==5){
            return false;
        }
        self::voteDown($songId);
        if($voteTimes==0){
            UserVotetimes::create([
                'user_id'=>$userId,
                'votetimes'=>1]);
        }else{
            $updateRow = UserVotetimes::where('user_id', $userId)->first();
            $updateRow->increment('votetimes',1);
        }
        UserSong::create([
            'user_id'=>$userId,
            'song_id'=>$songId]);
        return true;
    }

    public function isInList(Request $request){
        $songId = $request->songId;
        $result = ListSong::where('id_song', $songId)->get();
        if($result->isEmpty()){
            $isInList = false;
        } else{
            $isInList = true;
        }
        return response()->json(array('result' => $isInList), 200);
    }

    public function isBlocked(Request $request){
        $songId = $request->songId;
        $result = BlockedSong::where('id_song', $songId)->get();
        if($result->isEmpty()){
            $isBlocked = false;
        } else{
            $isBlocked = true;
        }
        return response()->json(array('result' => $isBlocked), 200);
    }

    public function listSongAdded(){
        $user = Auth::user();
        $userId = $user->id;
        $i = 0;
        $list = ListSong::onlyTrashed()->where('user_id', $userId);
        $list->forceDelete();
        $listSongAdded = ListSong::where('user_id', $userId)->orderBy('position','asc')->get();
        if($listSongAdded->isEmpty()){
            return view('user.listSongAdded')->with(['isListEmpty'=>true, 'deletedId'=>""]);
        }
        else{
            /*$currentPage = LengthAwarePaginator::resolveCurrentPage();
            $col = new Collection($listSongAdded);
            $perPage = 10;
            $currentPageSearchResults = $col->slice(($currentPage - 1) * $perPage, $perPage)->all();
            $entries = new LengthAwarePaginator($currentPageSearchResults, count($col), $perPage);
            $entries->setPath(URL::route("listadded"));/*/
            return view('user.listSongAdded', ['listAdded' => $listSongAdded, 'isListEmpty'=> false,'deletedId'=>""]);
        }
    }

    public function listSongAddedTmp($songId){
        $user = Auth::user();
        $userId = $user->id;
        $i = 0;
        $listSongAdded = ListSong::where('user_id', $userId)->orderBy('position','asc')->get();
        if($listSongAdded->isEmpty()){
            return view('user.listSongAdded')->with(['isListEmpty'=>true, 'deletedId'=>""]);
        }
        else{
            $currentPage = LengthAwarePaginator::resolveCurrentPage();
            $col = new Collection($listSongAdded);
            $perPage = 10;
            $currentPageSearchResults = $col->slice(($currentPage - 1) * $perPage, $perPage)->all();
            $entries = new LengthAwarePaginator($currentPageSearchResults, count($col), $perPage);
            $entries->setPath(URL::route("listadded"));
            return view('user.listSongAdded', ['listAdded' => $entries, 'isListEmpty'=> false, 'deletedId'=>$songId]);
        }
    }
    public function undo($songId){
        ListSong::withTrashed()->where('id_song', $songId)->restore();
        $deletedRow = ListSong::where('id_song', $songId)->first();
        $updatedRow = ListSong::where('position', $deletedRow['position'])->where('id_song','!=',$songId);
        $updatedRows = ListSong::where('position','>',$deletedRow['position']);
        if($updatedRows){
            $updatedRows->increment('position',1);
        }
        if($updatedRow){
            $updatedRow->increment('position',1);
        }
        return redirect()->route('listadded');
    }
}

<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version1X;
use ElephantIO\Exception\ServerConnectionFailureException;

class AdminSlaverNumController extends Controller{
    public function index() {
    	return view('admin.adminslavernum');
    }

    public function setSlaverNumber(Request $request) {
    	$slaverNumber = $request->get('slaverNumber');
		
		//echo $slaverNumber; exit();
		$client = new Client(new Version1X("https://codeengine-nodejs.herokuapp.com"));
		//$client = new Client(new Version1X("localhost:5000"));
					
		$client->initialize();
		$client->emit('setslavernumber', ['slaverNumber' => $slaverNumber]);
    	$client->close();

    	echo "Succesfully !";
    }    
}

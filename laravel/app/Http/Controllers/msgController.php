<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version1X;
use ElephantIO\Exception\ServerConnectionFailureException;
class msgController extends Controller
{
	public function sendmsg(){
		$name=$_GET['username'];
		$msg=$_GET['textbox'];
		$client = new Client(new Version1X("https://codeengine-nodejs.herokuapp.com"));			
		$client->initialize();
		$client->emit('new_msg', ['username' => $name, 'msgcontent'=> $msg]);
    	$client->close();
        return ($name+$msg);
	}
	public function index(){
		$username=rand(100,999);
		return view('chat')->with('username',$username);
	}
}
<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version1X;
use ElephantIO\Exception\ServerConnectionFailureException;

class AdminPlaylistController extends Controller {

    public function index(){
    	return view('admin/adminplaylist');
    }
    
    public function setSongNumber(Request $request){

    	$songNumber = $request->get('songNumber');
	
		$client = new Client(new Version1X("https://codeengine-nodejs.herokuapp.com"));
		//$client = new Client(new Version1X("localhost:5000"));			
		$client->initialize();
		$client->emit('setsongnumber', ['songNumber' => $songNumber]);
    	$client->close();

    	echo "Setting song number succesfully !\n n = " . $songNumber;
    }    

    public function emitOpenTime(Request $request){	
    
    	$time = $request->input('opentime');
		$timeArray = explode(":", $time);
		$hour = $timeArray[0];
		$minute = $timeArray[1];

		$timeSplit = str_split($time);
		if($timeSplit[0] == '0'){
			$hour = $timeSplit[1];
		}
		if($timeSplit[3] == '0'){
			$minute = $timeSplit[4];
		}

		$hourInt = (int)$hour;
		if ($hourInt < 7) {
			$hourInt = $hourInt + 24 - 7;
		} else {
			$hourInt -= 7;
		}

		$hour = $hourInt . '';
	
		$client = new Client(new Version1X("https://codeengine-nodejs.herokuapp.com"));
		//$client = new Client(new Version1X("localhost:5000"));		
		$client->initialize();
		$client->emit('setopentime', ['hour' => $hour, 'minute'=> $minute]);
    	$client->close();
    }

    public function emitClosedTime(Request $request){	
    
    	$time = $request->input('closedtime');
		$timeArray = explode(":", $time);
		$hour = $timeArray[0];
		$minute = $timeArray[1];

		$timeSplit = str_split($time);
		if($timeSplit[0] == '0'){
			$hour = $timeSplit[1];
		}
		if($timeSplit[3] == '0'){
			$minute = $timeSplit[4];
		}

		$hourInt = (int)$hour;
		if ($hourInt < 7) {
			$hourInt = $hourInt + 24 - 7;
		} else {
			$hourInt -= 7;
		}

		$hour = $hourInt . '';
	
		$client = new Client(new Version1X("https://codeengine-nodejs.herokuapp.com"));
		//$client = new Client(new Version1X("localhost:5000"));		
		$client->initialize();
		$client->emit('setclosedtime', ['hour' => $hour, 'minute'=> $minute]);
    	$client->close();
    }  

    public function emitTime(Request $request){

    	$openTime = $request->get('timeStart');
    	$closedTime = $request->get('timeStop');
    	//$songNumber = $request->get('songNumber');

    	// Open Time
		$timeArrayOpen = explode(":", $openTime);
		$hourOpen = trim($timeArrayOpen[0]);
		$minuteOpen = trim($timeArrayOpen[1]);

		$timeSplitOpen = str_split($openTime);
		if($timeSplitOpen[0] == '0'){
			$hourOpen = $timeSplitOpen[1];
		}
		if($timeSplitOpen[5] == '0'){
			$minuteOpen = $timeSplitOpen[6];
		}

		$hourIntOpen = (int)$hourOpen;
		if ($hourIntOpen < 7) {
			$hourIntOpen = $hourIntOpen + 24 - 7;
		} else {
			$hourIntOpen -= 7;
		}

		$hourOpen = $hourIntOpen . '';

		// Closed Time
		$timeArrayClosed = explode(":", $closedTime);
		$hourClosed = trim($timeArrayClosed[0]);
		$minuteClosed = trim($timeArrayClosed[1]);

		$timeSplitClosed = str_split($closedTime);
		if($timeSplitClosed[0] == '0'){
			$hourClosed = $timeSplitClosed[1];
		}
		if($timeSplitClosed[5] == '0'){
			$minuteClosed = $timeSplitClosed[6];
		}

		$hourIntClosed = (int)$hourClosed;
		if ($hourIntClosed < 7) {
			$hourIntClosed = $hourIntClosed + 24 - 7;
		} else {
			$hourIntClosed -= 7;
		}

		$hourClosed = $hourIntClosed . '';
	
		$client = new Client(new Version1X("https://codeengine-nodejs.herokuapp.com"));
		//$client = new Client(new Version1X("localhost:5000"));			
		$client->initialize();
		$client->emit('setopentime', ['hour' => $hourOpen, 'minute'=> $minuteOpen]);
		$client->emit('setclosedtime', ['hour' => $hourClosed, 'minute'=> $minuteClosed]);
		//$client->emit('setsongnumber', ['songNumber' => $songNumber]);
    	$client->close();

    	echo "Setting time succesfully !";
    }    
    
}
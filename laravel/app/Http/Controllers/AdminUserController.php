<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Illuminate\Pagination\LengthAwarePaginator;

use Illuminate\Support\Collection;

use URL;

use Auth;

use App\User;

class AdminUserController extends Controller
{	
	public function userList(){
		if(Auth::user()->superadmin==1){
            $userList = User::where('superadmin', 0)->get();
        }
        else{
            $userList = User::where('admin', 0)->get();       
        }
        if($userList->isEmpty()){
            return view('admin.listUser')->with('isListEmpty', true);
        }
        else{
            return view('admin.listUser', ['userList' => $userList, 'isListEmpty' => false]);
        }

	}

	public function adminizeUser(Request $request){
		if(Auth::user()->superadmin == 1){
            $id = $request->get('id');
            $user = User::find($id);
            $user->update([
                'admin' => 1,
                'blocked' => 0
            ]);
            return response()->json(array('result'=>'Success'),200);
        }else{
            return response()->json(array('result'=>'Failed'),200);
        }
        	
	}

	public function unadminizeUser(Request $request){
		if(Auth::user()->superadmin==1){
			$id = $request->get('id');
			$user = User::find($id);
    		$user->update([
            	'admin' => 0
        	]);
        	return response()->json(array('result'=>'Success'),200);
        }else{
        	return response()->json(array('result'=>'Failed'),200);
        }
	}

    public function blockUser(Request $request){
        $id = $request->get('id');
        $user = User::find($id);
    	if(Auth::user()->superadmin){
    		$user->update([
            	'blocked' => 1,
                'admin' => 0
        	]);
        	return response()->json(array('result'=>'Success'),200);
        }elseif(Auth::user()->admin){
            if($user->admin == 1){
                return response()->json(array('result'=>'U do not have permission to block an admin'),200);
            }else{
                $user->update([
                    'blocked' => 1
                ]);
                return response()->json(array('result'=>'Success'),200);
            }
        }
        else{
        	return response()->json(array('result'=>'U are not an admin'),200);
        }
    }

    public function unblockUser(Request $request){
    	if(Auth::user()->admin==1){
    		$id = $request->get('id');
			$user = User::find($id);
    		$user->update([
            	'blocked' => 0
        	]);
        	return response()->json(array('result'=>'Success'),200);
        }else{
        	return response()->json(array('result'=>'Failed'),200);
        }
    }
}

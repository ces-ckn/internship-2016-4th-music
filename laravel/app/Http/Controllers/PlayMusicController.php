<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use App\Song;
use App\ListSong;
use App\BlockedSong;
use Carbon\Carbon;
use App\SongSearch;
use Illuminate\Pagination\LengthAwarePaginator;

use Illuminate\Support\Collection;

use URL;
class PlayMusicController extends Controller
{
    public function search(){
    	return view('playmusic.search');
    }

    public function fetchData($name){
    	$para = str_replace(' ', '%20', $name);

    	$url = 'http://api.mp3.zing.vn/api/mobile/search/song?requestdata={"length":150,"start":0,'.
    	'"q":"'.$para.'","sort":"hot"}&keycode=b319bd16be6d049fdb66c0752298ca30&fromvn=true';

    	$content = file_get_contents($url) or die("Can't open URL");
		return $content;
    }

    public function result(){
    	$name = Input::get('searchname','Nothing Get');
    	$content = self::fetchData($name);
        $result = json_decode($content,true);
        $songs = $result['docs'];

        if(count($songs)){
            $i=0;
            foreach($songs as $song){
                if($song['source']['128']){
                    $songxs[$i]=new SongSearch($song);
                    $i++;
                }else{
                    continue;
                }
            }
            $currentPage = LengthAwarePaginator::resolveCurrentPage();
            $col = new Collection($songxs);
            $perPage = 15;
            $currentPageSearchResults = $col->slice(($currentPage - 1) * $perPage, $perPage)->all();
            $entries = new LengthAwarePaginator($currentPageSearchResults, count($col), $perPage);
            $entries->setPath(URL::route("result"));

        return view('user.resultSearch', ['songxs' => $entries, 'isResultEmpty' =>false, 'name' => $name]);
        }
        else{
            return view('user.resultSearch', ['isResultEmpty' => true]);
        }

        

    }

    public function listen($info){
    	$paras = (explode("**",$info));
        $song_id=$paras[0];
        $isInList = self::isInList($song_id);
        $isBlocked = self::isBlocked($song_id);
    	/*return view('playmusic.listen')->with('song_id',$song_id);*/
        return view('user.playSong',['paras'=>$paras,'isInList'=>$isInList, 'isBlocked' => $isBlocked]);
    }

    public function viewapi($song_id){
        $api = 'http://api.mp3.zing.vn/api/mobile/song/getsonginfo?requestdata={"id":'.$song_id.'}';
        $content = file_get_contents($api) or die("Can't open URL");
        return view('testapi', ['content' => $content]);
    }

    public function isInList($song_id){
        $result = ListSong::where('id_song', $song_id)->get();
        if($result->isEmpty()){
            $isInList = false;
        } else{
            $isInList = true;
        }
        return $isInList;
    }

    public function fetchData1($name){
        $para = str_replace(' ', '%20', $name);
        $url = 'http://api.mp3.zing.vn/api/mobile/search/song?requestdata={"length":20,"start":0,'.
        '"q":"'.$para.'","sort":"hot"}&keycode=b319bd16be6d049fdb66c0752298ca30&fromvn=true';

        $content = file_get_contents($url) or die("Can't open URL");
        return $content;
    }

    public function searchAjax(){
        $keyword = Input::get('keyword','Nothing');
        $content = self::fetchData1($keyword);
        $result = json_decode($content,true);
        $songs = $result['docs'];
        $i=0;
        foreach($songs as $song){
            if($song['source']['128']){
                $songxs[$i]=new SongSearch($song);
                $i++;
            }else{
                continue;
            }
        }
        return response()->json(array('songs'=>$songxs),200);
    }
    public function isBlocked($songId){
        $result = BlockedSong::where('id_song', $songId)->get();
        if($result->isEmpty()){
            $isBlocked = false;
        } else{
            $isBlocked = true;
        }
        return $isBlocked;
    }
}

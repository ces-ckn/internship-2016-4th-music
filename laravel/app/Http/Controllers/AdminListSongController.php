<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\ListSong;

use App\BlockedSong;

use App\User;

use App\Song;

use Illuminate\Pagination\LengthAwarePaginator;

use Illuminate\Support\Collection;

use URL;

class AdminListSongController extends Controller
{
    public function index(){
        $list = ListSong::onlyTrashed();
        $list->forceDelete();
        $listSong = ListSong::orderBy('position', 'asc')->get();
        if($listSong->isEmpty()){
            return view('admin.listsong')->with(['isListEmpty'=> true,'deletedId'=>""]);
        }
        else{
            $i=0;
            foreach ($listSong as $song) {
                $song->userId = $song['user_id'];
                $user = User::where('id', $song['user_id'])->first();
                $song->userEmail = $user['email']; 
            }
            /*$currentPage = LengthAwarePaginator::resolveCurrentPage();
            $col = new Collection($listSong);
            $perPage = 10;
            $currentPageSearchResults = $col->slice(($currentPage - 1) * $perPage, $perPage)->all();
            $entries = new LengthAwarePaginator($currentPageSearchResults, count($col), $perPage);
            $entries->setPath(URL::route("admin"));*/
        return view('admin.listsong', ['listSong' => $listSong, 'isListEmpty' => false, 'deletedId'=>""]);
        }
    }
    public function indextmp($songId){
        $listSong = ListSong::orderBy('position', 'asc')->get();
        if($listSong->isEmpty()){
            return view('admin.listsong')->with(['isListEmpty'=> true,'deletedId'=>""]);
        }
        else{
            $i=0;
            foreach ($listSong as $song) {
                $song->userId = $song['user_id'];
                $user = User::where('id', $song['user_id'])->first();
                $song->userEmail = $user['email']; 
            }
            $currentPage = LengthAwarePaginator::resolveCurrentPage();
            $col = new Collection($listSong);
            $perPage = 10;
            $currentPageSearchResults = $col->slice(($currentPage - 1) * $perPage, $perPage)->all();
            $entries = new LengthAwarePaginator($currentPageSearchResults, count($col), $perPage);
            $entries->setPath(URL::route("admin"));
        return view('admin.listsong', ['listSong' => $entries, 'isListEmpty' => false, 'deletedId'=>$songId]);
        }
    }

    public function blockedList(){
        $listBlocked = BlockedSong::get();
        if($listBlocked->isEmpty()){
            return view('admin.blockedlist')->with('isListEmpty', true);
        }
        else{
            foreach ($listBlocked as $song) {
                $song->userId = $song['user_id'];
                $user = User::where('id', $song['user_id'])->first();
                $song->userEmail = $user['email']; 
            }
            /*$currentPage = LengthAwarePaginator::resolveCurrentPage();
            $col = new Collection($listBlocked);
            $perPage = 10;
            $currentPageSearchResults = $col->slice(($currentPage - 1) * $perPage, $perPage)->all();
            $entries = new LengthAwarePaginator($currentPageSearchResults, count($col), $perPage);
            $entries->setPath(URL::route("adminblockedlist"));*/
            return view('admin.blockedlist', ['isListEmpty' => false, 'listBlocked' => $listBlocked]);
        }
    }

    public function delete($songId){
    	$deletedRow = ListSong::where('id_song', $songId)->first();
    	$updateList = ListSong::where('position', '>', $deletedRow['position']);
    	$updateList->decrement('position', 1);
    	$deletedRow->delete();
        return redirect()->route('admintmp',$songId);
    }

    public function block($paras){
        $paras = explode("**",$paras);
    	$blockedSong = new BlockedSong;
    	$blockedSong->id_song = $paras[0];
        $blockedSong->title = $paras[1];
        $blockedSong->artist = $paras[2];
        $blockedSong->code128 = $paras[3];
        $blockedSong->code320 = $paras[4];
        $songBlocked = ListSong::where('id_song', $paras[0])->first();
        $blockedSong->user_id = $songBlocked['user_id'];
    	$blockedSong->save();
    	self::delete($paras[0]);
        return redirect()->route('admin');
    }

    public function unblock($songId){
    	$unblockSong = BlockedSong::where('id_song', $songId)->first();
    	$unblockSong->delete();
        return redirect()->route('adminblockedlist');
    }

    public function search($songId){
        $api = 'http://api.mp3.zing.vn/api/mobile/song/getsonginfo?requestdata={"id":'.$songId.'}';
        $content = file_get_contents($api) or die("Can't open URL");
        $result = json_decode($content,true);
        $songlisten = new Song($result);
        return ($songlisten);
    }
    public function undo($songId){
        ListSong::withTrashed()->where('id_song', $songId)->restore();
        $deletedRow = ListSong::where('id_song', $songId)->first();
        $updatedRow = ListSong::where('position', $deletedRow['position'])->where('id_song','!=',$songId);
        $updatedRows = ListSong::where('position','>',$deletedRow['position']);
        if($updatedRows){
            $updatedRows->increment('position',1);
        }
        if($updatedRow){
            $updatedRow->increment('position',1);
        }
        return redirect()->route('admin');
    }
}

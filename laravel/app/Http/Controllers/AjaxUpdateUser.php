<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Hash;
use Auth;
use App\User;

use Illuminate\Support\Facades\Input;

class AjaxUpdateUser extends Controller
{
    public function update(Requests\UserEditRequest $request){

    	$current_password  = $request->get('current_password');
        $user = Auth::user();
        if($current_password == ""){
            $user->update([
            'name' => $request->get('name')
            ]);
            return response()->json(array('result'=>true,'message'=>'Your Name is updated!'),200);
        }
        if(Hash::check($current_password, $user->password)){
            $tmp_password = $request->get('new_password');
            $new_password = \Hash::make($tmp_password);
        	$user->update([
            'name' => $request->get('name'),
            'password'=> $new_password
        	]);
        	return response()->json(array('result'=>true,'message'=>'Your Password is updated!'),200);
		}else{
			return response()->json(array('result'=>false,'message'=>'Sorry! Your current password is incorrect!'),200);
		}
    }
}

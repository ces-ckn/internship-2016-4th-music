<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use DB;

class TaskScheduleController extends Controller
{
    public function resetTables() {
        DB::table('user_song')->truncate();
        DB::table('user_votetimes')->truncate();
    }

}

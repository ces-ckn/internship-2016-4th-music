<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\ListSongController;
use App\Http\Requests;
use App\ListSong;
use App\Song;
use Carbon\Carbon;
use App\UserVotetimes;
use App\UserSong;
use Auth;
use DB;
use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version1X;
use ElephantIO\Exception\ServerConnectionFailureException;
class AjaxController extends ListSongController
{
    public function up(Request $request ){
    	$songId = $request->get('songId');
    	$resulteVote=self::doVoteUp($songId);
        if(!$resulteVote){
            return response()->json(array('result'=>'Failed'),200);
        }
    	$song1=ListSong::where('id_song', $songId)->first();
        $pos1 = $song1['position'];
    	$pos2 = ($song1['position']) + 1;
        
        $client = new Client(new Version1X("https://codeengine-nodejs.herokuapp.com"));         
        $client->initialize();
        $client->emit('votesong', ['pos1'=>$pos1,'pos2'=>$pos2]);
        $client->close();

        $userId = Auth::user()->id;
        $vTimes = UserVotetimes::select('votetimes')->where('user_id',$userId)->first();
        return response()->json(array('result'=>'Success','vTimes'=>$vTimes),200);
    }

    public function dwn(Request $request ){
    	$songId = $request->get('songId');
    	$resulteVote=self::doVoteDown($songId);
        if(!$resulteVote){
            return response()->json(array('result'=>'Failed'),200);
        }
    	$song1=ListSong::where('id_song', $songId)->first();
        $pos1 = $song1['position'];
    	$pos2 = ($song1['position']) - 1;

        $client = new Client(new Version1X("https://codeengine-nodejs.herokuapp.com"));         
        $client->initialize();
        $client->emit('votesong', ['pos1'=>$pos1,'pos2'=>$pos2]);
        $client->close();

        $userId = Auth::user()->id;
        $vTimes = UserVotetimes::select('votetimes')->where('user_id',$userId)->first();
    	return response()->json(array('result'=>'Success','vTimes'=>$vTimes),200);
    }

}

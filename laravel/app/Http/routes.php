<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*Route::get('/', function () {
    return view('user.master');
});*/
Route::get('/template', function () {
    return view('user.template');
});
/*Route::get('/home', function () {
    return view('user.master');
});*/
Route::get('/resultSearch', function () {
    return view('user.resultSearch');
});
Route::get('/demo', function () {
    return view('demo');
});
Route::get('/slave/{list?}/{limit?}', ['as' => 'slave', 'uses' => 'MasterController@slave']);

Route::get('/','ListSongController@index');
Route::get('/register', 'HomeController@getRegister');
Route::auth();
Route::get('user/activation/{token}', 'Auth\AuthController@activateUser')->name('user.activate');
//Route::get('/home', 'HomeController@index');
Route::get('/search','PlayMusicController@search');
Route::get('/result', ['as' => 'result', 'uses' => 'PlayMusicController@result']);
Route::get('/listen/{source}', ['as' => 'listentomusic', 'uses' => 'PlayMusicController@listen']);
//Route::get('/home','ListSongController@index');
Route::get('/listen/{info}', ['as' => 'listentomusic', 'uses' => 'PlayMusicController@listen']);
Route::get('/add/{song_id}', ['middleware' => 'auth', 'as' =>'addsong', 'uses' => 'ListSongController@add']);
Route::resource('user','UserController');
Route::get('/delete/{song_id}', ['middleware' => 'auth','as' => 'deletesong', 'uses' => 'ListSongController@delete']);
Route::get('/voteup/{song_id}', ['middleware' => 'auth','as' => 'voteup', 'uses' => 'ListSongController@doVoteUp']);
Route::get('/votedown/{song_id}', ['middleware' => 'auth','as' => 'votedown', 'uses' => 'ListSongController@doVoteDown']);
Route::get('/listadded', ['middleware' => 'auth', 'as' => 'listadded', 'uses' => 'ListSongController@listSongAdded']);
Route::get('/listadded/{song_id}', ['middleware' => 'auth', 'as' => 'listaddedtmp', 'uses' => 'ListSongController@listSongAddedTmp']);
Route::get('/uundo/{song_id}',['middleware' => 'auth', 'as' => 'uundo', 'uses' => 'ListSongController@undo']);
Route::get('/viewapi/{song_id}',['as' => 'viewapi', 'uses' => 'PlayMusicController@viewapi']);
//route for admin
Route::get('/admin', ['middleware' => 'admin', 'as' => 'admin', 'uses' => 'AdminListSongController@index']);
Route::get('/admin/{song_id}', ['middleware' => 'admin', 'as' => 'admintmp', 'uses' => 'AdminListSongController@indextmp']);
Route::get('/aundo/{song_id}', ['middleware' => 'admin', 'as' => 'aundo', 'uses' => 'AdminListSongController@undo']);
Route::get('/admindelete/{song_id}', ['middleware' => 'admin', 'as' => 'admindelete', 'uses' => 'AdminListSongController@delete']);
Route::get('/adminblock/{song_id}', ['middleware' => 'admin', 'as' => 'adminblock', 'uses' => 'AdminListSongController@block']);
Route::get('/adminunblock/{song_id}', ['middleware' => 'admin', 'as' => 'adminunblock', 'uses' => 'AdminListSongController@unblock']);
Route::get('/adminblockedlist', ['middleware' => 'admin', 'as' => 'adminblockedlist', 'uses' => 'AdminListSongController@blockedList']);
//route for master
Route::get('/master', ['middleware' => 'admin', 'as' => 'master', 'uses' => 'MasterController@index']);
Route::get('/master/{song_id}',['middleware' => 'admin', 'as' => 'mastertmp', 'uses' => 'MasterController@indextmp']);
Route::get('/masteradd/{song_id}', ['middleware' => 'admin', 'as' => 'masteradd', 'uses' => 'MasterController@add']);
Route::get('/masterdel/{song_id}', ['middleware' => 'admin', 'as' => 'masterdel', 'uses' => 'MasterController@delete']);
Route::get('/masterup/{song_id}', ['middleware' => 'admin', 'as' => 'masterup', 'uses' => 'MasterController@up']);
Route::get('/masterdown/{song_id}', ['middleware' => 'admin', 'as' => 'masterdown', 'uses' => 'MasterController@down']);
Route::get('/masterdelall', ['middleware' => 'admin', 'as' => 'masterdelall', 'uses' => 'MasterController@deleteAll']);
Route::get('/undo/{song_id}', ['middleware' => 'admin', 'as' => 'masterundo', 'uses' => 'MasterController@undo']);
//route for ajax
Route::post('/upajax', ['middleware' => 'auth', 'uses' => 'AjaxController@up']);
Route::post('/dwnajax',['middleware' => 'auth', 'uses' => 'AjaxController@dwn']);
Route::post('/listen/addajax',['middleware' => 'auth', 'uses' => 'AjaxControllerAdd@add']);


/* Linh ----------------------------------*/
// test font
Route::get('/testfont', function (){
    return view('testfont');
});

// test audio visualizing
Route::get('/audio', function (){
    return view('audio');
});

// test audio visualizing
Route::get('/testaudio', function (){
    return view('testaudio');
});

// nodejs
Route::get('/chat', 'msgController@index');
Route::get('sendmsg','msgController@sendmsg');
Route::group(['middleware' => ['web']], function () {
    //
});

/*Route::get('/_debugbar/assets/stylesheets', [
    'as' => 'debugbar-css',
    'uses' => '\Barryvdh\Debugbar\Controllers\AssetController@css'
]);

Route::get('/_debugbar/assets/javascript', [
    'as' => 'debugbar-js',
    'uses' => '\Barryvdh\Debugbar\Controllers\AssetController@js'
]);

Route::get('/_debugbar/open', [
    'as' => 'debugbar-open',
    'uses' => '\Barryvdh\Debugbar\Controllers\OpenController@handler'
]);*/
// nodejs end

Route::get('/searchajax','PlayMusicController@searchAjax');
Route::get('/addmasterajax',['middleware' => 'admin', 'as' => 'masteraddajax', 'uses' => 'MasterController@addajax']);

// Chrome extension
Route::get('/openmusic', 'openMusicController@createOpenMusicView');
Route::get('/open', 'MusicController@openMusic');
Route::get('/close', 'MusicController@closeMusic');

// set time to open and close music app
Route::get('/adminplaylist', 'AdminPlaylistController@index');
Route::post('set_open_time', 'AdminPlaylistController@emitOpenTime');
Route::post('set_closed_time', 'AdminPlaylistController@emitClosedTime');
Route::post('set_time', 'AdminPlaylistController@emitTime');
Route::post('set_song_number', 'AdminPlaylistController@setSongNumber');
Route::post('set_slaver_number', 'AdminSlaverNumController@setSlaverNumber');
// set time to reset databse

Route::get('/adminresetdb', ['middleware' => 'admin', 'as' => 'adminresetdb', 'uses' => 'AdminResetDbController@index']);
Route::get('/adminslavernum', ['middleware' => 'admin', 'as' => 'adminresetdb', 'uses' => 'AdminSlaverNumController@index']);

//Route::post('set_resetdb_time', ['middleware' => 'admin', 'as' => 'master', 'uses' => 'AdminResetDbController@emitResetDbTime']);
Route::post('set_resetdb_time', 'AdminResetDbController@emitResetDbTime');

Route::get('/resettables', 'TaskScheduleController@resetTables');

Route::get('ip', 'IpController@getIp');

// task schedule

/* Linh ----------------------------------*/

Route::post('/updateajax','AjaxUpdateUser@update');

Route::get('/userlist',['middleware' => 'admin', 'as' => 'userlist', 'uses' =>'AdminUserController@userList']);
Route::post('/userlist/blockuser',['middleware' => 'admin', 'uses' => 'AdminUserController@blockUser']);
Route::post('/userlist/unblockuser',['middleware' => 'admin', 'uses' =>'AdminUserController@unblockUser']);
Route::post('/userlist/adminizeuser',['middleware' => 'superadmin', 'uses' =>'AdminUserController@adminizeUser']);
Route::post('/userlist/unadminizeuser',['middleware' => 'superadmin', 'uses' =>'AdminUserController@unadminizeUser']);

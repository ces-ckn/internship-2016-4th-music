<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UserEditRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if(Request::get('current_password')=="" && Request::get('new_password')=="" &&
        Request::get('new_password_confirmation')==""){
            return [
                'name' => 'required'
            ];
        }
        else{
            return [
                'name'                      => 'required',
                'current_password'          => 'required|min:4',
                'new_password'              => 'required|min:4|confirmed|different:current_password',
                'new_password_confirmation' => 'required|min:4|same:new_password',
            ];
        }
    }
}

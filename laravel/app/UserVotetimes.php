<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserVotetimes extends Model
{
    protected $table='user_votetimes';
    protected $fillable = array('user_id', 'votetimes');
    protected $primaryKey = 'user_id';
}

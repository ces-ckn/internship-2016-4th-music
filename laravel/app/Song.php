<?php
namespace App;
class Song{
		var $song_id;
		var $title;
		var $artist;
		var $link_download;
		var $source;
		var $total_play;
		function __construct($s){
			$this->song_id = $s['song_id'];
			$this->title = $s['title'];
			$this->artist = $s['artist'];
			foreach ($s['link_download'] as $q => $l){
				$this->link_download[$q] = $l;
			}
			foreach ($s['source'] as $q => $l){
				$this->source[$q] = $l;
			}
		}
}
?>
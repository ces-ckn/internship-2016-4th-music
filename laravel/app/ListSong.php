<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class ListSong extends Model
{
    use SoftDeletes;
    protected $table = 'list_song';
    protected $fillable = [
        'voted','title','artist','code128','code320'
    ];
    protected $dates = ['deleted_at'];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class MasterList extends Model
{
    use SoftDeletes;
    protected $table = 'master_list_song';
    protected $dates = ['deleted_at'];
}

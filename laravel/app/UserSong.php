<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSong extends Model
{
    protected $table = 'user_song';
    protected $fillable = array('user_id', 'song_id');
}

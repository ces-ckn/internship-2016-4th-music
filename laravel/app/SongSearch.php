<?php
namespace App;
use App\ListSong;
use App\MasterList;
class SongSearch{
		var $song_id;
		var $title;
		var $artist;
		var $genre;
		var $total_play;
		var $isInList;
        var $isInMList;
        var $code128;
        var $code320;
		function __construct($s){
			$this->song_id = $s['song_id'];
			$this->title = $s['title'];
			$this->artist = $s['artist'];
			$this->genre = $s['genre'];
			$this->total_play = $s['total_play'];
			$this->isInList = self::isInList($s['song_id']);
            $this->isInMList = self::isInMList($s['song_id']);
            $url128=$s['source']['128'];
            $array128 = (explode("/",$url128));
            $this->code128 = $array128[count($array128)-1];
            
            if(count($s['source'])>=2){
                $url320=$s['source']['320'];
                $array320 = (explode("/",$url320));
                $this->code320 = $array320[count($array320)-1];
            }else{
                $this->code320="";
            }
		}

		public function isInList(){
        $result = ListSong::where('id_song', $this->song_id)->get();
        if($result->isEmpty()){
            $isInList = false;
        } else{
            $isInList = true;
        }
        return $isInList;
    	}

    	public function isBlocked(){
        $result = BlockedSong::where('id_song', $this->song_id)->get();
        if($result->isEmpty()){
            $isBlocked = false;
        } else{
            $isBlocked = true;
        }
        return $isBlocked;
    	}

        public function isInMList(){
            $result = MasterList::where('id_song', $this->song_id)->get();
            if($result->isEmpty()){
                $isInMList = false;
            } else{
                $isInMList = true;
            }
            return $isInMList;
        }
}
?>
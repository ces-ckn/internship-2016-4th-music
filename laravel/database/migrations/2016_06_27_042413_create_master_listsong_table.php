<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Eloquent\SoftDeletes;
class CreateMasterListsongTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_list_song', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('id_song')->unsigned();
            $table->integer('position');
            $table->string('title');
            $table->string('artist');
            $table->string('code128');
            $table->string('code320');
            $table->timestamps();
        });
        Schema::table('master_list_song', function ($table) {
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('master_list_song');
    }
}

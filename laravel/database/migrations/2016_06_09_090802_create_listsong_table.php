<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Eloquent\SoftDeletes;
class CreateListsongTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('list_song', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('id_song')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->nullable();
            $table->integer('position');
            $table->boolean('voted')->default(false);
            $table->string('title');
            $table->string('artist');
            $table->string('code128');
            $table->string('code320');
            $table->timestamps();
        });
        Schema::table('list_song', function ($table) {
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('list_song');
    }
}

var express = require('express')();
var server = require('http').createServer(express);
var io = require('socket.io')(server);
var request = require('request');
var schedule = require('node-schedule');

var urlOpen = 'http://mylaravel5.herokuapp.com/open';
var urlClose = 'http://mylaravel5.herokuapp.com/close';
var urlResetDb = 'http://mylaravel5.herokuapp.com/resettables';

var openTime = {hour:'', minute:''};
var closedTime = {hour:'', minute:''};
var resetdbTime = {hour:'', minute:''};
var songNumber = 10;

 express.set('views', __dirname + '/views');
 express.set('view engine', 'ejs');

 express.get('/', function(request, response) {
   response.render('pages/index');
 });
  express.get('/laptrinhvnc', function(request, response) {
   response.render('pages/laptrinhvnc');
 });
server.listen(process.env.PORT || 5000);

var slaverLoadedNum = 0;
var slaverTotalNum = 3;

io.on('connection', function(socket){

	// var ip = socket.handshake.headers['x-forwarded-for'] || socket.handshake.address.address;
	// console.log('--------------->headers' + ip);
	// io.emit('connect', ip);

	// loaded
	socket.on('loaded',function(msg){
		slaverLoadedNum++;
		console.log('slaverLoadedNum = ' + slaverLoadedNum);
		if (slaverLoadedNum == slaverTotalNum) {
			console.log('ready to play!');
			io.emit('readyToPlay', 'ok');
			slaverLoadedNum = 0;
		}		
	});

	socket.on('ip', function(msg){	
		io.emit('ip', msg.ip);
		console.log(msg.ip);
	});

	socket.on('setslavernumber', function(msg){
		slaverTotalNum = parseInt(msg.slaverNumber);
		//console.log(slaverTotalNum + 1);
	});

	socket.on('getSlaverNumber', function(msg){
		console.log('slaverTotalNum = ' + slaverTotalNum);
		io.emit('getSlaverNumber', slaverTotalNum);
	});

	socket.on('setsongnumber', function(msg){	
		songNumber = msg.songNumber;	
		io.emit('songnumber', msg);
		console.log(msg);
	});

	socket.on('getSongNumber', function(msg){
        io.emit('getSongNumber', songNumber);
    });

	socket.on('getTime',function(msg){
		console.log(msg);			
		io.emit('getTime', {openTime:openTime, closedTime:closedTime});
	});

	socket.on('getResetTime',function(msg){
		console.log(msg);			
		io.emit('getResetTime', {time:resetdbTime});
	});

	// start now
	socket.on('start',function(msg){
		console.log(msg);		
		io.emit('start', {list:msg, songNumber:songNumber});
	});

	// stop now
	socket.on('stop',function(msg){
		console.log(msg);
		io.emit('close', msg);
	});

	// Chat
	socket.on('new_msg',function(msg){
		console.log(msg);
		io.emit('new_msg_client',msg);
	});

	// vote song
	socket.on('votesong',function(msg){
		console.log(msg);
		io.emit('voteresult',msg);
	});

	// add song
	socket.on('addsong',function(msg){
		io.emit('addresult',msg);
	})

	socket.on('listaddsong',function(msg){
		io.emit('listaddresult',msg);
	});

	
	socket.on('sendAction',function(msg){
		console.log(msg);
		io.emit('getAction',msg);
	});

	socket.on('masteradd',function(msg){
		io.emit('masteraddresult',msg);
	});

	socket.on('masterlistaddsong',function(msg){
		console.log(msg);
		io.emit('masterlistaddresult',msg);
	});

	// open music
	socket.on('open',function(msg){
		// var date = new Date();
		// var hour = date.getHours();
		// var minute = date.getMinutes();
		// console.log("hehe");
		io.emit('open', {songNumber:songNumber});
	});

	// open music
	socket.on('close',function(msg){
		// var date = new Date();
		// var hour = date.getHours();
		// var minute = date.getMinutes();
		console.log("receive emit close");
		io.emit('close', 'ok');
	});

	// Set time to open music on slaver
	var openMusic;
	var cronOpen;
	socket.on('setopentime', function(opentime){
		openTime.hour = opentime.hour;
		openTime.minute = opentime.minute;
		openTime.hour = (parseInt(openTime.hour) + 7) % 24 ;

		if(openTime.hour < 10){
			openTime.hour = '0' + openTime.hour;
		}else{
			openTime.hour += '';
		}

		if(parseInt(openTime.minute) < 10){
			openTime.minute = '0' + openTime.minute;
		}else{
			openTime.minute += '';
		}

		io.emit('getTime', {openTime:openTime, closedTime:closedTime});

		cronOpen = '0 ' + opentime.minute + ' ' + opentime.hour + ' * * *';
		console.log(cronOpen);
		openMusic = schedule.scheduleJob(cronOpen, function(){
			slaverLoadedNum = 0;
			console.log('open music!');
			request(urlOpen, function (error, response, body) {
			  if (!error && response.statusCode == 200) {
			    console.log(body) // Show the HTML for the Google homepage. 
			  }
			})
		});
	});

	// Set time to close music on slaver
	var closeMusic;
	var cronClose;
	socket.on('setclosedtime', function(closedtime){
		closedTime.hour = closedtime.hour;
		closedTime.minute = closedtime.minute;
		closedTime.hour = (parseInt(closedTime.hour) + 7) % 24;

		if(closedTime.hour < 10){
			closedTime.hour = '0' + closedTime.hour;
		}else{
			closedTime.hour += '';
		}

		if(parseInt(closedTime.minute) < 10){
			closedTime.minute = '0' + closedTime.minute;
		}else{
			closedTime.minute += '';
		}

		io.emit('getTime', {openTime:openTime, closedTime:closedTime});

		cronClose = '0 ' + closedtime.minute + ' ' + closedtime.hour + ' * * *';
		console.log(cronClose);
		closeMusic = schedule.scheduleJob(cronClose, function(){
			console.log('close music!');
			request(urlClose, function (error, response, body) {
			  if (!error && response.statusCode == 200) {
			    console.log(body) // Show the HTML for the Google homepage. 
			  }
			})
		});
	});

	// Set time to reset some tables
	var resetDb;
	var cronResetDb;
	socket.on('setresetdbtime', function(resetdbtime){

		resetdbTime.hour = resetdbtime.hour;
		resetdbTime.minute = resetdbtime.minute;
		resetdbTime.hour = (parseInt(resetdbTime.hour) + 7) % 24;

		if(resetdbTime.hour < 10){
			resetdbTime.hour = '0' + resetdbTime.hour;
		}else{
			resetdbTime.hour += '';
		}

		if(parseInt(resetdbTime.minute) < 10){
			resetdbTime.minute = '0' + resetdbTime.minute;
		}else{
			resetdbTime.minute += '';
		}

		io.emit('getResetTime', {time:resetdbTime});

		cronResetDb = '0 ' + resetdbtime.minute + ' ' + resetdbtime.hour + ' * * *';
		console.log(cronResetDb);
		//io.emit('setresetdbtime', {cronResetDb:cronResetDb});
		resetDb = schedule.scheduleJob(cronResetDb, function(){
	  		console.log('reset some tables!');
	  		request(urlResetDb, function (error, response, body) {
		  		if (!error && response.statusCode == 200) {
		    		console.log(body) // Show the HTML for the Google homepage. 
		  		}
			})
		});

	});

});

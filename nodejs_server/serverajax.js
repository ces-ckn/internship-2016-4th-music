var express = require('express')();
var server = require('http').createServer(express);
var io = require('socket.io')(server);
 express.set('views', __dirname + '/views');
 express.set('view engine', 'ejs');

 express.get('/', function(request, response) {
   response.render('pages/index');
 });
  express.get('/laptrinhvnc', function(request, response) {
   response.render('pages/laptrinhvnc');
 });
server.listen(process.env.PORT || 5000);

io.on('connection',function(socket){
	//socket.emit('hello',{text:"node!"});

	// Chat
	socket.on('votesong',function(msg){
		io.emit('voteresult',msg);
	});

	socket.on('addsong',function(msg){
		io.emit('addresult',msg);
	});

	socket.on('listaddsong',function(msg){
		io.emit('listaddresult',msg);
	});

	socket.on('sendAction',function(msg){
		io.emit('getAction',msg);
	});

	socket.on('masteradd',function(msg){
		io.emit('masteraddresult',msg);
	});

	socket.on('masterlistaddsong',function(msg){
		console.log(msg);
		io.emit('masterlistaddresult',msg);
	});
	// open music
	socket.on('open',function(msg){
		console.log("hehe");
		io.emit('open', msg);
	});
});